(function ($) {
	"use strict";
jQuery(document).ready(function($) {
         // Declare Carousel jquery object
  var owl = $('.hompage-slider');
  // Carousel initialization
  owl.owlCarousel({
      items: 1,
            loop: true,
            dots: true,
            nav: true,
            touchDrag : false,
            mouseDrag: false,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplay: true,
            autoplayTimeout: 4000,
            navText: ["<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"],
  });
        // add animate.css class(es) to the elements to be animated
  function animationSet ( uniElement, aniInOut ) {
    // Store all animationend event name in a string.
    // cf animate.css documentation
    var animationEnd = 'animationend';
    uniElement.each ( function () {
      var $element = $(this);
      var $animationType = 'animated ' + $element.data( 'animation-' + aniInOut );
      $element.addClass($animationType).one(animationEnd, function () {
           // remove animate.css Class at the end of the animations
        $element.removeClass($animationType);
      });
    });
  }
        // Fired before current slide change
  owl.on('change.owl.carousel', function(event) {
      var $currentItem = $('.owl-item', owl).eq(event.item.index);
      var $elemsToanim = $currentItem.find("[data-animation-out]");
      animationSet ($elemsToanim, 'out');
  });
// Fired after current slide has been changed
  owl.on('changed.owl.carousel', function(event) {
      var $currentItem = $('.owl-item', owl).eq(event.item.index);
      var $elemsToanim = $currentItem.find("[data-animation-in]");
      animationSet ($elemsToanim, 'in');
  })
	//binodon-slider
					$(".binodon-slider").owlCarousel({
									margin:10,
								 	items: 4,
									loop: true,
									dots: true,
									nav: false,
									touchDrag : true,
									mouseDrag: true,
									animateIn: 'fadeIn',
									animateOut: 'fadeOut',
									autoplay: true,
									autoplayTimeout: 4000,
									navText: ["<i class='fa fa-angle-left'></i>",
									"<i class='fa fa-angle-right'></i>"],
							responsive: {
									0: {
											items: 1
									},
									600: {
											items: 3
									},
									1000: {
											items: 4
									}
							}

					});
 });

 //search bar
$('a[href="#search"]').on('click', function(event) {
event.preventDefault();
$('#search').addClass('open');
$('#search > form > input[type="search"]').focus();
});
$('#search, #search button.close').on('click keyup', function(event) {
if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
$(this).removeClass('open');
}
});
}(jQuery));
