<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('slug');
            $table->text('title')->nullable();
            $table->longText('keywards')->nullable();
            $table->longText('description')->nullable();
            $table->text('banner')->nullable();
            $table->enum('top_menu', ['1','0'])->nullable()->comment('Yes=1,No=0')->default(0);
            $table->integer('order_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('created_by');
            $table->tinyInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
