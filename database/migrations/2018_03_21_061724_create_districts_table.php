<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('bn_name');
          $table->tinyInteger('division_id');
          $table->string('lat')->nullable();
          $table->string('lon')->nullable();
          $table->string('website')->nullable();
          $table->tinyInteger('order_id')->nullable();
          $table->tinyInteger('status')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
