<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFooterInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer_infos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->longText('about')->nullable();
          $table->string('copywrite')->nullable();
          $table->longText('address')->nullable();
          $table->string('uemail')->nullable();
          $table->string('phone')->nullable();
          $table->string('editor')->nullable();
          $table->string('contact')->nullable();
          $table->string('email')->nullable();
          $table->tinyInteger('created_by');
          $table->tinyInteger('updated_by')->nullable();
          $table->tinyInteger('status')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footer_infos');
    }
}
