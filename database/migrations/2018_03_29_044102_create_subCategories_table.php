<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatesubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subCategories', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('category_id');
          $table->text('slug');
          $table->longText('description')->nullable();
          $table->tinyInteger('created_by');
          $table->tinyInteger('updated_by')->nullable();
          $table->integer('order_id')->nullable();
          $table->tinyInteger('status')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subCategories');
    }
}
