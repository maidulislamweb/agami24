<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('news_id')->unsigned();
          $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade')->onUpdate('cascade');
          $table->tinyInteger('division_id')->nullable();
          $table->integer('district_id')->nullable();
          $table->integer('upazila_id')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
