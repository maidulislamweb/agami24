<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('category_id')->unsigned();
          $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
          $table->integer('sub_category_id')->nullable();
          $table->text('title');
          $table->text('slug');
          $table->longText('thumbnail')->nullable();
          $table->longText('content');
          $table->string('photo')->nullable();
          $table->text('caption')->nullable();
          $table->text('tags')->nullable();
          $table->enum('is_feature', ['1','0'])->nullable()->comment('Yes=1,No=0')->default(0);
          $table->enum('is_leadnews', ['1','0'])->nullable()->comment('Yes=1,No=0')->default(0);
          $table->tinyInteger('created_by');
          $table->tinyInteger('updated_by')->nullable();
          $table->tinyInteger('status')->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
