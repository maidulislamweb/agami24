<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('title')->nullable();
          $table->string('thumbnail')->nullable();
          $table->string('logo')->nullable();
          $table->string('favicon')->nullable();
          $table->string('founder')->nullable();
          $table->string('editor')->nullable();
          $table->string('contact')->nullable();
          $table->string('support')->nullable();
          $table->string('website')->nullable();
          $table->string('establish')->nullable();
          $table->string('email')->nullable();
          $table->longText('address')->nullable();
          $table->string('facebook')->nullable();
          $table->string('twitter')->nullable();
          $table->string('youtube')->nullable();
          $table->string('googleplus')->nullable();
          $table->string('linkedin')->nullable();
          $table->string('pinterest')->nullable();
          $table->string('instagram')->nullable();
          $table->string('flickr')->nullable();
          $table->tinyInteger('created_by');
          $table->tinyInteger('updated_by')->nullable();
          $table->tinyInteger('status')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
