<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('category_id');
          $table->integer('sub_category_id')->nullable();
          $table->text('title');
          $table->text('slug');
          $table->text('thumbnail')->nullable();
          $table->longText('content')->nullable();
          $table->string('photo');
          $table->text('tags')->nullable();
          $table->text('publish')->nullable();
          $table->tinyInteger('created_by');
          $table->tinyInteger('updated_by')->nullable();
          $table->tinyInteger('status')->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
