@extends('layouts.app')
@section('title', 'Home')
@section('content')
    <main id="main-content">
	<section>
		<div class="container">
			<div class="row paddingTopBottom20">
				<div class="col-sm-12">
                    <ol class="breadcrumb  no-margin">
                        <li><a href="index.html"><i class="fa fa-home text-danger"></i></a></li>
                        <li class="active">ক্রিকেট</li>
                    </ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8 main-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="single-block">
								<div class="row">
									<div class="col-sm-8">
										<div class="img-box">
											<a href="" title="">
                        <img alt="া" src="" class="img-responsive">
											</a>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="paddingRight10 paddingTop20">
											<h3 style="font-size:1.5em;line-height:34px;margin-bottom:10px;">
                        <a href="" title=""></a></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Advertisement -->
					<div class="row text-center">
						<div class="col-sm-12">

						</div>
					</div>
					<!-- End Advertisement -->
          <div class="row" id="loadMoreContent">
            @if (count($cricket) > 0)
          @foreach($cricket as $crick)
							<div class="col-sm-6">
							<div class="single-block cat-block">
								<div class="row">
									<div class="col-sm-5">
										<div class="img-box">
											<a href="education/news/423919.html" title="{{$crick->news_title}}">
                        <img alt="{{$crick->news_title}}" src="{{asset('uploads/'.$crick->n_photo)}}" class="img-responsive">
										</a>
										</div>
									</div>
									<div class="col-sm-7">
										<div class="paddingTop10 paddingRight10">
											<h3 style="font-size:1.1em;"><a href="education/news/423919.html" title="{{$crick->news_title}}">{{$crick->news_title}}</a></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
            @endforeach
            @else
      <hr>
      <div class="no-items">
      <div class="no-items-wrapper">
         <!--img src="{{ asset('media/imgAll/animated-tea-and-teapot-image-0041.gif') }}" alt="" class="" width="50px"-->
        <h1 class="text-center cup"><i class="fa fa-coffee" aria-hidden="true"></i></h1>
        <h4 class="text-center">Great, You have no thing to do. Just take a cup of tea</h4>
      </div>
    </div>
           @endif
				</div>
					<div class="text-center paddingBottom20">
						<button id="load_more_button"><img alt="Loader" src="../cdn.jagonews24.com/media/common/ajax-loader.gif"  class="animation_image" style="width: 30px;"> আরও পড়ুন</button>
					</div>
				</div>
				<aside class="col-sm-4 aside">
					<div class="row">
						<div class="col-sm-12">
							<div class="jagoTab2 ThreeTab">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs nav-justified" role="tablist">
									<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">খেলাধুলা</a></li>
									<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">বিনোদন</a></li>
									<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">লাইফস্টাইল</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane fade in active" id="tab1">
										<ul class="media-list">
											<li class="media">
						<div class="media-left">
							<a href=""><img class="media-object" src="" alt=""></a></div>
					<div class="media-body">
						<h4 class="media-heading"><a href=""></a></h4>
					</div></li>
										</ul>
										<div class="allnews"><a href="sports.html">খেলাধুলার সবখবর</a></div>
									</div>
									<div role="tabpanel" class="tab-pane" id="tab2">
										<ul class="media-list">
											<li class="media">
						<div class="media-left">
							<span>১</span><a href=""><img class="media-object" src="" alt=""></a></div>
					<div class="media-body">
						<h4 class="media-heading"><a href=""></a></h4>
					</div></li>
										</ul>
										<div class="allnews"><a href="">বিনোদনের সবখবর</a></div>
									</div>
									<div role="tabpanel" class="tab-pane" id="tab3">
										<ul class="media-list">
											<li class="media">
						<div class="media-left">
							<span>১</span><a href="lifestyle/article/423797.html">
                <img class="media-object" src="" alt=""></a></div>
					<div class="media-body">
						<h4 class="media-heading"><a href=""></a></h4>
					</div></li>
										</ul>
										<div class="allnews"><a href="">লাইফস্টাইলের সবখবর</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="jagoTab2 TwoTab">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs nav-justified" role="tablist">
									<li role="presentation" class="active"><a href="#tab21" aria-controls="tab21" role="tab" data-toggle="tab">সর্বশেষ</a></li>
									<li role="presentation"><a href="#tab22" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a></li>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane fade in active" id="tab21">
										<ul class="media-list">
											<li class="media">
						<div class="media-left">
              <a href=""><img class="media-object" src="" alt=""></a></div>
					<div class="media-body">
						<h4 class="media-heading"><a href=""></a></h4>
					</div></li>
										</ul>
										<div class="allnews">
                      <a href="" rel="nofollow">আজকের সর্বশেষ সবখবর</a></div>
									</div>
									<div role="tabpanel" class="tab-pane" id="tab22">
										<ul class="media-list">
											<li class="media">
						<div class="media-left">
							<span>১</span><a href="">
                <img class="media-object" src="" alt=""></a></div>
					<div class="media-body">
						<h4 class="media-heading"><a href=""></a></h4>
					</div></li>
										</ul>
										<div class="allnews">
                      <a href="" rel="nofollow"> আজকের সর্বশেষ সবখবর</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</section>
</main>
  @endsection
