@extends('layouts.app')
@section('content')
<main id="main-content">
    <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb cat-breadcrumb">
                    <div class="container">
                        <ul class="breadcrumb-head list-unstyled list-inline">
                            <!--li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li-->
                            <li class="active"><a href="{{url('/')}}">{{$category->bn_name}}</a></li>
                        </ul>
                    </div>
                    @if(!empty($subcategory))
                    <div class="secondary_menu_wrap">
                        <div class="container">
                            <div class="secondary_menu">
                                <ul class="list-unstyled list-inline">
                                     @foreach($subcategory as $subcat)
                                    <li class="menu_color">
                                        <a class="static " href="{{url('/')}}">{{$subcat->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 main-content">
                    <div class="row">
                        <div class="col-sm-12">
                          @if(!empty($featurenews))
                          @foreach($featurenews as $featur)
                            <div class="category-lead">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="img-box">
                                            <a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="">
                                                <img alt="" src="{{asset('media/news/'.$featur->nphoto)}}" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="title">
                                            <h3><a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="{{$featur->title}}">{{$featur->title}}</a></h3>
                                        </div>
                                        <h4 class="lead-thumb">{{str_limit($featur->thumbnail,150)}}</h4>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        @if(!empty($category_wise))
                        @foreach($category_wise as $bang)
                        <div class="col-sm-4">
                            <div class="category-block">
                                <div class="cat-img">
                                    <a href='{{ url("$bang->cslug/articles/{$bang->id}/{$bang->slug}") }}' title="{{$bang->title}}">
									<img alt="{{$bang->title}}" src="{{asset('media/news/'.$bang->nphoto)}}" class="img-responsive">
                                 </a>
                                </div>
                                <div class="cat-text-box">
                                    <h3 class="cat-text-title">
                                   <a href='{{ url("$bang->cslug/articles/{$bang->id}/{$bang->slug}") }}' title="{{$bang->title}}">{{$bang->title}}</a>
                                 </h3>
                                    <h4 class="cat-thumb">{{str_limit($bang->thumbnail,100)}}</h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="simple-pagination">
                        {{$category_wise->links()}}
                    </div>
                </div>
                <aside class="col-sm-4 aside">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="sidebar-widget">
                                <h4 class="sidebar-title">সর্বশেষ</h4>
                                <div class="tab-content">
                                    <div role="tabpanel" class="">
                                        <ul class="media-list">
                                            @if(!empty($latest))
                                            @foreach($latest as $lat)
                                            <li class="media">
                                                <div class="media-left">
                                                    <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>
                                        <img width="100px;" class="media-object" src="{{asset('media/news/'.$lat->nphoto)}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                         <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>{{$lat->title}}</a>
                                                    </h4>
                                                </div>
                                            </li>
                                            @endforeach @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="jagoTab2 TwoTab">
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation">
                                        <a href="#tab22" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel">
                                        <ul class="media-list">
                                            <li class="media">
                                                <div class="media-left">
                                                    <a href="">
                                                        <img class="media-object" src="" alt="">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> <a href=""></a></h4>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</main>
@endsection
