@extends('layouts.app')
@section('title', 'আর্কাইভ')
@section('content')
<section>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}"/>
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="row">
               <div class="col-sm-12">
                  <div class="text-center padding20 no-margin">
                     <h2 class="no-margin"> সব খবর</h2>
                  </div>
               </div>
            </div>
            <form action="archive" method="get" class="form-horizontal">
               <div class="box-white padding15">
                  <div class="row">
                     <div class="col-sm-5">
                        <div class="input-group">
                           <div class="input-group-addon">তারিখ  <i class="fa fa-calendar"></i></div>
                           <input type="text" name="dateFrom" class="form-control" id="dateFrom">
                           <div class="input-group-addon">থেকে  <i class="fa fa-calendar"></i></div>
                           <input type="text" name="dateTo" class="form-control" id="dateTo">
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <select class="form-control" name="cat" id="category">
													<option value="">-- ক্যাটাগরি --</option>
													@if(!empty($category))
													@foreach($category as $cat)
                           <option value="{{$cat->id}}">{{$cat->name}}</option>
													 @endforeach
													 @endif

                        </select>
                     </div>
                     <div class="col-sm-4">
                        <input type="text" name="keyword" class="form-control" id="text_search" placeholder="আপনি কী খুঁজছেন?">
                     </div>
                  </div>
               </div>
               <div class="box-ashes padding15 marginBottom20">
                  <div class="row">
                     <div class="col-sm-2">
                        <label for="division" class="sr-only">বিভাগ</label>
												<select class="form-control"  name="division_id" id="division_id" onchange="get_district(this.value);">
													<option value="">--বিভাগ--</option>
													@if(!empty($divisions))
													@foreach($divisions as $division)
													<option value="{{$division->id}}">{{$division->bn_name}}</option>
													@endforeach
													@endif
												</select>
												@if ($errors->has('division_id'))
												<p class="help-block">{{ $errors->first('division_id') }}</p>
												@endif
                     </div>
                     <div class="col-sm-3">
                        <label for="district" class="sr-only">জেলা</label>
												<select class="form-control" name="district_id" id="district_id" onchange="get_upazila(this.value);">
													<option value="">--জেলা--</option>
												</select>
												@if ($errors->has('district_id'))
												<p class="help-block">{{ $errors->first('district_id') }}</p>
												@endif
                     </div>
                     <div class="col-sm-3">
                        <label for="upozilla" class="sr-only">উপজেলা</label>
												<select class="form-control"name="upazila_id" id="upazila_id" >
													<option value="">--উপজেলা--</option>
												</select>
												@if ($errors->has('district_id'))
												<p class="help-block">{{ $errors->first('upazila_id') }}</p>
												@endif
                     </div>
                     <div class="col-sm-2"><button type="submit" class="btn btn-primary btn-block">খুজুন</button></div>
                     <div class="col-sm-2">
                        <a href="" class="btn btn-danger btn-block">সব সংবাদ খুজুন</a>
                     </div>
                  </div>
               </div>
            </form>
            <div class="row">

							@if(!empty($archive))
							@foreach($archive as $arc)
               <div class="col-sm-6">
                  <div class="single-block auto">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="single-block auto no-margin archive-box">
                              <a href='{{ url("$arc->cslug/articles/{$arc->id}/{$arc->slug}") }}'>
                              <img alt="{{$arc->title}}" src="{{asset('media/news/'.$arc->nphoto)}}" class="img-responsive">
                              </a>
                              <div class="overlay-category">
                                 <a href="#">{{$arc->cbname}}</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-8 paddingTop10">
                           <h3 class="no-margin"><a href='{{ url("$arc->cslug/articles/{$arc->id}/{$arc->slug}") }}'>{{$arc->title}}</a></h3>
                           <small class="text-muted">
                             <i class="fa fa fa-clock-o"></i> প্রকাশিত: {{ \App\Http\Controllers\CommonController::GetBangla(date('j M, Y, h:i', strtotime($arc->created_at)))}}</small>
                        </div>
                     </div>
                  </div>
               </div>
							 @endforeach
							 @endif

            </div>




            <div class="row">
               <div class="col-sm-12">
                  <div class="box-white paddingTop15 paddingBottom10 marginBottom20">
                     {{$archive->links()}}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
 <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/daterangepicker.js') }}"></script>
<script>
function get_subcategory(category_id)
{
		var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
		var post_url        = '{{ url("get_subcategory")}}';
		$.ajax({
				url: post_url,
				type: 'POST',
				data: {_token: CSRF_TOKEN, category_id: category_id},
				success: function(html_data)
				{
						if (html_data != '')
						{
								$('#subcategory_id').html(html_data);

						}
				}
		});
}
function get_district(division_id)
{
		var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
		var post_url        = '{{ url("get_district")}}';
		$.ajax({
				url: post_url,
				type: 'POST',
				data: {_token: CSRF_TOKEN, division_id: division_id},
				success: function(html_data)
				{
						if (html_data != '')
						{
								$('#district_id').html(html_data);

						}
				}
		});
}
function get_upazila(district_id)
{
		var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
		var post_url        = '{{ url("get_upazila")}}';
		$.ajax({
				url: post_url,
				type: 'POST',
				data: {_token: CSRF_TOKEN, district_id: district_id},
				success: function(html_data)
				{
						if (html_data != '')
						{
								$('#upazila_id').html(html_data);

						}
				}
		});
}
</script>
<script type="text/javascript">
   $(function() {
       $('#dateFrom').daterangepicker({
           singleDatePicker: true,
           showDropdowns: true,
           maxDate: new Date(),
           locale: {
               format: 'DD/MM/YYYY'
           }
       });
       $('#dateFrom').val('');
   });
   $(function() {
       $('#dateTo').daterangepicker({
           singleDatePicker: true,
           showDropdowns: true,
           maxDate: new Date(),
           locale: {
           format: 'DD/MM/YYYY'
           }
       });
       $('#dateTo').val('');
   });
   });
</script>
@endsection
