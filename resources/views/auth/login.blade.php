@extends('auth.app')
@section('title', 'Login')
@section('content')
<style media="screen">
  .panel{
    padding:50px 0px;
    margin-top: 25%;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3" >
            <div class="panel">
                <div class="panel-header"><h3 class="text-center">{{ __('Login') }}</h3></div>

                <div class="panel-body">
                    <form method="POST" class="form-horizontal" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-2">
                              <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-2">
                              <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>

                        <!--div class="form-group row">
                          <div class="col-md-4">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                  </label>
                              </div>
                          </div>

                        </div-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
