@extends('auth.app')
@section('title', 'Login')
@section('content')
<style media="screen">
  .panel{
    padding:50px 0px;
    margin-top: 25%;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3" >
            <div class="panel">
                <div class="panel-header"><h3 class="text-center">{{ __('Reset Password') }}</h3></div>

                <div class="panel-body">
                <div class="">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">


                            <div class="col-md-8 col-md-offset-2">
                              <label for="email" class="">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>
  @endsection
