<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="keyword" content="">
      <link rel="shortcut icon" type="image/png" href="img/favicon-16x16.png">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>@yield('title')</title>
      <!-- Bootstrap core CSS -->
      <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/bootstrap-reset.css') }}" rel="stylesheet">
      <!--external css-->
      <link href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="{{ URL::asset('css/style2.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet" />
      <!-- js placed at the end of the document so the pages load faster -->
      <script src="{{ URL::asset('js/jquery.js') }}"></script>
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <section id="container">
         @yield('content')
      </section>
   </body>
</html>
