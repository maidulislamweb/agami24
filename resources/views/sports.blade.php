@extends('layouts.app')
@section('content')
<main id="main-content">
    <section class="paddingTop20">
        
        <div class="container">
            <div class="row">
                <div class="col-sm-8 main-content">
                    <div class="row">
                        <div class="col-sm-12">
                          @if(!empty($featurenews))
                          @foreach($featurenews as $featur)
                            <div class="category-lead">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="img-box">
                                            <a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="">
                                                <img alt="" src="{{asset('media/news/'.$featur->nphoto)}}" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="title">
                                            <h3><a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="{{$featur->title}}">{{$featur->title}}</a></h3>
                                        </div>
                                        <h4 class="lead-thumb">{{str_limit($featur->thumbnail,150)}}</h4>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        @if(!empty($sports))
                        @foreach($sports as $spo)
                        <div class="col-sm-4">
                            <div class="category-block">
                                <div class="cat-img">
                                    <a href='{{ url("$spo->cslug/articles/{$spo->id}/{$spo->slug}") }}' title="{{$spo->title}}">
																	 <img alt="{{$spo->title}}" src="{{asset('media/news/'.$spo->nphoto)}}" class="img-responsive">
                                 </a>
                                </div>
                                <div class="cat-text-box">
                                    <h3 class="cat-text-title">
                                   <a href='{{ url("$spo->cslug/articles/{$spo->id}/{$spo->slug}") }}' title="{{$spo->title}}">{{$spo->title}}</a>
                                 </h3>
                                    <h4 class="cat-thumb">{{str_limit($spo->thumbnail,100)}}</h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="simple-pagination">
                        {{$sports->links()}}
                    </div>
                </div>
                <aside class="col-sm-4 aside">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="sidebar-widget">
                                <h4 class="sidebar-title">সর্বশেষ</h4>
                                <div class="tab-content">
                                    <div role="tabpanel" class="">
                                        <ul class="media-list">
                                            @if(!empty($latest))
                                            @foreach($latest as $lat)
                                            <li class="media">
                                                <div class="media-left">
                                                    <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>
                                        <img width="100px;" class="media-object" src="{{asset('media/news/'.$lat->nphoto)}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                         <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>{{$lat->title}}</a>
                                                    </h4>
                                                </div>
                                            </li>
                                            @endforeach @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</main>
@endsection
