@extends('layouts.app')
@section('content')
<main id="main-content">
 <section>
  <div class="container paddingTop20">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
    <div class="row">
      <div class="col-md-12">
        <div class="hidden-print">
                   <div class="hidden-xs">
                    <ol class="breadcrumb">
                     <li><a href=""><i class="fa fa-home"></i></a></li>
                     <li><a href="">{{$article->cbname}}</a></li>
                   </ol>
                 </div>
             </div>
      </div>
    </div>
    <div class="row">
    <div class="col-sm-8">
       @if(!empty($article))
      <article class="">
       <div class="single-news">
        <div class="padding15">
         <h1 class="no-margin">{{$article->title}}</h1>
         <div class="dividerDetails"></div>
         <div class="media">
          <div class="media-body">
           <span class="small">
             <i class="fa fa-pencil"></i>
             <a href="#" style="display:inline-block;" rel="nofollow">{{$article->udes}}</a>
             ---প্রকাশিত: {{ \App\Http\Controllers\CommonController::GetBangla(date('j M, Y, h:i', strtotime($article->created_at)))}}
           </span>

         </div>
       </div>
     </div>
     <div class="paddingLeft10 paddingRight10 hidden-print">
       <div class="custom-social-share">
        <ul class="social-media  list-inline list-unstyled">
         <div class="social-share">
          <div class="sharethis-inline-share-buttons"></div>
                             
                            </div>
                                 </div>
                               </div>
                               <div class="paddingTop10">
                                <div class="article-img">
                                  <a href="{{asset('media/news/'.$article->nphoto)}}" data-source="{{asset('media/news/'.$article->nphoto)}}" title="">
                                  <figure>
                            <img src="{{asset('media/news/'.$article->nphoto)}}"  style="width:100%;">
                            <figcaption>{{$article->pcaption}}</figcaption>
                                    </figure>
                                 </a>
                               </div>
                             </div>
                             <div class="content-details text-justify">{!!$article->content!!}</div>
                             <div class="dividerDetails"></div>
                             <div class="tag-dev" style="padding: 15px;">
                               <div class="article-tags">
                   <span style="font-size: 25px;">বিষয়: </span>@if(!empty($tags))
                     @foreach($tags as $tag)
                     <a class="btn btn-default" href="{{url('/topic/'.$tag->title)}}">{{$tag->title}}</a>
                      <a type="" onclick="window.print();"><i class="fa fa-print"></i></a>
                     @endforeach
                     @endif

                   <div class="dividerDetails"></div>
                   <!-- Your like button code -->
<div class="fb-like" data-href="{{ urlencode(Request::fullUrl()) }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
<div class="fb-save" data-uri="{{ urlencode(Request::fullUrl()) }}" data-size="large"></div>
                   
                 </div>
                             </div>

                             <div class="social-share">
                              <div class="sharethis-inline-share-buttons"></div>
                            </div>
                          </div>

                          <!-- Your share button code -->
  <div class="fb-share-button"
    data-href="{{ urlencode(Request::fullUrl()) }}"
    data-layout="button_count">
  </div>                         
                   </article>
               
          
           @endif
         </div>
         <aside class="col-sm-4 hidden-print" style="padding: 0px 10px;">
              <div class="single-tab">
              <ul class="media-list">
                @if(!empty($latest))
                @foreach($latest as $lat)
                <li class="media">
                  <div class="media-left">
                   <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>
                     <img alt="{{$lat->title}}" class="media-object" src="{{asset('media/news/'.$lat->nphoto)}}" width="100px" height="auto">
                   </a>
                 </div>
                 <div class="media-body">
                   <h4 class="media-heading text-justify">
                     <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>{{$lat->title}} </a></h4>
                   </div>
                 </li>
                 @endforeach
                 @endif
               </ul>
             </div>
       </aside>



     </div>
   </div>
</div>
</div>

 </section>
 <section>
   <!--single-news-->
                         <div class="container">
                            <div class="hidden-print">
                            <hr/>
                            <div class="padding15">
                             <div class="row">
                              <div class="col-sm-12">
                               <h1 class="cat-title">আরও পড়ুন</h1>
                             </div>
                           </div>
                           <div class="row">
                             @if(!empty($relnews))
                             @foreach($relnews as $rel)
                             <div class="col-sm-3">
                               <div class="read-more">
                                 <div class="single-block">
                                <div class="img-box">
                                 <a href='{{ url("$rel->cslug/articles/{$rel->id}/{$rel->slug}") }}'>
                                   <img alt="" src="{{asset('media/news/'.$rel->nphoto)}}" title="" class="img-responsive">
                                 </a>
                               </div>
                               <div class="related">
                                 <h3><a href='{{ url("$rel->cslug/articles/{$rel->id}/{$rel->slug}") }}'>{{$rel->title}}</a></h3>
                               </div>
                               <div class="meta">
                                 <span class="pull-left tags">
                                   <a href="{{url('/topic/'.$rel->ttitle)}}">{{$rel->ttitle}}</a>
                                 </span>
                               </div>
                             </div>
                               </div>
                           </div>
                           @endforeach
                           @endif
                         </div>
                       </div>
                     </div>
                         </div>

   
 </section>
</main>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b1b9997e9c37c00114cb6a3&product=inline-share-buttons"></script>
<script>
  $(document).ready(function() {
     $('.content-details img').addClass('img-responsive');
});
</script>
@endsection
