@extends('layouts.app')
@section('title', 'Home')
@section('content')
<main id="main-content">
   <section class="box-white photo">
      <div class="container">
         <div class="marginTopBottom20">
            <div class="row">
               <div class="col-sm-8">
                  <div class="col-sm-12 main-content">
                     <div class="slider-section">
                        <div class="gallery hompage-slider">
                           @if(!empty($galleries))
                           @foreach($galleries as $gal)
                           <div class="item single-slider">
                              <a href='{{ url("articles/{$gal->id}/{$gal->cslug}/{$gal->slug}") }}'>
                              <img alt="{{$gal->title}}" src="{{asset('media/gallery/'.$gal->nphoto)}}">
                              </a>
                              <div class="gallery-caption">
                                 <div class="caption">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <a style="color:#fff" href='{{ url("articles/{$gal->id}/{$gal->cslug}/{$gal->slug}") }}'>{{$gal->title}}</a>
                                 </div>
                              </div >
                           </div >
                           @endforeach
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
               <aside class="col-sm-4 aside">
                  <div class="jagoTab2">
                     <!-- Nav tabs -->
                     <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active"><a href="#tab21" aria-controls="tab21" role="tab" data-toggle="tab">সর্বশেষ</a></li>
                        <li role="presentation"><a href="#tab22" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a></li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tab21">
                           <ul class="media-list">
                              <li class="media">
                                 <div class="media-left">
                                   <a href="">
                                    <img class="media-object" src="" alt="">
                                  </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                      <a href=""></a>
                                    </h4>
                                 </div>
                              </li>
                           </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab22">
                           <ul class="media-list">
                              <li class="media">
                                 <div class="media-left">
                                   <a href=""><img class="media-object" src="" alt=""></a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading"><a href=""></a></h4>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </aside>
            </div>
         </div>
         <div class="marginBottom20">
            <div class="row">
               <div class="col-sm-12">
                  <h2 class="catTitle"><a href=""><i class="fa fa-camera"></i> বাংলাদেশ</a><span class="liner"></span></h2>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title=""><img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title=""><img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title="">
                          <img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title=""><img src="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title=""><img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title="">
                          <img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title=""><img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="single-block auto">
                     <div class="img-box">
                        <a href="" title="">
                          <img src="" alt="" class="img-responsive"></a>
                     </div>
                     <h4><a href="" title=""></a></h4>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </section>
</main>
@endsection
