@extends('layouts.app')

@section('content')
<section>
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="row">
               <div class="col-sm-12 paddingTopBottom20">
                  <div class="no-margin">
                    @foreach($tag as $tt)
                    @section('title', '{{$tt->title}}')
                     <h1 class="no-margin"> <i class="fa fa-tag" style="color:#9a1515;"></i> {{$tt->title}}</h1>
                     @endforeach
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-8 main-content">
                  <div class="white-box">
                     <div class="jagoTab2 TagTab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                           <li role="presentation" class="active"><a href="" aria-controls="news" role="tab" data-toggle="tab">খবর </a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div role="tabpanel" class="tab-pane fade in active" id="news">
                             @if (count($tag_wise) > 0)
                             @foreach($tag_wise as $tn)
                              <div class="tag-block">
                                 <div class="row">
                                    <div class="col-sm-4">
                                       <div class="tag-img">
                                          <a href='{{ url("$tn->cslug/articles/{$tn->id}/{$tn->slug}") }}'>
                                          <img alt="{{$tn->title}}" src="{{asset('media/news/'.$tn->nphoto)}}" class="img-responsive">
                                          </a>
                                          <div class="overlay-category">
                                             <a href="" rel="nofollow">{{$tn->cname}}</a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-8">
                                       <h3><a href='{{ url("$tn->cslug/articles/{$tn->id}/{$tn->slug}") }}'>{{$tn->title}}</a>
                                       </h3>
                                       <small>প্রকাশিত: {{ \App\Http\Controllers\CommonController::GetBangla(date('j M, Y, h:i', strtotime($tn->created_at)))}} </small>
                                       <p>{{$tn->thumbnail}}</p>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @else
                              <hr>
                              <div class="no-items">
                              <div class="no-items-wrapper">
                                <h1 class="text-center cup"><i class="fa fa-coffee" aria-hidden="true"></i></h1>
                                <h4 class="text-center">Great, You have no thing to do. Just take a cup of tea</h4>
                              </div>
                            </div>
                                  @endif

                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <aside class="aside col-sm-4">
                  <div class="row paddingBottom20">
                     <div class="col-sm-12 text-center">
                        <a href="" rel="nofollow" target="_blank">
                        <img src="" alt="" title="" style="display: none !important;">
                        </a>
                     </div>
                  </div>
               </aside>
            </div>
         </div>
      </div>
   </div>
</section>


@endsection
