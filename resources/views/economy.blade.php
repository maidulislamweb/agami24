@extends('layouts.app')
@section('content')
<main id="main-content">
    <section class="paddingTop20">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 main-content">
                    <div class="row">
                        <div class="col-sm-12">
                          @if(!empty($featurenews))
                          @foreach($featurenews as $featur)
                            <div class="category-lead">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="img-box">
                                            <a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="">
                                                <img alt="" src="{{asset('media/news/'.$featur->nphoto)}}" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="title">
                                            <h3><a href='{{ url("$featur->cslug/articles/{$featur->id}/{$featur->slug}") }}' title="{{$featur->title}}">{{$featur->title}}</a></h3>
                                        </div>
                                        <h4 class="lead-thumb">{{str_limit($featur->thumbnail,150)}}</h4>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        @if(!empty($economy))
                        @foreach($economy as $eco)
                        <div class="col-sm-4">
                            <div class="category-block">
                                <div class="cat-img">
                                    <a href='{{ url("$eco->cslug/articles/{$eco->id}/{$eco->slug}") }}' title="{{$eco->title}}">
																	 <img alt="{{$eco->title}}" src="{{asset('media/news/'.$eco->nphoto)}}" class="img-responsive">
                                 </a>
                                </div>
                                <div class="cat-text-box">
                                    <h3 class="cat-text-title">
                                   <a href='{{ url("$eco->cslug/articles/{$eco->id}/{$eco->slug}") }}' title="{{$eco->title}}">{{$eco->title}}</a>
                                 </h3>
                                    <h4 class="cat-thumb">{{str_limit($eco->thumbnail,100)}}</h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="simple-pagination">
                        {{$economy->links()}}
                    </div>
                </div>
                <aside class="col-sm-4 aside">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="sidebar-widget">
                                <h4 class="sidebar-title">সর্বশেষ</h4>
                                <div class="tab-content">
                                    <div role="tabpanel" class="">
                                        <ul class="media-list">
                                            @if(!empty($latest))
                                            @foreach($latest as $lat)
                                            <li class="media">
                                                <div class="media-left">
                                                    <a href='{{ url("articles/{$lat->id}/{$lat->cslug}/{$lat->slug}") }}'>
                                        <img width="100px;" class="media-object" src="{{asset('media/news/'.$lat->nphoto)}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                         <a href='{{ url("articles/{$lat->id}/{$lat->cslug}/{$lat->slug}") }}'>{{$lat->title}}</a>
                                                    </h4>
                                                </div>
                                            </li>
                                            @endforeach @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="jagoTab2 TwoTab">
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation">
                                        <a href="#tab22" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel">
                                        <ul class="media-list">
                                            <li class="media">
                                                <div class="media-left">
                                                    <a href="">
                                                        <img class="media-object" src="" alt="">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> <a href=""></a></h4>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</main>
@endsection
