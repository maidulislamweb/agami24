@extends('admin.layouts.app')
@section('title', 'Dashboard | Subscriber')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
              <section class="panel">
              <header class="panel-heading">
                Subscriber
             <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table table-responsive">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Type</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($subscriber))
                @foreach($subscriber as $sb)
              <tr class="gradeX">
                  <td>{{$sb->id}}</td>
                  <td>{{$sb->name}}</td>
                  <td>{{$sb->email}}</td>
                  <td>{{$sb->phone}}</td>
                  <td>
                    @if($sb->role == 1)
                    <span class="label label-primary" style="font-size: 11px;">Super Admin</span>
                    @endif
                    @if($sb->role == 2)
                    <span class="label label-success" style="font-size: 11px;">Admin</span>
                   @endif
                   @if($sb->role == 3)
                  <span class="label label-info" style="font-size: 11px;">Publisher</span>
                  @endif
                  @if($sb->role == 4)
                  <span class="label label-warning" style="font-size: 11px;">Subscriber</span>
                 @endif
                 </td>
                  <td>
                    <ul class="list-unstyled list-inline">
                    <li><a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                        <i class="fa fa-pencil-square-o"></i></a></li>
                        <li><a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                            <i class="fa fa-trash-o"></i></a></li>
                  </ul> </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Type</th>
                  <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<!--main content end-->
@endsection
