@extends('admin.layouts.app')
@section('title', 'Dashboard | Videos')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                Videos
             <span class="tools pull-right">
               <a class="btn" data-toggle="modal" href="#myModal3">Add Video</a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
              </tr>
              </thead>
              <tbody>
                            <tr class="gradeU">
                  <td>Other browsers</td>
                  <td>All others</td>
                  <td>-</td>
                  <td class="center">-</td>
                  <td class="center">U</td>
              </tr>
              </tbody>
              <tfoot>
              <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- Modal -->
                                           <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                           <h4 class="modal-title">Add Category</h4>
                                                       </div>
                                                       <div class="modal-body">

                                                         <form role="form">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Name</label>
                                   <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Slug</label>
                                   <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputEmail1">description</label>
                                   <textarea class="form-control" name="name" rows="8" cols="80"></textarea>
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Password</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Thumbnail</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Order By</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <button type="submit" class="btn btn-info">Submit</button>
                           </form>

                                                       </div>
                                                       <div class="modal-footer">
                                                           <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                           <!-- modal -->
              <!-- page end-->

    </section>
</section>
<!--main content end-->
@endsection
