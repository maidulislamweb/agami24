@extends('admin.layouts.app')
@section('title', 'Dashboard | Gallery')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                All Galleries
             <span class="tools pull-right">
               <a class="btn" data-toggle="modal" href="#myModal3">

                </a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                <th>Title</th>
                <th>Slug</th>
                <th>Body</th>
                <th>Thumbnail</th>
                <th>Feature</th>
                <th>Category</th>
                <th>Author</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($galleries))
                @foreach($galleries as $gal)
              <tr class="gradeC">
                  <td>{{str_limit($gal->title,20)}}</td>
                  <td>{{str_limit($gal->slug,20)}}</td>
                  <td>{{str_limit($gal->content,20)}}</td>
                  <td>{{str_limit($gal->thumbnail,20)}}</td>
                  <td><img src="{{asset('media/gallery/'.$gal->photo)}}" alt="" class="img-responsive zoom-image" width="100px"></td>
                  <td>{{$gal->cname}}</td>
                  <td>{{$gal->uname}}</td>
                  <td>{{date("d M y H:i a",strtotime($gal->created_at))}}</td>
                  <td>@if(!empty($gal->updated_at)){{date("d M y H:i a",strtotime($gal->updated_at))}} @endif</td>
                  <td>@if($gal->status == 0)
                    <a href="{{url('/gallery-publish/'.$gal->id)}}"><span class="label label-danger" style="font-size: 12px;">Pending</span></a>
                    @endif
                    @if($gal->status == 1)
                    <a href="{{url('/gallery-unpulish/'.$gal->id)}}"><span class="label label-success" style="font-size: 12px;">Published</span></a>
                   @endif
                </td>
                  <td>
                    <a href="{{url('gallery-view/'.$gal->id)}}" title="View"><i class="fa fa-heart-o"></i></a>
                    <a href="{{url('galleryEdit/'.$gal->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="{{url('galleryDelete/'.$gal->id)}}" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');">
                      <i class="fa fa-trash"></i></a>
                </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>Title</th>
                <th>Slug</th>
                <th>Body</th>
                <th>Thumbnail</th>
                <th>Feature</th>
                <th>Category</th>
                <th>Author</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- Modal -->
                                           <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                           <h4 class="modal-title">Add Category</h4>
                                                       </div>
                                                       <div class="modal-body">

                                                         <form role="form">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Name</label>
                                   <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Slug</label>
                                   <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputEmail1">description</label>
                                   <textarea class="form-control" name="name" rows="8" cols="80"></textarea>
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Password</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Thumbnail</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="exampleInputPassword1">Order By</label>
                                   <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                               </div>
                               <button type="submit" class="btn btn-info">Submit</button>
                           </form>

                                                       </div>
                                                       <div class="modal-footer">
                                                           <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                           <!-- modal -->
              <!-- page end-->

    </section>
</section>
<!--main content end-->
@endsection
