@extends('admin.layouts.app')
@section('title', 'Dashboard | Add News')
@section('content')
<!--  summernote -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datepicker/css/datepicker.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-timepicker/compiled/timepicker.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-colorpicker/css/colorpicker.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/jquery-multi-select/css/multi-select.css') }}" />
   <!--bootstrap switcher-->
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css') }}" />
   <!-- switchery-->
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/switchery/switchery.css') }}" />
   <!--select 2-->
   <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/select2/css/select2.min.css') }}"/>
   <link href="{{ URL::asset('assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<style>
</style>
<section id="main-content">
    <section class="wrapper">
      <div class="row">
           <div class="col-lg-8 col-lg-offset-2">
               <section class="panel">
                 @include('admin.layouts.message')
                   <header class="panel-heading">

                   </header>
                   <div class="panel-body">
                       <div class="form">
                           <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{ url('galleryStore') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="panel-group">
                              <div class="panel panel-primary">
                                  <div class="panel-heading">Add New Gallery Image <a class="btn pull-right" href="{{ url('/news') }}">All Photos</a></div>

                                  <div class="panel-body">
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">Title</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="title" name="title" type="text" />
                                            @if ($errors->has('title'))
                                            <p class="help-block">{{ $errors->first('title') }}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="news_slug" class="control-label col-lg-2">Slug</label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="slug" name="slug" type="text" />
                                            @if ($errors->has('slug'))
                                            <p class="help-block">{{ $errors->first('slug') }}</p>
                                            @endif
                                        </div>
                                        <label for="thumbnail" class="control-label col-lg-2">Thumbnail</label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="thumbnail" name="thumbnail" type="text" />
                                            @if ($errors->has('thumbnail'))
                                            <p class="help-block">{{ $errors->first('thumbnail') }}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="content" class="control-label col-lg-2">Body</label>
                                        <div class="col-lg-10">
                                           <textarea id="summernote" name="body" rows="3" cols="40" class="form-control"></textarea>
                                           @if ($errors->has('body'))
                                           <p class="help-block">{{ $errors->first('body') }}</p>
                                           @endif
                                        </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-primary">
                                      <div class="panel-heading">Category</div>
                                      <div class="panel-body">
                                        <div class="form-group ">
                                            <label for="category" class="control-label col-lg-2">Category</label>
                                            <div class="col-lg-4">
                                                <select class="form-control" name="category_id" id="category_id" onchange="get_subcategory(this.value);">
                                                   <option value="">Select Category</option>
                                                   @if(!empty($categoryinfo))
                                                   @foreach($categoryinfo as $category)
                                                   <option value="{{$category->id}}">{{$category->bn_name}}</option>
                                                   @endforeach
                                                   @endif
                                                </select>
                                                @if ($errors->has('category_id'))
                                                <p class="help-block">{{ $errors->first('category_id') }}</p>
                                                @endif
                                            </div>
                                            <label for="sub_category_id" class="control-label col-lg-2">Sub Category</label>
                                            <div class="col-lg-4">
                                                <select class="form-control" name="sub_category_id" id="sub_category_id">

                                                </select>
                                                @if ($errors->has('sub_category_id'))
                                                <p class="help-block">{{ $errors->first('sub_category_id') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                      </div>
                                      </div>
                                            <div class="panel panel-primary">
                                              <div class="panel-heading">Gallery Image</div>
                                              <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="photo" class="control-label col-lg-2">Featured</label>
                                                    <div class="col-lg-2">
                                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                 <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                     <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                 </div>
                                                                 <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                 <div>
                                                                  <span class="btn btn-white btn-file">
                                                                  <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                                  <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                                  <input type="file" name="photo"  class="default" />
                                                                  </span>
                                                                     <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>

                                                                 </div>
                                                                 <p class="help-text">ইমেজের সাইজ অবশ্যই 750* 450 হতে হবে</p>
                                                                 @if ($errors->has('photo'))
                                                                 <p class="help-block">{{ $errors->first('photo') }}</p>
                                                                 @endif
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                              </div>
                                              <div class="panel panel-primary">
                                                  <div class="panel-heading"> Published Date</div>
                                                  <div class="panel-body">
                                                    <div class="form-group date-time">
                                                        <label for="password" class="control-label col-lg-2">Published Date</label>
                                                        <div class="col-lg-4">
                                                            <input size="16" type="text" value="" name="publish" id="publish"  class="form_datetime form-control">
                                                            @if ($errors->has('publish'))
                                                            <p class="help-block">{{ $errors->first('publish') }}</p>
                                                            @endif
                                                        </div>
                                                        <label for="photo_caption" class="control-label col-lg-1">Tags</label>
                                                        <div class="col-lg-4">
                                                          <div class="form-group">
                                                           <select class="js-example-basic-multiple form-control" multiple="multiple" name="tags" id="tagsinput" >
                                                             @if(!empty($tags))
                                                             @foreach($tags as $tag)
                                                               <option value="{{$tag->id}}">{{$tag->title}}</option>
                                                               @endforeach
                                                               @endif
                                                           </select>
                                                    </div>
                                                  </div>
                                                  </div>

                                                            <!--div class="form-group">
                                                                <label for="meta_title" class="control-label col-lg-1">Title</label>
                                                                <div class="col-lg-5">
                                                                     <input type="text" name="meta_title" id="meta_title" class="form-control"/>
                                                                     @if ($errors->has('meta_title'))
                                                                     <p class="help-block">{{ $errors->first('meta_title') }}</p>
                                                                     @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="meta_key" class="control-label col-lg-1">Keywords</label>
                                                                <div class="col-lg-5">
                                                                     <textarea class="form-control" name="meta_key" rows="4" cols="80"></textarea>
                                                                     @if ($errors->has('meta_key'))
                                                                     <p class="help-block">{{ $errors->first('meta_key') }}</p>
                                                                     @endif
                                                                </div>
                                                                <label for="meta_description" class="control-label col-lg-1">Description</label>
                                                                <div class="col-lg-5">
                                                                     <textarea class="form-control" name="meta_description" rows="4" cols="80"></textarea>
                                                                     @if ($errors->has('meta_description'))
                                                                     <p class="help-block">{{ $errors->first('meta_description') }}</p>
                                                                     @endif
                                                                </div>
                                                            </div-->
                                                          </div>
                                                          <div class="panel-footer">
                                                            <button class="btn btn-primary" type="submit">Post</button>
                                                            <button class="btn btn-warning pull-right" type="reset">Cancel</button>
                                                          </div>
                                                          </div>
                                                          </div>
                           </form>
                       </div>
                   </div>
               </section>
           </div>
       </div>
    </section>
</section>
<!--main content end-->
<script type="text/javascript">
   $("#alert,.help-block").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert,.help-block").slideUp(500);
   });
</script>
<script>
function get_subcategory(category_id)
{
    var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
    var post_url        = '{{ url("get_subcategory")}}';

    $.ajax({
        url: post_url,
        type: 'POST',
        data: {_token: CSRF_TOKEN, category_id: category_id},
        success: function(html_data)
        {
            if (html_data != '')
            {
                $('#subcategory_id').html(html_data);

            }
        }
    });
}
function get_district(division_id)
{
    var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
    var post_url        = '{{ url("get_district")}}';

    $.ajax({
        url: post_url,
        type: 'POST',
        data: {_token: CSRF_TOKEN, division_id: division_id},
        success: function(html_data)
        {
            if (html_data != '')
            {
                $('#district_id').html(html_data);

            }
        }
    });
}
function get_upazila(district_id)
{
    var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
    var post_url        = '{{ url("get_upazila")}}';

    $.ajax({
        url: post_url,
        type: 'POST',
        data: {_token: CSRF_TOKEN, district_id: district_id},
        success: function(html_data)
        {
            if (html_data != '')
            {
                $('#upazila_id').html(html_data);

            }
        }
    });
}
</script>
<script>

     jQuery(document).ready(function(){

         $('#summernote').summernote({
             height: 200,                 // set editor height

             minHeight: null,             // set minimum height of editor
             maxHeight: null,             // set maximum height of editor

             focus: true                 // set focus to editable area after initializing summernote
         });
     });

 </script>
<!--script>


    jQuery(document).ready(function(){

        $('#summernote').summernote({
          fontNames: ['Arial','Arial Black','Comic Sans MS', 'Courier New','SolaimanLipi','SutonnyOMJ'],
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });

        $(".form_datetime").datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-left"
});
    });

</script-->
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>
<script src="{{ URL::asset('js/advanced-form-components.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/jquery-multi-select/js/jquery.multi-select.js') }}"></script>
 <script type="text/javascript" src="{{ URL::asset('assets/jquery-multi-select/js/jquery.quicksearch.js') }}"></script>
 <!--select2-->
 <script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
 <script src="{{ URL::asset('js/form-component.js') }}"></script>
 <script src="{{ URL::asset('js/jquery.tagsinput.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('assets/select2/js/select2.min.js') }}"></script>
  <!--summernote-->
  <script src="{{ URL::asset('assets/summernote/dist/summernote.min.js') }}"></script>
  <!--right slidebar-->
  <script src="{{ URL::asset('js/slidebars.min.js') }}"></script>
  <!--this page  script only-->
  <script src="{{ URL::asset('js/advanced-form-components.js') }}"></script>
  <!--bootstrap-switch-->
  <script src="{{ URL::asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') }}"></script>
  <!--bootstrap-switch-->
  <script src="{{ URL::asset('assets/switchery/switchery.js') }}"></script>
  <!--common script for all pages-->
  <script src="{{ URL::asset('js/common-scripts.js') }}"></script>
    <!--select2-->
  <script type="text/javascript">
      $(document).ready(function () {
          $(".js-example-basic-single").select2();
          $(".js-example-basic-multiple").select2();
      });
  </script>
  <!-- swithery-->
  <script type="text/javascript">
      $(document).ready(function () {
          //default
          var elem = document.querySelector('.js-switch');
          var init = new Switchery(elem);
          //small
          var elem = document.querySelector('.js-switch-small');
          var switchery = new Switchery(elem, { size: 'small' });
        //large
          var elem = document.querySelector('.js-switch-large');
          var switchery = new Switchery(elem, { size: 'large' });
          //blue color
          var elem = document.querySelector('.js-switch-blue');
          var switchery = new Switchery(elem, { color: '#7c8bc7', jackColor: '#9decff' });
        //green color
          var elem = document.querySelector('.js-switch-yellow');
          var switchery = new Switchery(elem, { color: '#FFA400', jackColor: '#ffffff' });
          //red color
          var elem = document.querySelector('.js-switch-red');
          var switchery = new Switchery(elem, { color: '#ff6c60', jackColor: '#ffffff' });
      });
  </script>
@endsection
