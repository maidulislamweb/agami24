@extends('admin.layouts.app')
@section('title', 'Dashboard | Sub Categories')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
              <section class="panel">
              <header class="panel-heading">
                All Sub-Categories
             <span class="tools pull-right">
               <a class="btn" data-toggle="modal" href="#myModal3">
                    Add Sub-category
                </a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table table-responsive">
              <table  class="display table table-bordered table-striped" id="dynamic">
              <thead>
              <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Parent</th>
                  <th>Order By</th>
                  <th class="text-center">Status</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($subCategory))
                @foreach($subCategory as $subCat)

<tr>
  <td>{{$subCat->id}}</td>
  <td>{{$subCat->name}}</td>
  <td>{{$subCat->category_name}}</td>
  <td>{{$subCat->order_id}}</td>
  <td class="text-center"><input type="checkbox" class="status" id="" data-id="{{$subCat->id}}" @if ($subCat->status) checked @endif></td>
  <td><ul class="list-unstyled list-inline">
     <li><a href="{{ route('subcategory.edit', $subCat->id) }}" class="tooltip-success" data-rel="tooltip" title="Edit">
         <i class="fa fa-pencil-square-o"></i></a>
     </li>
     <li><a  class="delete-modal"  data-delete-id="{{ $subCat->id }}"><i class="ace-icon fa fa-trash-o bigger-120"></i></a ></li>
  </ul></td>
</tr>
                @endforeach
                @endif

              </tbody>
              <tfoot>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Parent</th>
                <th>Order By</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- Modal -->
                    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                           <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Add Sub-Category</h4>
                                </div>
                                <div class="modal-body">

                                  <form method="post" action="{{ route('subcategory.store') }}" accept-charset="UTF-8">
                                     {{ csrf_field() }}
                               <div class="form-group">
                                   <label for="name">Name</label>
                                   <input type="text" name="name" class="form-control" id="name" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="slug">Slug</label>
                                   <input type="text" name="slug" class="form-control" id="slug" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="thumbnail">Category</label>
                                   <select class="form-control" name="category_id">
                                     <option value="">Select Category</option>
                                     @if(!empty($categoryinfo))
                                     @foreach($categoryinfo as $category)
                                     <option value="{{$category->id}}">{{$category->name}}</option>
                                     @endforeach
                                     @endif
                                   </select>
                               </div>
                               <div class="form-group">
                                   <label for="description">Description</label>
                                   <textarea class="form-control" id="description" name="description" rows="8" cols="80"></textarea>
                               </div>
                               <div class="form-group">
                                   <label for="order_id">Order By</label>
                                   <input type="text" class="form-control" name="order_id" id="order_id" placeholder="">
                               </div>

                               <div class="modal-footer">
                                 <button type="submit" class="btn btn-info">Submit</button>
                                      <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                             </div>
                           </form>
                                 </div>

                                                   </div>
                                               </div>
                                           </div>
                                           <!-- modal -->
              <!-- page end-->

    </section>
</section>

<!--main content end-->

  <div id="modal-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-info bigger-110">
          These items will be permanently deleted and cannot be recovered.
        </div>
        <h3 class="text-center">
          <p class="bigger-110 bolder center grey">
            <i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
            ☣Are you sure?☣
          </p>
        </h3>

        <p>Sure you want to delete this data with ID : <strong><span id="del-id"></span></strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="delete btn btn-danger" data-dismiss="modal">Delete</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


   <script>
       $(document).ready(function(){
           $('.status').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.status').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('subcatStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.status').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>



<script type="text/javascript">
$('.delete-modal').click(function() {
  $('#del-id').html($(this).data('delete-id'));
  $('#modal-delete').modal('show');
});
$('.delete').click(function() {
		$.ajax({
			url: '/sub_category/delete',
			type: 'DELETE',
			headers: {
		        'X-CSRF-TOKEN': $('#token').attr('content')
		    },
			data: {
				'id': $('#del-id').html()
			},
			success: function(result) {
				$('.tag' + result.id).remove();
				location.reload();
			},
			error: function(xhr, error) {
				alert(error);
			}
		});
	});
</script>
<script type="text/javascript">
   $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(500);
   });


</script>
@endsection
