@extends('admin.layouts.app')
@section('title', 'Dashboard | Sub Categories')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
              <section class="panel">
              <header class="panel-heading">
                All Sub-Categories
             <span class="tools pull-right">
               <a class="btn" href="{{url('subcategory')}}"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table">
                @if(!empty($subcategory))
                @foreach($subcategory as $sub)
                <form method="post" action="/subcategory/{{ $sub->id }}" accept-charset="UTF-8">
                  {{ method_field('PATCH') }}
                  <input type="hidden" name="id" class="form-control" id="id" value="{{$sub->id}}">
                   {{ csrf_field() }}
             <div class="form-group">
                 <label for="name">Name</label>
                 <input type="text" name="name" class="form-control" id="name" value="{{$sub->name}}">
                 @if ($errors->has('name'))
                 <p class="help-block">{{ $errors->first('name') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="slug">Slug</label>
                 <input type="text" name="slug" class="form-control" id="slug" value="{{$sub->slug}}">
                 @if ($errors->has('slug'))
                 <p class="help-block">{{ $errors->first('slug') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="thumbnail">Category</label>
                 <select class="form-control" name="category_id">
                   <option value="">Select Category</option>
                   @if(!empty($categoryinfo))
                   @foreach($categoryinfo as $category)
                   <option value="{{$category->id}}" {{ $sub->category_id == $category->id? 'selected' : '' }}>{{$category->name}}</option>
                   @endforeach
                   @endif
                 </select>
                 @if ($errors->has('category_id'))
                 <p class="help-block">{{ $errors->first('category_id') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="description">Description</label>
                 <textarea class="form-control" id="description" name="description" rows="3" cols="80">{{$sub->description}}</textarea>
                 @if ($errors->has('description'))
                 <p class="help-block">{{ $errors->first('description') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="order_id">Order By</label>
                 <input type="text" class="form-control" name="order_id" id="order_id"  value="{{$sub->order_id}}">
                 @if ($errors->has('order_id'))
                 <p class="help-block">{{ $errors->first('order_id') }}</p>
                 @endif
             </div>

             <div class="modal-footer">
               <button type="submit" class="btn btn-info">Update</button>
                    <button class="btn btn-danger" type="reset">Cancel</button>
           </div>

         </form>
         @endforeach
         @endif
              </div>
              </div>
              </section>
              </div>
              </div>
    </section>
</section>

<!--main content end-->

  <div id="modal-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-info bigger-110">
          These items will be permanently deleted and cannot be recovered.
        </div>
        <h3 class="text-center">
          <p class="bigger-110 bolder center grey">
            <i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
            ☣Are you sure?☣
          </p>
        </h3>

        <p>Sure you want to delete this data with ID : <strong><span id="del-id"></span></strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="delete btn btn-danger" data-dismiss="modal">Delete</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('.delete-modal').click(function() {
  $('#del-id').html($(this).data('delete-id'));
  $('#modal-delete').modal('show');
});
$('.delete').click(function() {
		$.ajax({
			url: '/sub_category/delete',
			type: 'DELETE',
			headers: {
		        'X-CSRF-TOKEN': $('#token').attr('content')
		    },
			data: {
				'id': $('#del-id').html()
			},
			success: function(result) {
				$('.tag' + result.id).remove();
				location.reload();
			},
			error: function(xhr, error) {
				alert(error);
			}
		});
	});
</script>
<script type="text/javascript">
   $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(500);
   });


</script>
@endsection
