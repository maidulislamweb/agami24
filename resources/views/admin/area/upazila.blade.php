@extends('admin.layouts.app')
@section('title', 'Area | Upazila')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
              <section class="panel">
              <header class="panel-heading">Upazila<span class="tools pull-right">
              <a class="btn" data-toggle="modal" href="#" data-toggle="modal" data-target="#create-item">Add</a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table table-container">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Bangla</th>
                  <th>District</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($upazilas))
                @foreach($upazilas as $up)
              <tr class="gradeX">
                  <td>{{$up->id}}</td>
                  <td>{{$up->name}}</td>
                  <td>{{$up->bn_name}}</td>
                  <td>{{$up->dist_name}}</td>
                  <td>@if($up->status == 0)
                    <a href="#"><span class="label label-danger" style="font-size: 11px;">Deactive</span></a>
                    @endif
                    @if($up->status == 1)
                    <a href="#"><span class="label label-success" style="font-size: 11px;">Active</span></a>
                   @endif</td>
                  <td>
                    <ul class="list-unstyled list-inline">
                    <li><a href="{{ route('upazila.edit', $up->id) }}" class="tooltip-success" data-rel="tooltip" title="Edit">
                        <i class="fa fa-pencil-square-o"></i></a></li>
                        <li><a href="#" class="delete remove-btn" href="#" data-toggle="modal" data-target="#destroy-item" value="{{ $up->id }}">
                    <i class="fa fa-trash-o"></i></a></li>
                  </ul> </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Bangla</th>
                <th>District</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>

<!-- Create item modal -->
<div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Add Upazila</h4>
</div>
<div class="modal-body">
    <form method="POST" action="{{route('upazila.store')}}" accept-charset="UTF-8">
      {{ csrf_field() }}
 <fieldset>
  <div class="form-group">
     <label for="district_id">District</label>
     <select class="form-control" name="district_id" id="district_id">
       <option value="">Select</option>
       @if(!empty($districts))
       @foreach($districts as $district)
       <option value="{{$district->id}}">{{$district->bn_name}}</option>
       @endforeach
       @endif
     </select>
  </div>
  <div class="form-group">
     <label for="name">Name</label>
     <input type="text" name="name" id="name" class="form-control">
  </div>
  <div class="form-group">
     <label for="bn_name">Bangla</label>
     <input type="text" name="bn_name" id="bn_name" class="form-control">
  </div>
  <div class="form-group">
     <label for="website">Order By</label>
     <input type="text" name="order_id" id="order_id" class="form-control">
  </div>
 </fieldset>
 <div class="modal-footer">
  <button type="submit" class="btn btn-success" id="store-submit">Save</button>
  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
 </div>
</form>
</div>
</div>
</div>
</div>
<!-- Destroy item modal -->
					<div class="modal fade" id="destroy-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">Delete</h4>
								</div>
								<div class="modal-body" id="remove-data">
									<div class="alert alert-info bigger-110">
										These items will be permanently deleted and cannot be recovered.
									</div>
									<h3 class="text-center">
										<p class="bigger-110 bolder center grey">
											<i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
											☣Are you sure?☣
										</p>
									</h3>
								</div>
								<div class="modal-footer">
									<form method="post" id="remove-form">
										<input type="hidden" name="id" id="remove-id">
										<button type="submit" class="btn btn-danger">Confirm</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</form>
								</div>
							</div>
						</div>
					</div>
<script type="text/javascript">
			$('#destroy-item').on('hidden.bs.modal', function () {
				$('#item-not-found').remove();
				$('#remove-data').show();
			});
			$(".table-container").on("click touchstart", ".remove-btn", function () {
				$("#remove-id").val($(this).attr("value"));
			});
			$("#remove-form").submit(function (e) {
				e.preventDefault();
				var CSRF_TOKEN  = $('meta[name="csrf-token"]').attr('content');
				var post_url	= '{{ url("upazila") }}';
				var msg         = $('#msg');
				var id			= $("#remove-id").val();
				$.ajax({
					type:'POST',
					dataType:'json',
					data:{_token: CSRF_TOKEN, _method: 'DELETE'},
					url: post_url + "/" + id,
					beforeSend: function() {
						$('#item-not-found').remove();
						$('#destroy-loading-bar').show();
					},
					success: function (data) {
						$success = data.responseJSON;
						$.notify({
							// options
							icon: 'fa fa-check',
							title: '<strong>Success</strong>: <br>',
							message: data['msg']
						},{
							// settings
							type: "success",
							allow_dismiss: true,
							newest_on_top: true,
							showProgressbar: false,
							placement: {
								from: "top",
								align: "right"
							},
							offset: 20,
							spacing: 10,
							z_index: 9999,
							delay: 5000,
							timer: 1000,
							mouse_over: "pause",
							animate: {
								enter: 'animated fadeInDown',
								exit: 'animated fadeOutUp'
							}
						});
						$('#destroy-item').modal('toggle');
						// refresh data
						refreshTable();
					},
					error: function(data) {
						$.notify({
							// options
							icon: 'fa fa-exclamation-triangle',
							title: '<strong>Error</strong>: <br>',
							message: 'An error occurred while getting data.'
						},{
							// settings
							type: "danger",
							allow_dismiss: true,
							newest_on_top: true,
							showProgressbar: false,
							placement: {
								from: "top",
								align: "right"
							},
							offset: 20,
							spacing: 10,
							z_index: 9999,
							delay: 5000,
							timer: 1000,
							mouse_over: "pause",
							animate: {
								enter: 'animated fadeInDown',
								exit: 'animated fadeOutUp'
							}
						});
						$('#remove-data').hide();
						(function(){
							var notFound = $('<div class="modal-body fade-text" id="item-not-found"><h1 class="text-center danger">☠</h1><h2 class="text-center">Item not found</h2></div>');
							notFound.insertAfter('#remove-data');
						})();
					},
					complete:function(data) {
					//	$('#destroy-loading-bar').hide();
					}
				});
			});
</script>
<!--main content end-->
@endsection
