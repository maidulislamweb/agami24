@extends('admin.layouts.app')
@section('title', 'Dashboard | Division Edit')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
              <section class="panel">
              <header class="panel-heading">
                Division
             <span class="tools pull-right">
               <a class="btn" href="{{url('division')}}"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
             @include('admin.layouts.message')
              </header>
              <div class="panel-body">
              <div class="adv-table">
                <form method="post" action="/division/{{ $division->id }}" accept-charset="UTF-8">
                  {{ method_field('PATCH') }}
                  <input type="hidden" name="id" class="form-control" id="id" value="{{$division->id}}">
                   {{ csrf_field() }}
             <div class="form-group">
                 <label for="name">Name</label>
                 <input type="text" name="name" class="form-control" id="name" value="{{$division->name}}">
                 @if ($errors->has('name'))
                 <p class="help-block">{{ $errors->first('name') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="name">Bengali</label>
                 <input type="text" name="bn_name" class="form-control" id="bn_name" value="{{$division->bn_name}}">
                 @if ($errors->has('bn_name'))
                 <p class="help-block">{{ $errors->first('bn_name') }}</p>
                 @endif
             </div>
             <div class="form-group">
                 <label for="order_id">Order</label>
                 <input type="text" name="order_id" class="form-control" id="order_id" value="{{$division->order_id}}">
                 @if ($errors->has('order_id'))
                 <p class="help-block">{{ $errors->first('order_id') }}</p>
                 @endif
             </div>
             <div class="modal-footer">
               <button type="submit" class="btn btn-info">Update</button>
                    <button class="btn btn-danger" type="reset">Cancel</button>
           </div>

         </form>
              </div>
              </div>
              </section>
              </div>
              </div>
    </section>
</section>
@endsection
