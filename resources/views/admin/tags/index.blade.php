@extends('admin.layouts.app')
@section('title', 'Dashboard | Tags')
@section('content')
<!--main content start-->
<section id="main-content">
   <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-sm-12 col-md-8 col-md-offset-2">
            <section class="panel">
               <header class="panel-heading">
                Tags
                  <span class="tools pull-right">
                  <a class="btn" data-toggle="modal" href="#myModal3">
                  Add New
                  </a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                  </span>
                  @include('admin.layouts.message')
               </header>
               <div class="panel-body">
                 <div id="table-loader" style="display: none">
                    <div class="col-md-12">
                        <div class="no-items loader-height">
                        <div class="no-items-wrapper">
                          <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                          <span class="sr-only">Loading...</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="adv-table table-container">
                     @include('admin.tags.table')
                  </div>
               </div>
            </section>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add Tag</h4>
               </div>
               <div class="modal-body">
                  <form method="POST" action="/tags" accept-charset="UTF-8">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="">
                     </div>
                     <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" class="form-control" id="slug" placeholder="">
                     </div>
                     <!--div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3" cols="80"></textarea>
                     </div-->
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-info">Submit</button><button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</section>
<!-- Destroy item modal -->
					<div class="modal fade" id="destroy-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">Delete</h4>
								</div>
								<div class="modal-body" id="remove-data">
									<div class="alert alert-info bigger-110">
										These items will be permanently deleted and cannot be recovered.
									</div>
									<h3 class="text-center">
										<p class="bigger-110 bolder center grey">
											<i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
											☣Are you sure?☣
										</p>
									</h3>
								</div>
								<div class="modal-footer">
									<form method="post" id="remove-form">
										<input type="hidden" name="id" id="remove-id">
										<button type="submit" class="btn btn-danger">Confirm</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					<script>
       $(document).ready(function(){
           $('.status').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.status').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('tagStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.status').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>
<script type="text/javascript">
		$('#destroy-item').on('hidden.bs.modal', function () {
				$('#item-not-found').remove();
				$('#remove-data').show();
			});
			$(".table-container").on("click touchstart", ".remove-btn", function () {
				$("#remove-id").val($(this).attr("value"));
			});
			$("#remove-form").submit(function (e) {
				e.preventDefault();
				var CSRF_TOKEN  = $('meta[name="csrf-token"]').attr('content');
				var post_url	= '{{ url("tags") }}';
				var msg         = $('#msg');
				var id			= $("#remove-id").val();
				$.ajax({
					type:'POST',
					dataType:'json',
					data:{_token: CSRF_TOKEN, _method: 'DELETE'},
					url: post_url + "/" + id,
					beforeSend: function() {
						$('#item-not-found').remove();
						$('#destroy-loading-bar').show();
					},
					success: function (data) {
						$success = data.responseJSON;
						$.notify({
							// options
							icon: 'fa fa-check',
							title: '<strong>Success</strong>: <br>',
							message: data['msg']
						},{
							// settings
							type: "success",
							allow_dismiss: true,
							newest_on_top: true,
							showProgressbar: false,
							placement: {
								from: "top",
								align: "right"
							},
							offset: 20,
							spacing: 10,
							z_index: 9999,
							delay: 5000,
							timer: 1000,
							mouse_over: "pause",
							animate: {
								enter: 'animated fadeInDown',
								exit: 'animated fadeOutUp'
							}
						});
						$('#destroy-item').modal('toggle');
						// refresh data
						refreshTable();
					},
					error: function(data) {
						$.notify({
							// options
							icon: 'fa fa-exclamation-triangle',
							title: '<strong>Error</strong>: <br>',
							message: 'An error occurred while getting data.'
						},{
							// settings
							type: "danger",
							allow_dismiss: true,
							newest_on_top: true,
							showProgressbar: false,
							placement: {
								from: "top",
								align: "right"
							},
							offset: 20,
							spacing: 10,
							z_index: 9999,
							delay: 5000,
							timer: 1000,
							mouse_over: "pause",
							animate: {
								enter: 'animated fadeInDown',
								exit: 'animated fadeOutUp'
							}
						});
						$('#remove-data').hide();
						(function(){
							var notFound = $('<div class="modal-body fade-text" id="item-not-found"><h1 class="text-center danger">☠</h1><h2 class="text-center">Item not found</h2></div>');
							notFound.insertAfter('#remove-data');
						})();
					},
					complete:function(data) {
					//	$('#destroy-loading-bar').hide();
					}
				});
			});
</script>
<!--main content end-->

@endsection
