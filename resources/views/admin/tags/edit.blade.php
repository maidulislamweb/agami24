@extends('admin.layouts.app')
@section('title', 'Dashboard | Tags')
@section('content')
<!--main content start-->
<section id="main-content">
   <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-sm-12 col-md-6 col-md-offset-3">
            <section class="panel">
               <header class="panel-heading">
                Tags
                  <span class="tools pull-right">
                  <a class="btn"  href="{{route('tags.index')}}"><i class="fa fa-undo" aria-hidden="true"></i>Back</a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                  </span>
                  @include('admin.layouts.message')
               </header>
               <div class="panel-body">
                  <div class="adv-table table-container">

                    <form method="post" action="/tags/{{ $tag->id }}" accept-charset="UTF-8">
                      {{ method_field('PATCH') }}
                      <input type="hidden" name="id" class="form-control" id="id" value="{{$tag->id}}">
                       {{ csrf_field() }}
                       <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" name="title" class="form-control" id="title" value="{{$tag->title}}">
                          @if ($errors->has('title'))
                          <p class="help-block">{{ $errors->first('title') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="slug">Slug</label>
                          <input type="text" name="slug" class="form-control" id="slug" value="{{$tag->slug}}">
                          @if ($errors->has('slug'))
                          <p class="help-block">{{ $errors->first('slug') }}</p>
                          @endif
                       </div>

                       <!--div class="form-group">
                          <label for="description">Description</label>
                          <textarea class="form-control" id="description" name="description" rows="3" cols="80">{{$tag->description}}</textarea>
                          @if ($errors->has('description'))
                          <p class="help-block">{{ $errors->first('description') }}</p>
                          @endif
                       </div-->
                       <div class="modal-footer">
                          <button type="submit" class="btn btn-info">Submit</button><button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                       </div>
                    </form>

                  </div>
               </div>
            </section>
         </div>
      </div>
      <!-- page end-->
   </section>
</section>
<!--main content end-->
<script type="text/javascript">
   $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
       $(".alert-danger").slideUp(500);
   });

</script>
@endsection
