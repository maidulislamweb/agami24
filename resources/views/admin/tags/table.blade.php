<table  class="display table table-bordered table-striped" id="dynamic-table">
   <thead>
      <tr>
         <th>ID</th>
         <th>Title</th>
         <th>Slug</th>
         <th>Status</th>
         <th>Action</th>
      </tr>
   </thead>
   <tbody>
      @if(!empty($tags))
      @foreach($tags as $tag)
      <tr class="gradeX">
         <td>{{$tag->id}}</td>
         <td>{{$tag->title}}</td>
         <td>{{$tag->slug}}</td>
         <td class="text-center"><input type="checkbox" class="status" id="" data-id="{{$tag->id}}" @if ($tag->status) checked @endif></td>
         
         <td>
            <ul class="list-unstyled list-inline">
              <li><a href="{{ route('tags.edit', $tag->id) }}" class="tooltip-success" data-rel="tooltip" title="Edit">
                  <i class="fa fa-pencil-square-o"></i></a>
             </li>
               <li><a href="#" class="delete remove-btn" href="#" data-toggle="modal" data-target="#destroy-item" value="{{ $tag->id }}">
       <i class="fa fa-trash-o"></i></a>
                 </li>
            </ul>
         </td>
      </tr>
      @endforeach
      @endif
   </tbody>
   <tfoot>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Slug</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
   </tfoot>
</table>
