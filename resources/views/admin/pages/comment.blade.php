@extends('admin.layouts.app')
@section('title', 'Comments')
@section('content')
<!--  summernote -->
<link href="{{ URL::asset('assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}" />
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
      <div class="row">
           <div class="col-lg-8 col-lg-offset-2">
               <section class="panel">
                   <header class="panel-heading">
                     Comment

                   </header>
                   <div class="panel-body">
                       <div class="form">
                           <form class="cmxform form-horizontal tasi-form" id="signupForm" method="get" action="#">
                               <div class="form-group ">
                                   <label for="username" class="control-label col-lg-2">Code to use after the opening <body> tag *</label>
                                   <div class="col-lg-10">
                                      <textarea id="" name="name" rows="8" cols="80" class="form-control" value=""> <code><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=323620764400430";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script></code>  </textarea>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="col-lg-offset-2 col-lg-10">
                                       <button class="btn btn-danger" type="submit">Post</button>
                                       <button class="btn btn-default" type="button">Cancel</button>
                                   </div>
                               </div>
                           </form>
                       </div>
                   </div>
               </section>
           </div>
       </div>

    </section>
</section>
<!--main content end-->

<script>

    jQuery(document).ready(function(){

        $('#summernote').summernote({
            height: 200,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true                 // set focus to editable area after initializing summernote
        });

        $(".form_datetime").datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-left"

});
    });

</script>
 <script type="text/javascript" src="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ URL::asset('js/advanced-form-components.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
@endsection
