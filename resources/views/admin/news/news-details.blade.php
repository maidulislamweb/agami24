@extends('layouts.app')
@section('title', '')
@section('content')
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
      @if (count($newsDetails) > 0)
      @foreach($newsDetails as $news)
      <main id="main-content">
         <section class="paddingTop20">
            <div class="container">
               <div class="row">
                  <div class="col-sm-8 main-content">
                     <div class="row">
                        <div class="col-lg-9 col-lg-push-3 col-md-push-0">
                           <div class="box-white marginBottom20 visible-xs hidden-print">
                              <ol class="breadcrumb">
                                 <li><a href=""><i class="fa fa-home text-danger"></i></a></li>
                                 <li><a href="">দেশজুড়ে</a></li>
                              </ol>
                           </div>
                           <article class="box-white">
                              <div class="padding15 box-white">
                                 <h1 class="no-margin">{{$news->title}}</h1>
                                 <div class="dividerDetails"></div>
                                 <div class="media">
                                    <div class="media-left">
                                       <img alt="{{$news->designation}}" src="{{asset('media/author/'.$news->uphoto)}}" class="media-object img-responsive" width="80px">
                                    </div>
                                    <div class="media-body">
                                       <span class="small text-muted time-with-author">
                                       <i class="fa fa-pencil"></i>
                                       <a href="" style="display:inline-block;" rel="nofollow">{{$news->designation}}</a>
                                       <span> <i class="fa fa-map-marker text-danger"></i></span>
                                       <br>
                                       <i class="fa fa fa-clock-o text-danger"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="paddingTop10 ">
                                 <div class="gallery">
                                   <a href="{{asset('media/news/'.$news->n_photo)}}" data-source="{{asset('media/news/'.$news->n_photo)}}" title="">
  		                                  <img src="{{asset('media/news/'.$news->n_photo)}}"  style="width:100%;">
  	                               </a>
                                 </div>
                              </div>
                              <div class="content-details">
                                {!!$news->content!!}
                              </div>
                              <hr>
                              <div class="padding15 hidden-print">
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <h2 class="catTitle">
                                          আরও পড়ুন
                                          <span class="liner"></span>
                                       </h2>
                                    </div>
                                 </div>
                              </div>
                           </article>
                        </div>
                        <div class="col-lg-3 col-lg-pull-9 col-md-pull-0 hidden-print">
                           <div class="box-white marginBottom20 hidden-xs">
                              <ol class="breadcrumb">
                                 <li><a href=""><i class="fa fa-home text-danger"></i></a></li>
                                 <li><a href="">দেশজুড়ে</a></li>
                              </ol>
                           </div>
                           <div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <aside class="col-sm-4 aside hidden-print">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="jagoTab2 ThreeTab">
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs nav-justified" role="tablist">
                                 <li role="presentation" class="active">
                                   <a href="" aria-controls="tab1" role="tab" data-toggle="tab" class="cat_more">দেশজুড়ে এর আরও খবর</a></li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane fade in active" id="tab1">
                                    <ul class="media-list">
                                       <li class="media">
                                          <div class="media-left">
                                             <a href="">
                                               <img alt="" class="media-object" src=""></a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading"><a href=""></a></h4>
                                          </div>
                                       </li>
                                       </ul>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="jagoTab2 TwoTab">
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs nav-justified" role="tablist">
                                 <li role="presentation" class="active"><a href="" aria-controls="tab21" role="tab" data-toggle="tab">সর্বশেষ</a></li>
                                 <li role="presentation"><a href="" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a></li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane fade in active" id="tab21">
                                    <ul class="media-list">
                                       <li class="media">
                                          <div class="media-left">

                                               <img class="media-object" src="" alt=""></a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading">
                                               <a href=""></a></h4>
                                          </div>
                                       </li>

                                    </ul>
                                 </div>
                                 <div role="tabpanel" class="tab-pane" id="tab22">
                                    <ul class="media-list">
                                       <li class="media">
                                          <div class="media-left">
                                            <a href=""><img class="media-object" src="" alt=""></a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading">
                                               <a href=""></a>
                                             </h4>
                                          </div>
                                       </li>
                                       </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </aside>
               </div>
            </div>
         </section>
      </main>
      @endforeach
      @else
      <hr>
      <div class="no-items">
         <div class="no-items-wrapper">
            <!--img src="{{ asset('media/imgAll/animated-tea-and-teapot-image-0041.gif') }}" alt="" class="" width="50px"-->
            <h1 class="text-center cup"><i class="fa fa-coffee" aria-hidden="true"></i></h1>
            <h4 class="text-center">Great, You have no thing to do. Just take a cup of tea</h4>
         </div>
      </div>
      @endif
      <script src="{{ asset('js/magnific-popup.min.js') }}"></script>
      <script type="text/javascript">
	$('.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank"></a>';
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}

	});
      </script>
@endsection
