@extends('admin.layouts.app')
@section('title', 'Dashboard | News Post')
@section('content')
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
                <a class="btn pull-right" href="{{ url('/addNews') }}">Add News</a>
              <header class="panel-heading">All Posts</header>
              <div class="panel-body">
                @include('admin.layouts.message')
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>News Title</th>
                  <th>News Slug</th>
                  <th>Body</th>
                  <th>Feature</th>
                  <th>Category</th>
                  <th>Author</th>
                  <th>Published Date</th>
                  <th>Updated</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($news))
                @foreach($news as $news)
              <tr class="gradeC">
                  <td>{{str_limit($news->title,20)}}</td>
                  <td>{{str_limit($news->slug,20)}}</td>
                  <td>{{str_limit($news->content,20)}}</td>
                  <td><img src="{{asset('media/news/'.$news->photo)}}" alt="" class="img-responsive zoom-image" width="100px"></td>
                  <td>{{$news->cname}}</td>
                  <td>{{$news->uname}}</td>
                  <td>{{date("d M y H:i a",strtotime($news->created_at))}}</td>
                  <td>@if(!empty($news->updated_at)){{date("d M y H:i a",strtotime($news->updated_at))}} @endif</td>
                  <td>@if($news->status == 0)
                    <a href="{{url('/news-publish/'.$news->id)}}"><span class="label label-danger" style="font-size: 12px;">Pending</span></a>
                    @endif
                    @if($news->status == 1)
                    <a href="{{url('/news-unpulish/'.$news->id)}}"><span class="label label-success" style="font-size: 12px;">Published</span></a>
                   @endif
                </td>
                  <td>
                    <a href="{{url('/news-view/'.$news->id)}}" title="View"><i class="fa fa-heart-o"></i></a>
                    <a href="{{url('/news-edit/'.$news->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="{{url('/news-delete/'.$news->id)}}" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');">
                      <i class="fa fa-trash"></i>
                    </a>
                </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>News Title</th>
                <th>News Slug</th>
                <th>News Content</th>
                <th>Featured Photo</th>
                <th>Category</th>
                <th>Author</th>
                <th>Published Date</th>
                <th>Updated</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
