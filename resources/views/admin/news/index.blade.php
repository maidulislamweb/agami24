@extends('admin.layouts.app')
@section('title', 'Dashboard | News Post')
@section('content')
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
                <a class="btn pull-right" href="{{ url('/addNews') }}">Add News</a>
              <header class="panel-heading">All Posts</header>
              <div class="panel-body">
                @include('admin.layouts.message')
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id=""><!--dynamic-table-->
              <thead>
              <tr>
                <th>Title</th>
                <th>Content</th>
                <th>Category</th>
                <th>Author</th>
                <th>Updated</th>
                <th>is_feature</th>
                <th>is_lead</th>
                <th>is_published</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($news))
                @foreach($news as $new)
              <tr class="gradeC">
                  <td>{{str_limit($new->title,20)}}</td>
                  <td>{{str_limit($new->content,20)}}</td>
                  <td>{{$new->cname}}</td>
                  <td>{{$new->uname}}</td>
                  <td> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->updated_at)->diffForHumans() }}
                  </td>
                  <td class="text-center">
                    <input type="checkbox" class="feature" id="" data-id="{{$new->id}}" @if ($new->is_feature) checked @endif>
                  </td>
                    <td class="text-center">
                      <input type="checkbox" class="leadnews" id="" data-id="{{$new->id}}" @if ($new->is_leadnews) checked @endif>
                    </td>
                  <td class="text-center">
                    <input type="checkbox" class="status" id="" data-id="{{$new->id}}" @if ($new->status) checked @endif>
                  </td>
                  <td>
                    <a href="{{url('/news-view/'.$new->id)}}" title="View"><i class="fa fa-heart-o"></i></a>
                    <a href="{{url('/news-edit/'.$new->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="{{url('/news-delete/'.$new->id)}}" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');">
                      <i class="fa fa-trash"></i>
                    </a>
                </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>Title</th>
                <th>Content</th>
                <th>Category</th>
                <th>Author</th>
                <th>Updated</th>
                <th>is_feature</th>
                <th>is_lead</th>
                <th>is_published</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              <div class="simple-pagination">
                  {{$news->links()}}
              </div>
              </div>

              </div>
              </section>
              </div>
              </div>
              <!-- page end-->
    </section>
</section>
<!--main content end-->
<script>
       $(window).load(function(){
           $('#postTable').removeAttr('style');
       })
   </script>

   <script>
       $(document).ready(function(){
           $('.feature').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.feature').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('featureStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.feature').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>
   <script>
       $(document).ready(function(){
           $('.leadnews').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.leadnews').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('leadnewsStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.leadnews').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>
   <script>
       $(document).ready(function(){
           $('.status').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.status').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('changeStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.status').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>
@endsection
