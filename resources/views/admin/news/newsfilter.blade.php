@extends('admin.layouts.app')
@section('title', 'Dashboard | News Post')
@section('content')
<!--main content start-->
<style>
.zoom-image:hover {
  -webkit-transition: all .3s ease;
  -moz-transition: all .3s ease;
  -o-transition: all .3s ease;
  -ms-transition: all .3s ease;
  transition: all .3s ease;
}
</style>
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">User Wise News</header>
              <div class="panel-body">
                <div class="form">
                    <form class="form-horizontal" method="post" action="{{ url('newsfilterView') }}" accept-charset="UTF-8">
                       {{ csrf_field() }}
                       <div class="panel-group">
                       <div class="panel panel-primary">
                           <div class="panel panel-primary">
                               <div class="panel-body">
                                 <div class="form-group ">
                                     <div class="col-lg-3">
                                       <label for="user_id" class="control-label">User</label>
                                         <select class="form-control" name="user_id" id="user_id">
                                            <option value="">Select</option>
                                            @if(!empty($users))
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->bn_name}}</option>
                                            @endforeach
                                            @endif
                                         </select>
                                         @if ($errors->has('user_id'))
                                         <p class="help-block">{{ $errors->first('user_id') }}</p>
                                         @endif
                                     </div>
                                     <div class="col-lg-3">
                                       <label for="category_id" class="control-label">Category</label>
                                         <select class="form-control" name="category_id" id="category_id" onchange="get_subcategory(this.value);">
                                            <option value="">Select</option>
                                            @if(!empty($categoryinfo))
                                            @foreach($categoryinfo as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                            @endif
                                         </select>
                                         @if ($errors->has('category_id'))
                                         <p class="help-block">{{ $errors->first('category_id') }}</p>
                                         @endif
                                     </div>

                                     <div class="col-lg-3">
                                       <label for="sub_category_id" class="control-label">Sub Category</label>
                                         <select class="form-control" name="sub_category_id" id="sub_category_id">
                                         </select>
                                         @if ($errors->has('sub_category_id'))
                                         <p class="help-block">{{ $errors->first('sub_category_id') }}</p>
                                         @endif
                                     </div>
                                     <div class="col-lg-3">
                                       <label for="district_id" class="control-label">District Wise</label>
                                         <select class="form-control" name="district_id" id="district_id">
                                           <option value="">Select</option>
                                           @if(!empty($districts))
                                           @foreach($districts as $district)
                                           <option value="{{$district->id}}">{{$district->bn_name}}</option>
                                           @endforeach
                                           @endif
                                         </select>
                                         @if ($errors->has('district_id'))
                                         <p class="help-block">{{ $errors->first('district_id') }}</p>
                                         @endif
                                     </div>
                                 </div>
                                 <div class="form-group ">
                                     <div class="col-lg-3">
                                       <label for="user_id" class="control-label">Date</label>
                                         <select class="form-control" name="user_id" id="user_id">
                                            <option value="">Select</option>
                                            @if(!empty($users))
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->bn_name}}</option>
                                            @endforeach
                                            @endif
                                         </select>
                                         @if ($errors->has('user_id'))
                                         <p class="help-block">{{ $errors->first('user_id') }}</p>
                                         @endif
                                     </div>
                                     <div class="col-lg-3">
                                       <label for="category_id" class="control-label">Month</label>
                                         <select class="form-control" name="category_id" id="category_id" onchange="get_subcategory(this.value);">
                                            <option value="">Select</option>
                                            @if(!empty($categoryinfo))
                                            @foreach($categoryinfo as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                            @endif
                                         </select>
                                         @if ($errors->has('category_id'))
                                         <p class="help-block">{{ $errors->first('category_id') }}</p>
                                         @endif
                                     </div>

                                     <div class="col-lg-3">
                                       <label for="sub_category_id" class="control-label">Year</label>
                                         <select class="form-control" name="sub_category_id" id="sub_category_id">
                                         </select>
                                         @if ($errors->has('sub_category_id'))
                                         <p class="help-block">{{ $errors->first('sub_category_id') }}</p>
                                         @endif
                                     </div>
                                 </div>
                               </div>
                               </div>
                               <div class="panel panel-success">
                              <div class="panel-footer">
                                <button class="btn btn-primary" type="submit">View</button>
                                <button class="btn btn-warning" type="reset">Cancel</button>
                              </div>
                              </div>
                              </div>
                    </form>
                </div>
              </div>
            </section>
              </div>


                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">All Posts</header>
              <div class="panel-body">
                @include('admin.layouts.message')
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>News Title</th>
                  <th>News Slug</th>
                  <th>Body</th>
                  <th>Feature</th>
                  <th>Category</th>
                  <th>Author</th>
                  <th>Published Date</th>
                  <th>Updated</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($news))
                @foreach($news as $news)
              <tr class="gradeC">
                  <td>{{str_limit($news->title,20)}}</td>
                  <td>{{str_limit($news->slug,20)}}</td>
                  <td>{{str_limit($news->content,20)}}</td>
                  <td>
                    <img src="{{asset('media/news/'.$news->photo)}}" alt="" class="img-responsive zoom-image" width="50px"></td>
                  <td>{{$news->cname}}</td>
                  <td>{{$news->uname}}</td>
                  <td>{{date("d M y H:i a",strtotime($news->created_at))}}</td>
                  <td>@if(!empty($news->updated_at)){{date("d M y H:i a",strtotime($news->updated_at))}} @endif</td>
                  <td>@if($news->status == 0)
                    <a href="{{url('/news-publish/'.$news->id)}}"><span class="label label-danger" style="font-size: 12px;">Pending</span></a>
                    @endif
                    @if($news->status == 1)
                    <a href="{{url('/news-unpulish/'.$news->id)}}"><span class="label label-success" style="font-size: 12px;">Published</span></a>
                   @endif
                </td>
                  <td>
                    <a href="{{url('/news-view/'.$news->id)}}" title="View"><i class="fa fa-heart-o"></i></a>
                    <a href="{{url('/news-edit/'.$news->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="{{url('/news-delete/'.$news->id)}}" title="Delete" class="text-danger" onclick="return confirm('Are you sure you want to delete this item?');">
                      <i class="fa fa-trash"></i></a>
                </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>News Title</th>
                <th>News Slug</th>
                <th>News Content</th>
                <th>Feature</th>
                <th>Category</th>
                <th>Author</th>
                <th>Published Date</th>
                <th>Updated</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--script type="text/javascript">
   $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
       $(".alert-danger").slideUp(500);
   });

</script-->
<script>
function get_subcategory(category_id)
{
    var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
    var post_url        = '{{ url("get_subcategory")}}';

    $.ajax({
        url: post_url,
        type: 'POST',
        data: {_token: CSRF_TOKEN, category_id: category_id},
        success: function(html_data)
        {
            if (html_data != '')
            {
                $('#sub_category_id').html(html_data);

            }
        }
    });
}
   function get_subcategory(category_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_subcategory")}}';
       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, category_id: category_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#subcategory_id').html(html_data);

               }
           }
       });
   }
   function get_district(division_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_district")}}';
       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, division_id: division_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#district_id').html(html_data);

               }
           }
       });
   }
   function get_upazila(district_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_upazila")}}';
       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, district_id: district_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#upazila_id').html(html_data);

               }
           }
       });
   }
</script>
@endsection
