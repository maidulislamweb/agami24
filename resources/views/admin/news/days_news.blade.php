@extends('admin.layouts.app')
@section('title', 'Dashboard | News Post')
@section('content')
<!--main content start-->
<style>
.zoom-image:hover {
  -webkit-transition: all .3s ease;
  -moz-transition: all .3s ease;
  -o-transition: all .3s ease;
  -ms-transition: all .3s ease;
  transition: all .3s ease;
}
</style>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datepicker/css/datepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-timepicker/compiled/timepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-colorpicker/css/colorpicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">Day Wise News</header>
              <div class="panel-body">
                <div class="form">
                    <form class="form-horizontal" method="post" action="{{ url('newsfilterView') }}" accept-charset="UTF-8">
                       {{ csrf_field() }}
                       <div class="panel-group">
                       <div class="panel panel-primary">
                           <div class="panel panel-primary">
                               <div class="panel-body">
                                 <div class="form-group ">
                                     <div class="col-lg-3">
                                       <label for="user_id" class="control-label">Select date</label>
                                          <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                          <input type="text" readonly="" value="12-02-2012" size="16" class="form-control">
                                              <span class="input-group-btn add-on">
                                                <button class="btn btn-danger" type="button"><i class="fa fa-calendar"></i></button>
                                              </span>
                                      </div>
                                      <span class="help-block"></span>
                                     </div>

             
                                 </div>

                               </div>
                               </div>
                               <div class="panel panel-success">
                              <div class="panel-footer">
                                <button class="btn btn-primary" type="submit">View</button>
                                <button class="btn btn-warning" type="reset">Cancel</button>
                              </div>
                              </div>
                              </div>
                    </form>
                </div>
              </div>
            </section>
              </div>


                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">All Posts</header>
              <div class="panel-body">
                @include('admin.layouts.message')
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>News Title</th>
                  <th>News Slug</th>
                  <th>Body</th>
                  <th>Feature</th>
                  <th>Category</th>
                  <th>Author</th>
                  <!--th>Published Date</th>
                  <th>Updated</th>
                  <th>Status</th>
                  <th>Action</th-->
              </tr>
              </thead>
              <tbody>
                @if(!empty($news))
                @foreach($news as $new)
              <tr class="gradeC">
                  <td>{{str_limit($new->title,20)}}</td>
                  <td>{{str_limit($new->slug,20)}}</td>
                  <td>{{str_limit($new->content,20)}}</td>
                  <td>
                    <img src="{{asset('media/news/'.$new->photo)}}" alt="" class="img-responsive zoom-image" width="50px"></td>
                  <td>{{$new->cname}}</td>
                  <td>{{$new->uname}}</td>
                  <!--td>{{date("d M y H:i a",strtotime($new->created_at))}}</td>
                  <td>@if(!empty($new->updated_at)){{date("d M y H:i a",strtotime($new->updated_at))}} @endif</td>
                  <td>@if($new->status == 0)
                    <a href="{{url('/news-publish/'.$new->id)}}"><span class="label label-danger" style="font-size: 12px;">Pending</span></a>
                    @endif
                    @if($new->status == 1)
                    <a href="{{url('/news-unpulish/'.$new->id)}}"><span class="label label-success" style="font-size: 12px;">Published</span></a>
                   @endif
                </td>
                  <td>
                    <a href="{{url('/news-view/'.$new->id)}}" title="View"><i class="fa fa-heart-o"></i></a>
                    <a href="{{url('/news-edit/'.$new->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="{{url('/news-delete/'.$new->id)}}" title="Delete" class="text-danger" onclick="return confirm('Are you sure you want to delete this item?');">
                      <i class="fa fa-trash"></i></a>
                </td-->
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>News Title</th>
                <th>News Slug</th>
                <th>News Content</th>
                <th>Feature</th>
                <th>Category</th>
                <th>Author</th>
                <!--th>Published Date</th>
                <th>Updated</th>
                <th>Status</th>
                <th>Action</th-->
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              <div class="simple-pagination">
                        {{$news->links()}}
                    </div>
              </section>
              </div>
              </div>
              <!-- page end-->

    </section>
</section>
<!--this page  script only-->
  <script src="{{ URL::asset('js/advanced-form-components.js') }}"></script>
    <script src="{{ URL::asset('js/respond.min.js') }}" ></script>
  <script src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ URL::asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
  <script  src="{{ URL::asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script>
  <script  src="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script  src="{{ URL::asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
  <script src="{{ URL::asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript">

      $(document).ready(function () {
          $(".js-example-basic-single").select2();

          $(".js-example-basic-multiple").select2();
      });
  </script>

  <!--bootstrap swither-->
  <script type="text/javascript">
      $(document).ready(function () {
          // Resets to the regular style
          $('#dimension-switch').bootstrapSwitch('setSizeClass', '');
          // Sets a mini switch
          $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-mini');
          // Sets a small switch
          $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-small');
          // Sets a large switch
          $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-large');


          $('#change-color-switch').bootstrapSwitch('setOnClass', 'success');
          $('#change-color-switch').bootstrapSwitch('setOffClass', 'danger');
      });
  </script>

  <!-- swithery-->
  <script type="text/javascript">
      $(document).ready(function () {
          //default
          var elem = document.querySelector('.js-switch');
          var init = new Switchery(elem);


          //small
          var elem = document.querySelector('.js-switch-small');
          var switchery = new Switchery(elem, { size: 'small' });

          //large
          var elem = document.querySelector('.js-switch-large');
          var switchery = new Switchery(elem, { size: 'large' });


          //blue color
          var elem = document.querySelector('.js-switch-blue');
          var switchery = new Switchery(elem, { color: '#7c8bc7', jackColor: '#9decff' });

          //green color
          var elem = document.querySelector('.js-switch-yellow');
          var switchery = new Switchery(elem, { color: '#FFA400', jackColor: '#ffffff' });

          //red color
          var elem = document.querySelector('.js-switch-red');
          var switchery = new Switchery(elem, { color: '#ff6c60', jackColor: '#ffffff' });


      });
  </script>
@endsection
