@extends('admin.layouts.app')
@section('title', 'Dashboard | Add News')
@section('content')
<!--  summernote -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/select2/css/select2.min.css') }}"/>
<!--  summernote -->
<link href="{{ URL::asset('assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<style></style>
<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
            <section class="panel">
               <header class="panel-heading">
                  <a href="{{ url('/tags') }}">Add Tag</a>
                  <a class="btn pull-right" href="{{ url('/news') }}">All News</a>
                  @include('admin.layouts.message')
               </header>
               <div class="panel-body">
                  <div class="form">
                     <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{ url('postInsert') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="panel-group">
                           <div class="panel panel-primary">
                              <div class="panel-body">
                                 <div class="form-group row">
                                    <div class="col-lg-8">
                                       <label for="firstname" class="control-label">News Title</label>
                                       <input class=" form-control"  name="title" type="text" required/>
                                       @if ($errors->has('news_title'))
                                       <p class="help-block">{{ $errors->first('title') }}</p>
                                       @endif
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                       <label for="thumbnail" class="control-label">Thumbnail</label>
                                       <textarea  name="thumbnail" rows="2" class="form-control" required></textarea>
                                       @if ($errors->has('thumbnail'))
                                       <p class="help-block">{{ $errors->first('thumbnail') }}</p>
                                       @endif
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <div class="col-md-12">
                                       <label for="content" class="control-label">News Content</label>
                                       <textarea id="summernote" name="content" rows="8" class="form-control" required></textarea>
                                       @if ($errors->has('detail'))
                                       <p class="help-block">{{ $errors->first('content') }}</p>
                                       @endif
                                    </div>
                                 </div>
                              </div>
                              <div class="panel panel-primary">
                                 <div class="panel-body">
                                   <div class="form-group row">
                                      <div class="col-md-6">
                                         <label for="category" class="control-label">Category</label>
                                         <select class="form-control" name="category_id" id="category_id" onchange="get_subcategory(this.value);" required>
                                            <option value="">Select Category</option>
                                            @if(!empty($categoryinfo))
                                            @foreach($categoryinfo as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                            @endif
                                         </select>
                                         @if ($errors->has('category_id'))
                                         <p class="help-block">{{ $errors->first('category_id') }}</p>
                                         @endif
                                      </div>
                                      <div class="col-lg-6">
                                         <label for="subcategory_id" class="control-label">Sub Category</label>
                                         <select class="form-control" name="subcategory_id" id="sub_category_id">
                                         </select>
                                         @if ($errors->has('sub_category_id'))
                                         <p class="help-block">{{ $errors->first('subcategory_id') }}</p>
                                         @endif
                                      </div>
                                   </div>
                                    <div class="form-group row">
                                       <div class="col-md-4">
                                          <label for="category" class="control-label">Division</label>
                                          <select class="form-control" name="division_id" id="division_id" onchange="get_district(this.value);">
                                             <option value="">বিভাগ</option>
                                             @if(!empty($divisions))
                                             @foreach($divisions as $division)
                                             <option value="{{$division->id}}">{{$division->bn_name}}</option>
                                             @endforeach
                                             @endif
                                          </select>
                                          @if ($errors->has('division_id'))
                                          <p class="help-block">{{ $errors->first('division_id') }}</p>
                                          @endif
                                       </div>
                                       <div class="col-md-4">
                                          <label for="district_id" class="control-label">জিলা</label>
                                          <select class="form-control" name="district_id" id="district_id" onchange="get_upazila(this.value);">
                                             <option value="">Select District</option>
                                          </select>
                                          @if ($errors->has('district_id'))
                                          <p class="help-block">{{ $errors->first('district_id') }}</p>
                                          @endif
                                       </div>
                                       <div class="col-lg-4">
                                          <label for="upazila_id" class="control-label">উপজেলা</label>
                                          <select class="form-control" name="upazila_id" id="upazila_id" >
                                          </select>
                                          @if ($errors->has('upazila_id'))
                                          <p class="help-block">{{ $errors->first('upazila_id') }}</p>
                                          @endif
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <div class="col-md-2">
                                          <label for="photo" class="control-label">Featured Photo</label>
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <div class="fileupload-new thumbnail" style="width: 150px; height: auto;">
                                                <img src="{{asset('media/logo/logo.png')}}" alt="" class="img-responsive" />
                                             </div>
                                             <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                                             </div>
                                             <div>
                                                <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="photo"  class="default" required/>
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                             </div>
                                             @if ($errors->has('photo'))
                                             <p class="help-block">{{ $errors->first('photo') }}</p>
                                             @endif
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <label for="photo_caption" class="control-label">Caption</label>
                                          <input type="text" name="caption"class="form-control">
                                          @if ($errors->has('caption'))
                                          <p class="help-block">{{ $errors->first('caption') }}</p>
                                          @endif
                                       </div>

                                        <div class="col-lg-4">
                                          <label for="reporter_id" class="control-label">প্রতিনিধি</label>
                                          <select class="form-control" name="reporter_id" id="reporter_id" required>
                                              <option value="">Select</option>
                                             @if(!empty($users))
                                             @foreach($users as $user)
                                             <option value="{{$user->id}}">{{$user->designation}}</option>
                                             @endforeach
                                             @endif
                                          </select>
                                          @if ($errors->has('reporter_id'))
                                          <p class="help-block">{{ $errors->first('reporter_id') }}</p>
                                          @endif
                                       </div>


                                    </div>
                                    <div class="form-group date-time row">
                                       <div class="col-md-4">
                                          <label for="feature" class="control-label">Is Feature</label>
                                          <input name="is_feature" id="radio-01" value="1" type="radio" />Yes
                                          <input name="is_feature" id="radio-02" value="0" type="radio" checked/>No
                                       </div>
                                       <div class="col-md-4">
                                          <label for="is_leadnews" class="control-label">Lead News</label>
                                          <input name="is_leadnews" id="radio-01" value="1" type="radio"  />Yes
                                          <input name="is_leadnews" id="radio-02" value="0" type="radio" checked/>No
                                       </div>
                                       <div class="col-md-4">
                                          <label for="editor_choice" class="control-label">Editor Choice</label>
                                          <input name="editor_choice" id="editor_choice" value="0" type="checkbox"/>Yes
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel panel-success">
                                 <div class="panel-heading">Keyword and Description Area</div>
                                 <div class="panel-body">
                                    <div class="form-group row">
                                       <div class="col-md-6">
                                          <label for="tags" class="control-label">Tags</label>
                                          <div class="form-group">
                                             <select class="js-example-basic-multiple" multiple="multiple" name="tags[]" id="tagsinput" required >
                                                @if(!empty($tags))
                                                @foreach($tags as $tag)
                                                <option value="{{$tag->id}}">{{$tag->title}} ({{$tag->slug}})</option>
                                                @endforeach
                                                @endif
                                             </select>
                                          </div>
                                          @if ($errors->has('tags'))
                                          <p class="help-block">{{ $errors->first('tags') }}</p>
                                          @endif
                                       </div>
                                       <!--div class="col-md-6">
                                          <label for="meta_title" class="control-label">Title</label>
                                          <input type="text" name="meta_title" id="meta_title" class="form-control"/>
                                          @if ($errors->has('meta_title'))
                                          <p class="help-block">{{ $errors->first('meta_title') }}</p>
                                          @endif
                                       </div-->
                                    </div>
                                    <div class="form-group row">
                                       <div class="col-md-6">
                                          <label for="meta_key" class="control-label">Keywords</label>
                                          <textarea class="form-control" name="meta_key" rows="4" cols="80"></textarea>
                                          @if ($errors->has('meta_key'))
                                          <p class="help-block">{{ $errors->first('meta_key') }}</p>
                                          @endif
                                       </div>
                                       <div class="col-md-6">
                                          <label for="meta_description" class="control-label">Description</label>
                                          <textarea class="form-control" name="meta_description" rows="4" cols="80"></textarea>
                                          @if ($errors->has('meta_description'))
                                          <p class="help-block">{{ $errors->first('meta_description') }}</p>
                                          @endif
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-footer">
                                    <button class="btn btn-primary" type="submit">Post</button>
                                    <button class="btn btn-warning pull-right" type="reset">Cancel</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<!--main content end-->
<script>
   function get_subcategory(category_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_subcategory")}}';

       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, category_id: category_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#sub_category_id').html(html_data);

               }
           }
       });
   }
   function get_district(division_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_district")}}';

       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, division_id: division_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#district_id').html(html_data);

               }
           }
       });
   }
   function get_upazila(district_id)
   {
       var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
       var post_url        = '{{ url("get_upazila")}}';

       $.ajax({
           url: post_url,
           type: 'POST',
           data: {_token: CSRF_TOKEN, district_id: district_id},
           success: function(html_data)
           {
               if (html_data != '')
               {
                   $('#upazila_id').html(html_data);

               }
           }
       });
   }
</script>
<script>
   jQuery(document).ready(function(){

       $('#summernote').summernote({
         fontNames: ['Arial','Arial Black','Comic Sans MS', 'Courier New','SolaimanLipi','SutonnyOMJ'],
           height: 200,                 // set editor height
           minHeight: null,             // set minimum height of editor
           maxHeight: null,             // set maximum height of editor
           focus: true                 // set focus to editable area after initializing summernote
       });
       $('#summernote').summernote('code', 'html_tags_string_from_db');

       $(".form_datetime").datetimepicker({
   format: 'yyyy-mm-dd hh:ii',
   autoclose: true,
   todayBtn: true,
   pickerPosition: "bottom-left"
   });
   });

</script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ URL::asset('js/jquery.tagsinput.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/select2/js/select2.min.js') }}"></script>
<!--summernote-->
<script src="{{ URL::asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ URL::asset('js/advanced-form-components.js') }}"></script>
<!--select2-->
<script type="text/javascript">
   $(document).ready(function () {
       $(".js-example-basic-single").select2();
       $(".js-example-basic-multiple").select2();
   });
</script>
@endsection
