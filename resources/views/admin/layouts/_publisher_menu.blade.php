<div id="sidebar"  class="nav-collapse ">
   <!-- sidebar menu start-->
   <ul class="sidebar-menu" id="nav-accordion">
      <li><a class="active" href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-user-o"></i><span>Users</span></a>
         <ul class="sub">
            <li class="sub-menu">
               <a  href="#">Profile Info</a>
               <ul class="sub">
                   <li><a href="{{url('profile')}}">Profile</a></li>
               </ul>
               </li>
         </ul>
      </li>
      <li><a href="{{ url('/tags') }}" ><i class="fa fa-tags"></i><span>Tags</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-tasks"></i><span>News</span></a>
         <ul class="sub">
            <li><a  href="{{ url('addNews') }}">Add New</a></li>
            <li><a  href="{{ url('mypost') }}">News</a></li>
         </ul>
      </li>
   </ul>
   <!-- sidebar menu end-->
</div>