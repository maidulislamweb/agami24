<div class="" id="alert">
  @if(Session::has('info_message'))
    <div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Warning! </strong> {{ Session::get('info_message') }}.
    </div>
    @endif
    @if(Session::has('success_message'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success! </strong> {{ Session::get('success_message') }}.
    </div>
    @endif
</div>
