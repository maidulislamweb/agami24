<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="keyword" content="">
      <link rel="shortcut icon" type="image/png" href="img/favicon-16x16.png">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>@yield('title')</title>
      <!-- Bootstrap core CSS -->
      <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/bootstrap-reset.css') }}" rel="stylesheet">
      <!--external css-->
      <link href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
          <!--right slidebar-->
      <link href="{{ URL::asset('css/slidebars.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="{{ URL::asset('assets/data-tables/DT_bootstrap.css') }}" />
      <link href="{{ URL::asset('css/toastr.min.css') }}" rel="stylesheet">
      <!-- For Checkbox Active inactive -->
      <link rel="stylesheet" href="{{ asset('icheck/square/yellow.css') }}">
      <!-- Custom styles for this template -->
      <link href="{{ URL::asset('css/style2.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet" />
      <!-- js placed at the end of the document so the pages load faster -->
      <script src="{{ URL::asset('js/jquery.js') }}"></script>
      <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
      <script>
       window.Laravel = {!! json_encode([
           'csrfToken' => csrf_token(),
       ]) !!};
   </script>
   </head>
   <body>
      <section id="container">
         <!--header start-->
         <header class="header white-bg">
            <div class="sidebar-toggle-box">
               <i class="fa fa-bars"></i>
            </div>
            <!--logo start-->
            <a href="{{url('/admin')}}" class="logo">News<span></span></a>
            <!--logo end-->
            <div class="top-nav ">
               <!--search & user info start-->
               <ul class="nav pull-right top-menu">
                  <li>
                     <input type="text" class="form-control search" placeholder="Search">
                  </li>
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                     <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                       @if(!empty(Auth::user()->photo))
                        <img width="30px" class="nav-user-photo" alt="{{ Auth::user()->name }}" id="avatar2" src="{{asset('media/author/'.Auth::user()->photo)}}" />
                        @else
                      <img width="30px" class="nav-user-photo" alt="{{ Auth::user()->name }}" id="avatar2" src="{{ Gravatar::src(Auth::user()->email, 200) }}" />
                     @endif
                        <span class="username">{{ Auth::user()->name }}</span>
                        <b class="caret"></b>

                     </a>
                     <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="{{url('profile')}}"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <li><a href="{{url('profile-activity')}}"><i class="fa fa-cog"></i>Activity</a></li>
                        <li><a href="{{url('')}}"><i class="fa fa-user"></i>Role</a></li>
                        <li>
                           <a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                           Logout
                           </a>
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                           </form>
                        </li>
                     </ul>
                  </li>
                 <li class="sb-toggle-right">
                     <i class="fa  fa-align-right"></i>
                  </li>
                  <!-- user login dropdown end -->
               </ul>
               <!--search & user info end-->
            </div>
         </header>
         <!--header end-->
         <!--sidebar start-->
         <aside>
            @includeWhen(Auth::user()->role==1, 'admin.layouts._super_admin_menu')
            @includeWhen(Auth::user()->role==2, 'admin.layouts._admin_menu')
            @includeWhen(Auth::user()->role==3, 'admin.layouts._publisher_menu')
         </aside>
         <!--sidebar end-->
         <!--Main COntent Here-->
         @yield('content')
         <!-- Right Slidebar start -->
         <!--div class="sb-slidebar sb-right sb-style-overlay">
            <h5 class="side-title">Online Customers</h5>
            <ul class="quick-chat-list">
               <li class="online">
                  <div class="media">
                     <a href="#" class="pull-left media-thumb">
                     <img alt="" src="img/chat-avatar2.jpg" class="media-object">
                     </a>
                     <div class="media-body">
                        @if('Auth::user()')
                        <strong> {{ Auth::user()->name }}</strong>
                        <small></small>
                        @endif
                     </div>
                  </div>

               </li>
            </ul>
            </ul>
         </div-->
         <!-- Right Slidebar end -->
         <!--footer start-->
         <footer class="site-footer">
            <div class="text-center">
               2018 &copy; Newspaper by ABC Tech.
               <a href="#" class="go-top">
               <i class="fa fa-angle-up"></i>
               </a>
            </div>
         </footer>
         <!--footer end-->
      </section>
      <!-- icheck checkboxes -->
   <script type="text/javascript" src="{{ asset('icheck/icheck.min.js') }}"></script>
      <script class="include" type="text/javascript" src="{{ URL::asset('js/jquery.dcjqaccordion.2.7.js') }}"></script>
      <script src="{{ URL::asset('js/jquery.scrollTo.min.js') }}"></script>
      <script src="{{ URL::asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
      <!--script src="{{ URL::asset('js/jquery.sparkline.js') }}" type="text/javascript"></script-->
      <!--script src="{{ URL::asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script-->
      <script src="{{ URL::asset('js/jquery.customSelect.min.js') }}" ></script>
      <script src="{{ URL::asset('js/respond.min.js') }}" ></script>
      <!--right slidebar-->
      <script src="{{ URL::asset('js/slidebars.min.js') }}"></script>
      <!--common script for all pages-->
      <script src="{{ URL::asset('js/common-scripts.js') }}"></script>
      <!--script for this page-->
      <!--script src="{{ URL::asset('js/sparkline-chart.js') }}"></script>
      <script src="{{ URL::asset('js/easy-pie-chart.js') }}"></script-->
      <!--script src="{{ URL::asset('js/count.js') }}"></script-->
      <script src="{{ URL::asset('js/bootstrap-notify.min.js') }}"></script>
      <script src="{{ URL::asset('js/toastr.min.js') }}"></script>
      <!--dynamic table initialization -->
      <script src="{{ URL::asset('js/dynamic_table_init.js') }}"></script>
      <script src="{{ URL::asset('assets/advanced-datatable/media/js/jquery.dataTables.js') }}"></script>
      <script src="{{ URL::asset('assets/data-tables/DT_bootstrap.js') }}"></script>
<script>
function refreshTable() {
	$('div.table-container').hide();
	$('#table-loader').hide();
	$('#table-loader').fadeIn();
	$('div.table-container').load('table', function() {
		$('#table-loader').hide();
		$('div.table-container').fadeIn();
	});
}

jQuery(document).ready(function($)  {
	$('#update-form').hide();
	$('#view-data').hide();
	$('#table-loader').hide();
	$('#destroy-loading-bar').hide();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	// refresh button
	$("#refresh").bind("click touchstart", function () {
		refreshTable();
	});
});


</script>

      <script>
         //custom select box

         $(function(){
             $('select.styled').customSelect();
         });

      </script>
      <script type="text/javascript">
         $("#alert").fadeTo(2000, 500).slideUp(500, function(){
             $("#alert").slideUp(500);
         });
      </script>
   </body>
</html>
