<div id="sidebar"  class="nav-collapse ">
   <!-- sidebar menu start-->
   <ul class="sidebar-menu" id="nav-accordion">
      <li><a class="active" href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-user-o"></i><span>Users</span></a>
         <ul class="sub">
            <li><a  href="{{ url('/user') }}">All User</a></li>
            <li><a href="{{ url('/subscriber') }}">Subscriber</a></li>
            <li class=""><a href="{{ url('/userAdd') }}"></i>Add User</a></li>
            <li class="sub-menu">
               <a  href="boxed_page.html">Profile Info</a>
               <ul class="sub">
                   <li><a href="{{url('profile')}}">Profile</a></li>
                   <li><a href="{{url('profile-activity')}}">Recent Activity</a></li>
                   <li><a href="{{url('profile-edit')}}">Update Profile</a></li>
               </ul>
               </li>
         </ul>
      </li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-map"></i><span>Area</span></a>
         <ul class="sub">
            <li><a  href="{{ url('division') }}">Division</a></li>
            <li><a  href="{{ url('district') }}">District</a></li>
            <li><a  href="{{ url('upazila') }}">Upazila</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-th-large"></i><span>Category</span></a>
         <ul class="sub">
            <li><a  href="{{ url('category') }}">Category</a></li>
            <li><a  href="{{ route('subcategory.index') }}">Sub Category</a></li>
         </ul>
      </li>
      <li><a href="{{ url('/tags') }}" ><i class="fa fa-tags"></i><span>Tags</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-tasks"></i><span>News</span></a>
         <ul class="sub">
            <li><a  href="{{ url('addNews') }}">Add New</a></li>
            <li><a  href="{{ url('days_news') }}">Day Wise News</a></li>
            <li><a  href="{{ url('allnews') }}">All News</a></li>
            <li><a  href="{{ url('mypost') }}">My Post</a></li>
            <li><a  href="{{ url('draft_post') }}">Unpublished Post</a></li>
            <li><a  href="{{ url('newsfilter') }}">News Filter</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-camera"></i><span>Gallery</span></a>
         <ul class="sub">
            <li><a  href="{{ url('gallery_add') }}">Add Photos</a></li>
            <li><a  href="{{ url('gallery') }}">Photos</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a href="{{ url('google-analytics') }}"><i class="fa fa-line-chart" aria-hidden="true"></i>Google Analytics</a </li>
      <li class="sub-menu"><a href="{{ url('/home_info') }}" ><i class="fa fa-pie-chart"></i><span>SEO Meta</span></a></li>
      <li><a href="{{ url('/advertisement') }}" ><i class="fa fa-ravelry"></i><span>Advertisement</span></a></li>
      <!--multi level menu start-->
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-cogs"></i><span>Settings</span></a>
         <ul class="sub">
            <li><a  href="{{ url('/company_info') }}">Company Info</a></li>           
         </ul>
      </li>
      <!--multi level menu end-->
   </ul>
   <!-- sidebar menu end-->
</div>
