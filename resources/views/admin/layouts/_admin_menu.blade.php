<div id="sidebar"  class="nav-collapse ">
   <!-- sidebar menu start-->
   <ul class="sidebar-menu" id="nav-accordion">
      <li><a class="active" href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-user-o"></i><span>Users</span></a>
         <ul class="sub">
            <li><a href="{{ url('/subscriber') }}">Subscriber</a></li>
            <li class="sub-menu">
               <a  href="boxed_page.html">Profile Info</a>
               <ul class="sub">
                   <li><a href="{{url('profile')}}">Profile</a></li>
                   <li><a href="{{url('profile-activity')}}">Recent Activity</a></li>
                   <li><a href="{{url('profile-edit')}}">Update Profile</a></li>
               </ul>
               </li>
         </ul>
      </li>
      <li><a href="{{ url('/tags') }}" ><i class="fa fa-tags"></i><span>Tags</span></a></li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-tasks"></i><span>News</span></a>
         <ul class="sub">
            <li><a  href="{{ url('addNews') }}">Add New</a></li>
            <li><a  href="{{ url('news') }}">News</a></li>
            <li><a  href="{{ url('mypost') }}">My Post</a></li>
            <li><a  href="{{ url('draft_post') }}">Unpublished Post</a></li>
            <li><a  href="{{ url('newsfilter') }}">News Filter</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-camera"></i><span>Gallery</span></a>
         <ul class="sub">
            <li><a  href="{{ url('gallery_add') }}">Add Photos</a></li>
            <li><a  href="{{ url('gallery') }}">Photos</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a href="javascript:;" ><i class="fa fa-line-chart"></i><span>Analytics</span></a>
         <ul class="sub">
            <li><a  href="{{ url('google-analytics') }}">Google Analytics</a></li>
         </ul>
      </li>
   </ul>
   <!-- sidebar menu end-->
</div>
