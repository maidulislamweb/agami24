@extends('admin.layouts.app')
@section('title', 'Dashboard | Users')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
              <section class="panel">
                 @include('admin.layouts.message')
              <header class="panel-heading">
                Home Page Meta Element
             <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
                <form class="form-horizontal user-form" method="POST" action="{{ url('updateMeta') }}" style="padding: 15px 30px" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if(!empty($meta))
                    @foreach($meta as $meta)
                    <input type="hidden" name="id" value="{{$meta->id}}">
                                <div class="form-group">
                                    <label for="meta_title" class="col-lg-2 col-sm-2 control-label">Meta Title</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="title" id="meta_title" class="form-control" value="{{$meta->title}}">
                                        <!--p class="help-block">Example block-level help text here.</p-->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="meta_key" class="col-lg-2 col-sm-2 control-label">Meta Keyward</label>
                                    <div class="col-lg-10">
                                      <textarea name="keyward" class="form-control" id="meta_key" rows="8" cols="80">{{$meta->keyward}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="meta_des" class="col-lg-2 col-sm-2 control-label">Meta description</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" name="description" rows="8" id="meta_des" cols="80">{{$meta->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="meta_des" class="col-lg-2 col-sm-2 control-label">Author</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="author" value="{{$meta->author}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="meta_des" class="col-lg-2 col-sm-2 control-label">Developer</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="developer" value="{{$meta->developer}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="logo" class="col-lg-2 col-sm-2 control-label">Logo</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="file" name="logo">
                                    </div>
                                    <div class="col-lg-6">
                                      @if(!empty($meta->logo))
                          <img class="img-responsive" src="{{asset('media/logo/'.$meta->logo)}}" width="80px"/>
                          @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </form>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<!--main content end-->
@endsection
