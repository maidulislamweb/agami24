@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                All User
             <span class="tools pull-right">
               <a class="btn btn-success" data-toggle="modal" href="#myModal3">
                    Add New User
                </a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Photo</th>
                  <th>Type</th>
                  <th>Status</th>
              </tr>
              </thead>
              <tbody>
              <tr class="gradeX">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
              </tbody>
              <tfoot>
              <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Photo</th>
                  <th>Type</th>
                  <th>Status</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- Modal -->
                      <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                             <div class="modal-content">
                                 <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                     <h4 class="modal-title">Add User</h4>
                                  </div>
                                  <div class="modal-body">
                                <form role="form">
                               <div class="form-group">
                                   <label for="name">Name</label>
                                   <input type="text" class="form-control" id="name" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="phone">Phone</label>
                                   <input type="email" class="form-control" id="phone" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="photo">Photo</label>
                                   <input type="file" class="form-control" id="photo" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="email">Email</label>
                                   <input type="email" class="form-control" id="email" placeholder="">
                               </div>
                               <div class="form-group">
                                   <label for="user_type">User Type</label>
                                  <select class="form-control" name="user_type">
                                    <option value="1">Super Admin</option>
                                    <option value="2">Admin</option>
                                    <option value="3">Publisher</option>
                                    <option value="4">User</option>
                                  </select>
                               </div>
                               <div class="form-group">
                                   <label for="">Is Active</label>
                                   <input type="radio" name="active" value="1">Yes
                                  <input type="radio" name="active" value="0">
                                  No
                               </div>
                               <div class="modal-footer">
                                  <button type="submit" class="btn btn-info">Submit</button>
                                   <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                               </div>
                           </form>

                                                       </div>

                                                   </div>
                                               </div>
                                           </div>
                                           <!-- modal -->
              <!-- page end-->

    </section>
</section>
<!--main content end-->
@endsection
