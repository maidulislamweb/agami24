@extends('admin.layouts.app')
@section('title', 'Dashboard | Users')
@section('content')
<link href="{{ URL::asset('assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}" />

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
              <section class="panel">
                 @include('admin.layouts.message')
              <header class="panel-heading">
                Company Info
             <!--span class="tools pull-right">
               <a class="btn" data-toggle="modal" href="{{ url('registration') }}">New User</a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span-->
              </header>
              <div class="panel-body">
              <div class="adv-table">
                <form class="form-horizontal" id="signupForm" method="post" action="{{ url('companyInfo_save') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                     {{ csrf_field() }}
                     <input type="hidden" name="id" class="form-control" id="id" value="{{$company[0]->id}}">
                      <div class="form-group ">
                          <label for="name" class="control-label col-lg-2">Name</label>
                          <div class="col-lg-4">
                              <input class=" form-control" id="name" name="name" type="text"  value="{{$company[0]->name}}"/>
                              @if ($errors->has('name'))
                              <p class="help-block">{{ $errors->first('name') }}</p>
                              @endif
                          </div>
                          <label for="title" class="control-label col-lg-2">Title</label>
                          <div class="col-lg-4">
                              <input class="form-control" id="title" name="title" type="text"  value="{{$company[0]->title}}" />
                              @if ($errors->has('title'))
                              <p class="help-block">{{ $errors->first('title') }}</p>
                              @endif
                          </div>
                      </div>
                      <div class="form-group ">

                          <label for="thumbnail" class="control-label col-lg-2">Thumbnail</label>
                          <div class="col-lg-4">
                              <input class=" form-control" id="thumbnail" name="thumbnail" type="text"  value="{{$company[0]->thumbnail}}" />
                              @if ($errors->has('thumbnail'))
                              <p class="help-block">{{ $errors->first('thumbnail') }}</p>
                              @endif
                          </div>
                          <label for="founder" class="control-label col-lg-2">Founder</label>
                          <div class="col-lg-4">
                            <input type="text" name="founder"  class="form-control"  value="{{$company[0]->founder}}">
                            @if ($errors->has('founder'))
                            <p class="help-block">{{ $errors->first('founder') }}</p>
                            @endif
                          </div>
                      </div>

                      <div class="form-group ">

                          <label for="editor" class="control-label col-lg-2">Editor</label>
                          <div class="col-lg-4">
                            <input type="text" name="editor"  class="form-control"  value="{{$company[0]->editor}}">
                            @if ($errors->has('editor'))
                            <p class="help-block">{{ $errors->first('editor') }}</p>
                            @endif
                          </div>
                          <label for="password" class="control-label col-lg-2">Contact No</label>
                          <div class="col-lg-4">
                              <input type="text" name="contact"  class="form-control"  value="{{$company[0]->contact}}">
                              @if ($errors->has('contact'))
                              <p class="help-block">{{ $errors->first('contact') }}</p>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="support" class="control-label col-lg-2">Support No</label>
                          <div class="col-lg-4">
                              <input type="text" name="support"  class="form-control"  value="{{$company[0]->support}}">
                              @if ($errors->has('support'))
                              <p class="help-block">{{ $errors->first('support') }}</p>
                              @endif
                          </div>
                          <label for="website" class="control-label col-lg-2">Webite</label>
                          <div class="col-lg-4">
                              <input type="text" name="website"  class="form-control"  value="{{$company[0]->website}}">
                              @if ($errors->has('website'))
                              <p class="help-block">{{ $errors->first('website') }}</p>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="establish" class="control-label col-lg-2">Establish</label>
                          <div class="col-lg-4">
                              <input type="text" name="establish"  class="form-control"  value="{{$company[0]->establish}}">
                              @if ($errors->has('establish'))
                              <p class="help-block">{{ $errors->first('establish') }}</p>
                              @endif
                          </div>
                          <label for="password" class="control-label col-lg-2">Email</label>
                          <div class="col-lg-4">
                              <input type="text"name="email"  class="form-control"  value="{{$company[0]->email}}">
                              @if ($errors->has('email'))
                              <p class="help-block">{{ $errors->first('email') }}</p>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Facebook</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="facebook" value="{{$company[0]->facebook}}">
                          @if ($errors->has('facebook'))
                          <p class="help-block">{{ $errors->first('facebook') }}</p>
                          @endif
                        </div>
                        <label class="col-sm-2 control-label">Twitter</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="twitter" value="{{$company[0]->twitter}}">
                          @if ($errors->has('twitter'))
                          <p class="help-block">{{ $errors->first('twitter') }}</p>
                          @endif
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">LinkedIn</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="linkedin" value="{{$company[0]->linkedin}}">
                          @if ($errors->has('linkedin'))
                          <p class="help-block">{{ $errors->first('linkedin') }}</p>
                          @endif
                        </div>
                        <label class="col-sm-2 control-label">Google Plus</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="googleplus" value="{{$company[0]->googleplus}}">
                          @if ($errors->has('googleplus'))
                          <p class="help-block">{{ $errors->first('googleplus') }}</p>
                          @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Pinterest</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="pinterest" value="{{$company[0]->pinterest}}">
                          @if ($errors->has('pinterest'))
                          <p class="help-block">{{ $errors->first('pinterest') }}</p>
                          @endif
                        </div>
                        <label class="col-sm-2 control-label">YouTube</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="youtube" value="{{$company[0]->youtube}}">
                          @if ($errors->has('youtube'))
                          <p class="help-block">{{ $errors->first('youtube') }}</p>
                          @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Instagram</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="instagram" value="{{$company[0]->instagram}}">
                          @if ($errors->has('instagram'))
                          <p class="help-block">{{ $errors->first('instagram') }}</p>
                          @endif
                        </div>
                        <label class="col-sm-2 control-label">Flickr</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" name="flickr" value="{{$company[0]->flickr}}">
                          @if ($errors->has('flickr'))
                          <p class="help-block">{{ $errors->first('flickr') }}</p>
                          @endif
                        </div>
                      </div>

                      <div class="form-group ">
                          <label for="logo" class="control-label col-lg-2">Featured Photo</label>
                          <div class="col-lg-4">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                       <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                           <img src="{{asset('media/logo/'.$company[0]->logo)}}" alt="" />
                                       </div>
                                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                       <div>
                                        <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" name="logo"  class="default" />
                                        </span>
                                           <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                       </div>
                                       @if ($errors->has('logo'))
                                       <p class="help-block">{{ $errors->first('logo') }}</p>
                                       @endif
                                   </div>

                          </div>
                          <label for="photo_caption" class="control-label col-lg-2">Favicon</label>
                          <div class="col-lg-4">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                       <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                           <img src="{{asset('media/logo/'.$company[0]->favicon)}}" alt="" />
                                       </div>
                                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                       <div>
                                        <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" name="favicon"  class="default" />
                                        </span>
                                           <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                       </div>
                                       @if ($errors->has('favicon'))
                                       <p class="help-block">{{ $errors->first('favicon') }}</p>
                                       @endif
                                   </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="address" class="control-label col-lg-2">Address</label>
                          <div class="col-lg-10">
                             <textarea id="summernote" name="address" rows="8" cols="80" class="" value="">{{$company[0]->address}}</textarea>
                             @if ($errors->has('address'))
                             <p class="help-block">{{ $errors->first('address') }}</p>
                             @endif
                          </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                          <button type="submit" class="btn btn-primary" name="form1">Update</button>
                          <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                      </div>
                  </form>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<script type="text/javascript" src="{{ URL::asset('assets/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>
<script>
    jQuery(document).ready(function(){
        $('#summernote').summernote({
          fontNames: ['Arial','Arial Black','Comic Sans MS', 'Courier New','SolaimanLipi','SutonnyOMJ'],
            height: 120,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });
    });
</script>
<script src="{{ URL::asset('assets/summernote/dist/summernote.min.js') }}"></script>
<!--main content end-->
@endsection
