@extends('admin.layouts.app')
@section('title','')
@section('content')

<div class="main-content">
   <style>
   .help-block{
      color: red;
   }
</style>
   <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state clearfix" id="breadcrumbs">
         <ul class="breadcrumb">
            <li>
               <i class="ace-icon fa fa-tachometer"></i>
               <a href="">Dashboard</a>
            </li>
            <li class="active">Register User</li>
         </ul>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <div class="widget-box">
                  <div class="" id="alert">
  @if(Session::has('info_message'))
    <div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Warning! </strong> {{ Session::get('info_message') }}.
    </div>
    @endif
    @if(Session::has('success_message'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success! </strong> {{ Session::get('success_message') }}.
    </div>
    @endif
</div>
                  <div class="widget-header">
                     <h4 class="widget-title">Register</h4>
                     <div class="widget-toolbar">
                        <span class="pull-right">
                          <a href="{{url('/user-edit/'.$user->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a>
                        </span>
                     </div>
                  </div>
                  <div class="widget-body">
                     <div class="widget-main">
                        <div class="">
                          <div class="center-block profile-picture">

                                                   @if(!empty($user->photo))
                                                   <img width="100px" class="img-responsive" alt="{{ $user->name }}" src="{{asset('media/author/'.$user->photo)}}" />
                                                   @else
                                                <img width="100px" class="img-responsive" alt="{{ $user->name }}" src="{{ Gravatar::src($user->email, 200) }}" />
                                                @endif

                                                <div class="space space-4"></div>

                                              </div>
                                              <h3 class="blue">
                                                 <span class="middle">{{ $user->name }}</span>
                                                 <span class="label label-purple arrowed-in-right">
                                                 <i class="ace-icon fa fa-circle-thin align-middle" aria-hidden="true" style="background-color:#42B72A;"></i>
                                                 online
                                                 </span>
                                              </h3>
                          <h4 class="header blue">User Info</h4>
                                                     <table class="table table-bordered">
                                                        <tr>
                                                           <th>Email</th>
                                                           <td>{{ $user->email }}</td>
                                                        </tr>
                                                        <tr>
                                                           <th>User name</th>
                                                           <td>{{ $user->username }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>Address</td>
                                                           <td>{{ $user->address }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>Phone</td>
                                                           <td>{{ $user->phone }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>User Type</td>
                                                           <td>@if($user->role == 1)
                                                           <span>Super Admin</span>
                                                           @endif
                                                           @if($user->role == 2)
                                                           <span>Admin</span>
                                                          @endif
                                                          @if($user->role == 3)
                                                         <span>Publisher</span>
                                                         @endif
                                                         @if($user->role == 4)
                                                         <span>Subscriber</span>
                                                        @endif
                                                      </td>
                                                        </tr>
                                                     </table>
                                                     <h4 class="header blue">Device Info</h4>
                                                     <table class="table table-bordered">
                                                        <tr>
                                                           <th>Device Name</th>
                                                           <td>{{ $user->device }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>IP Address</td>
                                                           <td>{{ $user->ip }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>Port</td>
                                                           <td>{{ $user->port }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>Device UserName</td>
                                                           <td>{{ $user->device_user }}</td>
                                                        </tr>
                                                        <tr>
                                                           <td>Device Password</td>
                                                           <td>{{ $user->device_password }}</td>
                                                        </tr>

                                                     </table>



                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /.row -->
</div>
<!-- /.page-content -->
</div>
<!-- /.main-content -->
@endsection
