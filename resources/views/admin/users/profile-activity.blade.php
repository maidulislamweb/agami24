@extends('admin.layouts.app')
@section('title', 'Profile | Activity')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <aside class="profile-nav col-lg-3">
              <section class="panel">
                 @include('admin.layouts.message')
                  <div class="user-heading round">
                      <a href="#">
                          <img src="{{ Gravatar::src(Auth::user()->email, 200) }}" alt="">
                      </a>
                      <h1>{{ Auth::user()->name }}</h1>
                      <p>{{ Auth::user()->email }}</p>
                  </div>

                  <ul class="nav nav-pills nav-stacked">
                      <li class="active"><a href="{{url('profile')}}"> <i class="fa fa-user"></i> Profile</a></li>
                      <li><a href="{{url('profile-activity')}}"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-danger pull-right r-activity">9</span></a></li>
                      <li><a href="{{url('profile-edit')}}"> <i class="fa fa-edit"></i> Edit profile</a></li>
                  </ul>

              </section>
            </aside>
            <aside class="profile-info col-lg-9">
                <section class="panel">
                    <div class="panel-body profile-activity">
                        <h5 class="pull-right">12 August 2013</h5>
                        <div class="activity terques">
                            <span>
                                <i class="fa fa-shopping-cart"></i>
                            </span>
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="arrow"></div>
                                        <i class=" fa fa-time"></i>
                                        <h4>10:45 AM</h4>
                                        <p>Purchased new equipments for zonal office setup and stationaries.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="activity alt purple">
                            <span>
                                <i class="fa fa-rocket"></i>
                            </span>
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="arrow-alt"></div>
                                        <i class=" fa fa-time"></i>
                                        <h4>12:30 AM</h4>
                                        <p>Lorem ipsum dolor sit amet consiquest dio</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="activity blue">
                            <span>
                                <i class="fa fa-bullhorn"></i>
                            </span>
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="arrow"></div>
                                        <i class=" fa fa-time"></i>
                                        <h4>10:45 AM</h4>
                                        <p>Please note which location you will consider, or both. Reporting to the VP <br> of Compliance and CSR, you will be responsible for managing.. </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="activity alt green">
                            <span>
                                <i class="fa fa-beer"></i>
                            </span>
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="arrow-alt"></div>
                                        <i class=" fa fa-time"></i>
                                        <h4>12:30 AM</h4>
                                        <p>Please note which location you will consider, or both.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </aside>
        </div>

        <!-- page end-->
    </section>
</section>
@endsection
