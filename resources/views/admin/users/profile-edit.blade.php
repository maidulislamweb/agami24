@extends('admin.layouts.app')
@section('title', 'Profile | Edit')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <aside class="profile-nav col-lg-3">
              <section class="panel">
                 @include('admin.layouts.message')
                  <div class="user-heading round">
                      <a href="#">
                        @if(!empty(Auth::user()->photo))
                      <img width="100px" class="img-responsive" alt="{{ Auth::user()->name }}" src="{{asset('media/author/'.Auth::user()->photo)}}" />
                      @else
                    <img width="100px" class="img-responsive" alt="{{ Auth::user()->name }}" src="{{ Gravatar::src(Auth::user()->email, 200) }}" />
                    @endif
                      </a>
                      <h1>{{ Auth::user()->name }}</h1>
                      <p>{{ Auth::user()->email }}</p>
                  </div>

                  <ul class="nav nav-pills nav-stacked">
                      <li class="active"><a href="{{url('profile')}}"> <i class="fa fa-user"></i> Profile</a></li>
                      <li><a href="{{url('profile-activity')}}"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-danger pull-right r-activity">9</span></a></li>
                      <li><a href="{{url('profile-edit')}}"> <i class="fa fa-edit"></i> Edit profile</a></li>
                  </ul>

              </section>
            </aside>
            <aside class="profile-info col-lg-9">
                <section class="panel">
                    <div class="panel-body bio-graph-info">
                        <h1> Profile Info</h1>
                        <form class="user-form form-horizontal" method="POST" action="{{url('profile-update')}}" style="padding: 15px 30px" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Name</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="f-name" name="name" value="{{ Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Bangla Name</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="" name="bn_name"  value="{{ Auth::user()->bn_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Display Name</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="" name="display_name"  value="{{ Auth::user()->display_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">User Name</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="" name="username"  value="{{ Auth::user()->username }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="" name="email" value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Phone</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{ Auth::user()->phone }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Designation</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="" name="designation" value="{{ Auth::user()->designation }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Address</label>
                                <div class="col-lg-6">
                                    <textarea class="form-control" value="" name="address">{{ Auth::user()->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">Change Avatar</label>
                                <div class="col-lg-6">
                                    <input type="file" name="photo" class="file-pos" id="exampleInputFile">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label">New Password</label>
                                <div class="col-lg-6">
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </aside>
        </div>

        <!-- page end-->
    </section>
</section>
@endsection
