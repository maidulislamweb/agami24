@extends('admin.layouts.app')
@section('title', 'Dashboard | Users')
@section('content')
<!--main content start-->
<section id="main-content">
   <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <section class="panel">
               @include('admin.layouts.message')
               <header class="panel-heading">
                  Add New User
                  <span class="tools pull-right">
                     <a class="btn" data-toggle="modal" href="{{ url('user') }}">All User</a>
                     <!--a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a-->
                  </span>
               </header>
               <div class="panel-body">
                  <div class="adv-table">
                     <form class="form-horizontal user-form" method="POST" action="{{ url('register_user') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group row">
                           <label for="name" class="col-md-4 ">{{ __('Name') }}</label>
                           <div class="col-md-8">
                              <input id="name" type="text" class="form-control" name="name">
                              @if ($errors->has('name'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="name" class="col-md-4">{{ __('Bangla Name') }}</label>
                           <div class="col-md-8">
                              <input id="name" type="text" class="form-control" name="bn_name">
                              @if ($errors->has('bn_name'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="name" class="control-label col-md-4">User name</label>
                           <div class="col-md-8">
                              <input id="name" type="text" class="form-control" name="username">
                              @if ($errors->has('name'))
                              <span class="help-block">
                              <strong>{{ $errors->first('username') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="dname" class="col-md-4 ">{{ __('Display Name') }}</label>
                           <div class="col-md-8">
                              <input id="dname" type="text" class="form-control" name="dname" autofocus>
                              @if ($errors->has('dname'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('dname') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="name" class="col-md-4 ">{{ __('Phone') }}</label>
                           <div class="col-md-8">
                              <input id="name" type="text" class="form-control" name="phone" autofocus>
                              @if ($errors->has('phone'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="designation" class="col-md-4 ">{{ __('Designation') }}</label>
                           <div class="col-md-8">
                              <input id="name" type="text" class="form-control" name="designation" autofocus>
                              @if ($errors->has('designation'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('designation') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="email" class="col-md-4 ">{{ __('E-Mail Address') }}</label>
                           <div class="col-md-8">
                              <input id="email" type="email" class="form-control" name="email" required>
                              @if ($errors->has('email'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="control-label col-md-4">Address</label>
                           <div class="col-md-8">
                              <textarea class="form-control" name="address" id="" rows="2"></textarea>
                              @if ($errors->has('address'))
                              <span class="help-block">
                              <strong>{{ $errors->first('address') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="email" class="col-md-4 ">{{ __('Role') }}</label>
                           <div class="col-md-8">
                              <select class="form-control" name="role">
                                 <option value="4" selected>User</option>
                                 <option value="3">Publisher</option>
                                 <option value="2">Admin</option>
                                 <option value="1">Super Admin</option>
                              </select>
                              @if ($errors->has('email'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="photo" class="control-label col-md-4">Profile Picture</label>
                           <div class="col-md-8">
                              <input id="photo" type="file" class="form-control" name="photo">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="password" class="col-md-4 ">{{ __('Password') }}</label>
                           <div class="col-md-8">
                              <input id="password" type="password" class="form-control" name="password" required>
                              @if ($errors->has('password'))
                              <span class="invalid-feedback">
                              <strong>{{ $errors->first('password') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row mb-0">
                           <div class="col-md-8 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                              {{ __('Register') }}
                              </button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<!--main content end-->
@endsection
