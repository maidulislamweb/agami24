@extends('admin.layouts.app')
@section('title', 'Dashboard | Users')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
              <section class="panel">
                 @include('admin.layouts.message')
              <header class="panel-heading">
                All User
             <span class="tools pull-right">
               <a class="btn" href="{{ url('userAdd') }}">New User</a>
                <!--a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a-->
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped">
              <thead>
              <tr>
                <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Photo</th>
                  <th>Type</th>
                  <th>IS_Active</th>
                  <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(!empty($users))
                @foreach($users as $user)
              <tr class="gradeX">
                  <td>{{$user->id}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->phone}}</td>
                  <td><a href="#">
                    @if(!empty($user->photo))
                  <img width="50px" class="img-responsive" alt="{{ $user->name }}" src="{{asset('media/author/'.$user->photo)}}" />
                  @else
                <img width="50px" class="img-responsive" alt="{{ $user->name }}" src="{{ Gravatar::src($user->email, 200) }}" />
                @endif
                  </a> </td>
                  <td>
                    @if($user->role == 1)
                    <span class="label label-primary" style="font-size: 11px;">Super Admin</span>
                    @endif
                    @if($user->role == 2)
                    <span class="label label-success" style="font-size: 11px;">Admin</span>
                   @endif
                   @if($user->role == 3)
                  <span class="label label-info" style="font-size: 11px;">Publisher</span>
                  @endif
                  @if($user->role == 4)
                  <span class="label label-warning" style="font-size: 11px;">Subscriber</span>
                 @endif
                 </td>
                  <td class="text-center">
                    <input type="checkbox" class="status" id="" data-id="{{$user->id}}" @if ($user->status) checked @endif></td>
                  <td>
                    <ul class="list-unstyled list-inline">
                <li><a href="{{url('/singleUser/'.$user->id)}}" title="View"><i class="fa fa-heart-o"></i></a></li>
              <li><a href="{{url('/user-edit/'.$user->id)}}" title="Edit"><i class="fa fa-pencil-square-o"></i></a></li>
                <li><a href="{{url('/user_delete/'.$user->id)}}" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');">
                                       <i class="fa fa-trash"></i></a></li>
                  </ul> </td>
              </tr>
              @endforeach
              @endif
              </tbody>
              <tfoot>
              <tr>
                <th>ID</th>
                  <th>Name</th>
                  <th>email</th>
                  <th>Phone</th>
                  <th>Photo</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<!--main content end-->

<script>
    $(document).ready(function(){
        $('.status').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
            increaseArea: '20%'
        });
        $('.status').on('ifClicked', function(event){
            id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: "{{ URL::route('userStatus') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function(data) {
                    // empty
                },
            });
        });
        $('.status').on('ifToggled', function(event) {
            $(this).closest('tr').toggleClass('warning');
        });
    });

</script>
@endsection
