@extends('admin.layouts.app')
@section('title', 'User | Profile')
@section('content')
<!--main content start-->
 <section id="main-content">
     <section class="wrapper">
         <!-- page start-->
         <div class="row">
             <aside class="profile-nav col-lg-3">
                 <section class="panel">
                    @include('admin.layouts.message')
                     <div class="user-heading round">
                       <a href="#">
                         @if(!empty(Auth::user()->photo))
                       <img width="100px" class="img-responsive" alt="{{ Auth::user()->name }}" src="{{asset('media/author/'.Auth::user()->photo)}}" />
                       @else
                     <img width="100px" class="img-responsive" alt="{{ Auth::user()->name }}" src="{{ Gravatar::src(Auth::user()->email, 200) }}" />
                     @endif
                       </a>
                         <h1>{{ Auth::user()->name }}</h1>
                         <p>{{ Auth::user()->email }}</p>
                     </div>

                     <ul class="nav nav-pills nav-stacked">
                         <li class="active"><a href="{{url('profile')}}"> <i class="fa fa-user"></i> Profile</a></li>
                         <li><a href="{{url('profile-activity')}}"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-danger pull-right r-activity">9</span></a></li>
                         <li><a href="{{url('profile-edit')}}"> <i class="fa fa-edit"></i> Edit profile</a></li>
                     </ul>

                 </section>
             </aside>
             <aside class="profile-info col-lg-9">

                 <section class="panel">
                     <div class="panel-body bio-graph-info">
                         <h1>Bio Graph</h1>
                         <div class="row">
                             <div class="bio-row">
                                 <p><span>Name </span>: {{ Auth::user()->name }}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Bangla Name </span>: {{ Auth::user()->bn_name }}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Display Name </span>: {{ Auth::user()->display_name}}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>User Name </span>: {{ Auth::user()->username}}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Designation</span>: {{ Auth::user()->designation}}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Join Date</span>:{{ Auth::user()->created_at}}</p>
                             </div>

                             <div class="bio-row">
                                 <p><span>User Type </span>: @if(Auth::user()->role == 1)
                                 <span class="label label-primary" style="font-size: 11px;">Super Admin</span>
                                 @endif
                                 @if(Auth::user()->role == 2)
                                 <span class="label label-success" style="font-size: 11px;">Admin</span>
                                @endif
                                @if(Auth::user()->role == 3)
                               <span class="label label-info" style="font-size: 11px;">Publisher</span>
                               @endif
                               @if(Auth::user()->role == 4)
                               <span class="label label-warning" style="font-size: 11px;">Subscriber</span>
                              @endif</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Email </span>: {{ Auth::user()->email }}</p>
                             </div>
                             <div class="bio-row">
                                 <p><span>Mobile </span>: {{ Auth::user()->phone }}</p>
                             </div>
                         </div>
                     </div>
                 </section>
             </aside>
         </div>

         <!-- page end-->
     </section>
 </section>
 <!--main content end-->
@endsection
