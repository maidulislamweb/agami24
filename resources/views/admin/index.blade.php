@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-md-4">
            <section class="panel post-wrap pro-box">
                <aside>
                    <div class="post-info">
                        <span class="arrow-pro right"></span>
                        <div class="panel-body">
                            <h1><strong>Category</strong>  Wise News</h1>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Number Of News</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if(!empty($categories))
                              @foreach($categories as $category)
                              <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>8</td>
                              </tr>
                              @endforeach
                              @endif

                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Number Of News</th>
                              </tr>
                            </tfoot>

                          </table>

                        </div>
                    </div>
                </aside>
            </section>
          </div>
          <div class="col-md-8">
            <section class="panel post-wrap pro-box">
                <aside>
                    <div class="post-info">
                        <span class="arrow-pro right"></span>
                        <div class="panel-body">
                            <h1><strong>Popular</strong> News</h1>
                         <div class="table-responsive">
                              <table class="table table-bordered" id="dynamic-table">
                  <thead>
                    <tr>
                    <th>Date</th>
                  <th>Page</th>
                  <th>Visitor</th>
                  <th>View</th>
                </tr>
                  </thead>
                  <tbody>
                     @foreach($analytic as $ana)
                  <tr>
                    <td>{{$ana['date']}}</td>
                    <td>{{$ana['pageTitle']}}</td>
                    <td>{{$ana['visitors']}}</td>
                    <td>{{$ana['pageViews']}}</td>
                </tr>
       @endforeach
                    
                  </tbody>
                 
                <tfoot>
                   <tr>
                    <th>Date</th>
                  <th>Page</th>
                  <th>Visitor</th>
                  <th>View</th>
                </tr>
                </tfoot>
                </table>
                         </div>

                        </div>
                    </div>
                </aside>
            </section>
          </div>
        </div>
    </section>
</section>
<!--main content end-->
@endsection
