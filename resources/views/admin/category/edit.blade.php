@extends('admin.layouts.app')
@section('title', "| $category->name")
@section('content')
<!--main content start-->
<section id="main-content">
   <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-md-6 col-md-offset-3">
            <section class="panel">
               <header class="panel-heading">
                  All Categories
                  <span class="tools pull-right">
                  <a class="btn" href="{{url('category')}}"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                  </span>
                  @include('admin.layouts.message')
               </header>
               <div class="panel-body">
                  <div class="adv-table table-container">
                    <form method="post" action="{{url('category_update')}}" accept-charset="UTF-8"  enctype="multipart/form-data">
                      <input type="hidden" name="id" class="form-control" id="id" value="{{$category->id}}">
                       {{ csrf_field() }}
                       <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" name="name" class="form-control" value="{{$category->name}}">
                          @if ($errors->has('name'))
                          <p class="help-block">{{ $errors->first('name') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="slug">Slug</label>
                          <input type="text" name="slug" class="form-control" value="{{$category->slug}}">
                          @if ($errors->has('slug'))
                          <p class="help-block">{{ $errors->first('slug') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="title">title</label>
                          <input type="text" name="title" class="form-control" value="{{$category->title}}">
                          @if ($errors->has('title'))
                          <p class="help-block">{{ $errors->first('title') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="keywards">Keywards</label>
                          <textarea class="form-control"  name="keywards" rows="2" cols="40">{{$category->keywards}}</textarea>
                          @if ($errors->has('keyward'))
                          <p class="help-block">{{ $errors->first('keywards') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="description">Description</label>
                          <textarea class="form-control"  name="description" rows="3" cols="40">{{$category->description}}</textarea>
                          @if ($errors->has('description'))
                          <p class="help-block">{{ $errors->first('description') }}</p>
                          @endif
                       </div>

                       <div class="form-group">
                          <label for="top_menu" class="control-label">Show Top Menu</label>
                          <input name="top_menu" id="radio-01" value="1" type="radio"  />Yes
                          <input name="top_menu" id="radio-02" value="0" type="radio" checked/>No
                          @if ($errors->has('order_id'))
                          <p class="help-block">{{ $errors->first('order_id') }}</p>
                          @endif
                       </div>

                       <div class="form-group">
                          <label for="order_id">Order By</label>
                          <input type="text" class="form-control" name="order_id" id="order"  value="{{$category->order_id}}">
                          @if ($errors->has('order_id'))
                          <p class="help-block">{{ $errors->first('order_id') }}</p>
                          @endif
                       </div>
                       <div class="form-group">
                          <label for="banner">Banner</label>
                          <input type="file" class="form-control" name="banner">
                          @if ($errors->has('banner'))
                          <p class="help-block">{{ $errors->first('banner') }}</p>
                          @endif
                       </div>

                       <div class="modal-footer">
                          <button type="submit" class="btn btn-info">Submit</button>
                          <button class="btn btn-danger" type="reset">Cancel</button>
                       </div>
                    </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
      <!-- page end-->
   </section>
</section>

<script type="text/javascript">
   $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
       $(".alert-danger").slideUp(500);
   });

</script>
@endsection
