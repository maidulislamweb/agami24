@extends('admin.layouts.app')
@section('title', 'Dashboard | Category')
@section('content')
<!--main content start-->
<section id="main-content">
   <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-md-10 col-md-offset-1">
            <section class="panel">
               <header class="panel-heading">
                  All Categories
                  <span class="tools pull-right">
                  <a class="btn" data-toggle="modal" href="#myModal3">
                  Add category
                  </a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                  </span>
                  @include('admin.layouts.message')
               </header>
               <div class="panel-body">
                  <div class="adv-table table-container table-responsive">
                     <table  class="display table table-bordered table-striped" id="dynamic">
                        <thead>
                           <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Slug</th>
                              <th>Order By</th>
                              <th>Top Menu?</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(!empty($categories))
                           @foreach($categories as $category)
                           <tr class="gradeX">
                              <td>{{$category->id}}</td>
                              <td>{{$category->name}}</td>
                              <td>{{$category->title}}</td>
                              <td>{{$category->top_menu}}</td>
                              <td>{{$category->order_id}}</td>
                              <td>
                                <input type="checkbox" class="status" id="" data-id="{{$category->id}}" @if ($category->status) checked @endif></td>
                              <td>
                                 <ul class="list-unstyled list-inline">
                                  <li><a href="{{url('/category-edit/'.$category->id)}}" class="tooltip-success" data-rel="tooltip" title="Edit">
                                       <i class="fa fa-pencil-square-o"></i></a>
                                  </li>
                                    <li><a href="{{url('/category-delete/'.$category->id)}}" title="Delete" class="" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></a></li>
                                 </ul>
                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                        <tfoot>
                           <tr>
                             <th>ID</th>
                             <th>Name</th>
                             <th>Meta Title</th>
                             <th>Order By</th>
                             <th>Top Menu?</th>
                             <th>Status</th>
                             <th>Action</th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
            </section>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add Category</h4>
               </div>
               <div class="modal-body">
                  <form method="post" action="{{ url('categoryInsert') }}" accept-charset="UTF-8">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control">
                        @if ($errors->has('name'))
                        <p class="help-block">{{ $errors->first('name') }}</p>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" class="form-control">
                        @if ($errors->has('slug'))
                        <p class="help-block">{{ $errors->first('slug') }}</p>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="title">title</label>
                        <input type="text" name="title" class="form-control">
                        @if ($errors->has('title'))
                        <p class="help-block">{{ $errors->first('title') }}</p>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="keywards">Keyward</label>
                        <textarea class="form-control"  name="keywards" rows="2" cols="40"></textarea>
                        @if ($errors->has('keyward'))
                        <p class="help-block">{{ $errors->first('keywards') }}</p>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control"  name="description" rows="3" cols="40"></textarea>
                        @if ($errors->has('description'))
                        <p class="help-block">{{ $errors->first('description') }}</p>
                        @endif
                     </div>

                     <div class="form-group">
                        <label for="top_menu" class="control-label">Show Top Menu</label>
                        <input name="top_menu" id="radio-01" value="1" type="radio"  />Yes
                        <input name="top_menu" id="radio-02" value="0" type="radio" checked/>No
                        @if ($errors->has('order_id'))
                        <p class="help-block">{{ $errors->first('order_id') }}</p>
                        @endif
                     </div>

                     <div class="form-group">
                        <label for="order_id">Order By</label>
                        <input type="text" class="form-control" name="order_id">
                        @if ($errors->has('order_id'))
                        <p class="help-block">{{ $errors->first('order_id') }}</p>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="banner">Banner</label>
                        <input type="file" class="form-control" name="banner">
                        @if ($errors->has('banner'))
                        <p class="help-block">{{ $errors->first('banner') }}</p>
                        @endif
                     </div>

                     <div class="modal-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                     </div>
                  </form>
               </div>

            </div>
         </div>
      </div>
      <!-- modal -->
      <!-- page end-->
   </section>
</section>
<!--main content end-->
          <!-- Delete  modal -->



  <div id="modal-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-info bigger-110">
          These items will be permanently deleted and cannot be recovered.
        </div>
        <h3 class="text-center">
          <p class="bigger-110 bolder center grey">
            <i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
            ☣Are you sure?☣
          </p>
        </h3>

        <p>Sure you want to delete this data with ID : <strong><span id="del-id"></span></strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="delete btn btn-danger" data-dismiss="modal">Delete</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$('.delete-modal').click(function() {
  $('#del-id').html($(this).data('delete-id'));
  $('#modal-delete').modal('show');
});
$('.delete').click(function() {
		$.ajax({
			url: '/category/delete',
			type: 'DELETE',
			headers: {
		        'X-CSRF-TOKEN': $('#token').attr('content')
		    },
			data: {
				'id': $('#del-id').html()
			},
			success: function(result) {
				$('.category' + result.id).remove();
				location.reload();
			},
			error: function(xhr, error) {
				alert(error);
			}
		});
	});
</script>

   <script>
       $(document).ready(function(){
           $('.status').iCheck({
               checkboxClass: 'icheckbox_square-yellow',
               radioClass: 'iradio_square-yellow',
               increaseArea: '20%'
           });
           $('.status').on('ifClicked', function(event){
               id = $(this).data('id');
               $.ajax({
                   type: 'POST',
                   url: "{{ URL::route('categoryStatus') }}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'id': id
                   },
                   success: function(data) {
                       // empty
                   },
               });
           });
           $('.status').on('ifToggled', function(event) {
               $(this).closest('tr').toggleClass('warning');
           });
       });

   </script>
@endsection
