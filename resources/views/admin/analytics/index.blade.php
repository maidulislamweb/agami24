@extends('admin.layouts.app')
@section('title', 'Analytics | Google')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">Google Analytics</header>
              <div class="panel-body">
              <div class="adv-table">
                <table class="table table-bordered table-striped" id="dynamic-table">
                            <thead>
                    <tr>
                    <th>Date</th>
                  <th>Page</th>
                  <th>Visitor</th>
                  <th>View</th>
                </tr>
                  </thead>
                  <tbody>
                     @foreach($analytic as $ana)
                  <tr>
                    <td>{{$ana['date']}}</td>
                    <td>{{$ana['pageTitle']}}</td>
                    <td>{{$ana['visitors']}}</td>
                    <td>{{$ana['pageViews']}}</td>
                </tr>
       @endforeach
                    
                  </tbody>
                 
                <tfoot>
                   <tr>
                    <th>Date</th>
                  <th>Page</th>
                  <th>Visitor</th>
                  <th>View</th>
                </tr>
                </tfoot>
                </table>
              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<!--main content end-->
@endsection
