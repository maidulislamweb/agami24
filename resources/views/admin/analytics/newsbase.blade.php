@extends('admin.layouts.app')
@section('title', 'Analytics | Google')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">Google Analytics<span class="tools pull-right">
               <a class="btn" data-toggle="modal" href="{{ url('') }}"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">

              </div>
              </div>
              </section>
              </div>
              </div>
      </section>
</section>
<!--main content end-->
@endsection
