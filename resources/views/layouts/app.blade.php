<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--title>{!! Meta::get('name') !!}|{!! Meta::get('title') !!}</title-->
  <link rel="shortcut icon" href="{{ asset('media/favicon.ico') }}" type="image/x-icon"/>
  <link rel="icon" href="{{ asset('media/favicon.ico') }}" type="image/x-icon"/>
  <meta name="theme-color" content="#4285f4">
  <meta name="robots" content="index,follow">
  <meta name="googlebot" content="index,follow">
  <meta name="google-site-verification" content="BR-V1uHu3VR0NPSUeutj1f_yFODxP1GJpp3Ufv8YCcE">
  <!-- Geo tags -->
  <meta name="ICBM" content="23.777176;90.399452">
  <meta name="geo.position" content="23.777176;90.399452">
  <meta name="geo.region" content="3166-2:BD-C">
  <meta name="geo.placename" content="Dhaka">
  {!! Meta::tag('robots') !!}
  {!! Meta::tag('site_name') !!}
  {!! Meta::tag('url', Request::url()); !!}
  {!! Meta::tag('locale', 'en_EN') !!}
  {!! Meta::tag('title') !!}
  {!! Meta::tag('key') !!}
  {!! Meta::tag('description') !!}
  {{-- Print custom section images and a default image after that --}}
  {!! Meta::tag('image', asset('images/default-logo.png')) !!}
  <!-- Schema.org markup (Google) -->
  <!-- Twitter Card markup-->
  <meta name="twitter:image:alt" content="Cover">
  <!-- Open Graph markup (Facebook, Pinterest) -->
  <meta property="og:title" content="@yield('title')">
  <meta property="og:type" content="article">
  {!! Meta::tag('author') !!}
  {!! Meta::tag('Developed By','Software Team') !!}
  {!! Meta::tag('developer') !!}
  {!! Meta::tag('distribution','Global') !!}
  <meta http-equiv="Cache-Control" content="no-cache"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Content-Language" content="bn"/>
  <meta property="fb:pages" content="351757248257846" />
  <meta property="fb:app_id" content="280720125780723" />
  <meta property="fb:admins" content="Arifur Rahman"/>
  <meta property="fb:app_id" content="123456789">
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Organization",
    "url": "https://www.agami24.com",
    "logo": "media/common/logo.jpg",
    "contactPoint" : [
      {
        "@type" : "ContactPoint",
        "telephone" : "+8801711102083",
        "email" : "info@agami24.com",
        "contactType" : "customer service"
      }
    ],
    "sameAs" : [
      "https://www.facebook.com/agami24hours",
      "https://twitter.com/agami24",
      "https://plus.google.com/agami24",
      "https://www.youtube.com/agami24"
    ]
  }
  </script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript" src="{{ asset('common/js/jquery.min.js') }}"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122993133-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-122993133-1');
  </script>

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5LFD572');</script>
<!-- End Google Tag Manager -->
</head>
<body>
 <div id="fb-root"></div>
 <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LFD572"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <header>
   <div class="visible-print-block text-center">
    <img alt="" src="{{ asset('media/logo/logo.png') }}" width="260px"/>
  </div>
  <nav class="navbar navbar-default no-margin navbar-static-top top hidden-xs" role="navigation" style="z-index:9" id="top">
    <div class="container">
     <div class="row">
      <div class="col-sm-3">
       <a class="" href="{{url('/')}}">
         <img alt="Logo" src="{{ asset('media/logo/logo.png') }}" width="300px" style="padding-top:-30px!important"/>
       </a>
     </div>

     <div class="col-sm-4 text-center hidden-print">
      @include('layouts.BanglaDate')
      @php
      $bn = new BanglaDate(time()); $bdate = $bn->get_date();
      @endphp

                   <small class="date">
                    <i class="fa fa-map-marker"></i> ঢাকা, {{ \App\Http\Controllers\CommonController::GetBangla(date("D, d M Y"))}} | @if(isset($bn)){{$bdate[0]. " ". $bdate[1]." ". $bdate[2]}} @endif
                  </small>
                </div>
                <div class="col-sm-5 hidden-print">

                </div>
              </div>
            </div>
          </nav>
          @include('layouts.topmenu')
        </header>
        <main class="">
         @yield('content')
       </main>
       <footer>
        @include('layouts.footermenu')
        <div class="footer-logo hidden-xs">
          <div class="container">
         <div class="row hidden-print">
          <div class="col-sm-4">
           <a href="{{url('/')}}">
             <img src="{{ asset('media/logo/logo.png') }}" style="" alt="Logo" width="200px" />
           </a>
         </div>
         <div class="col-sm-4 text-center subscribe hidden-print">
       </div>
       <div class="col-sm-4 text-center subscribe hidden-print">
        <div class="fb-page" data-href="https://www.facebook.com/agami24hours/" data-tabs="timeline" data-height="130" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false">
          <blockquote cite="https://www.facebook.com/agami24hours/" class="fb-xfbml-parse-ignore">
            <a href="https://www.facebook.com/agami24hours/">agami24.com</a>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="container">
   <div class="row">
    <div class="col-sm-8">
     <p class="small text-muted">
      <span>&copy; ২০১৮ সর্বস্বত্ব সংরক্ষিত | আগামী ২৪ ডটকম</span><br>
      <i class="fa fa-map-marker"></i>বাসাঃ৪৫,রোড#৩,সেক্টর#৬,উত্তরা,ঢাকা-১২৩০
      <br>
                          <abbr title="Email:"><i class="fa fa-envelope"></i> </abbr> info@agami24.com, news@agami24.com<br>
                        </span>
                      </p>
                    </div>
                    <div class="col-sm-4 footer-top hidden-print">
                    </div>
                  </div>
                </div>
                <div class="scroll-top-wrapper hidden-print">
                 <span class="scroll-top-inner">
                   <i class="fa fa-2x fa-angle-up"></i>
                 </span>
               </div>
             </div>
           </footer>
           <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
           <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
           <script src="{{ asset('common/js/custom.js') }}"></script>
           <script src="{{ asset('js/agami.js') }}"></script>
         </body>
         </html>
