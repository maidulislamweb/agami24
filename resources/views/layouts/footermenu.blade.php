<div class="footer-top footer-menu  hidden-xs hidden-print">
   <div class="container">
      <div class="row">
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/bangladesh')}}">বাংলাদেশ</a></li>
                  <li><a href="{{url('/politics')}}">রাজনীতি</a></li>
                  <li><a href="{{url('/economy')}}">অর্থনীতি</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/religion')}}">ইসলাম</a></li>
                  <li><a href="{{url('/opinion')}}">মতামত</a></li>
                  <li><a href="{{url('/home')}}">প্রবাস</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/')}}">শেয়ার বাজার</a></li>
                  <li><a href="{{url('/various')}}">চিত্র-বিচিত্র</a></li>
                  <li><a href="{{url('/lifestyle')}}">জীবনযাপন</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/literature')}}">সাহিত্য</a></li>
                  <li><a href="{{url('/jobs')}}">চাকরি</a></li>
                  <li><a href="{{url('/')}}">বানিজ্যমেলা</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/')}}">বাজেট</a></li>
                  <li><a href="{{url('/')}}">প্রযুক্তি</a></li>
                  <li><a href="{{url('/')}}">আইন আদালত</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="footer-link">
               <ul class="list-unstyled">
                  <li><a href="{{url('/home')}}"><i class="fa fa-camera" aria-hidden="true"></i> ছবি</a></li>
                  <li><a href="{{url('/home')}}"><i class="fa fa-video-camera" aria-hidden="true"></i> ভিডিও</a></li>
                  <li><a href="{{url('/archive')}}"><i class="fa fa-archive" aria-hidden="true"></i>আকর্ইভ</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
