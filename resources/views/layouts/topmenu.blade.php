<nav class="navbar navbar-default navbar-static-top no-margin hidden-print" role="navigation" style="z-index:10;"  data-spy="affix" data-offset-top="70" id="menu">
   <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand visible-xs" href="{{url('/')}}">
         <img alt="Logo" src="{{ asset('media/logo/logo.png') }}" style="width: 160px;margin-top: -20px;"/>
         </a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
         <ul class="nav navbar-nav text-center">
            <li class="active"><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{url('/bangladesh')}}">বাংলাদেশ</a></li>
            <li><a href="{{url('/international')}}">আন্তর্জাতিক </a></li>
            <li><a href="{{url('/country')}}">দেশজুড়ে </a></li>
            <li><a href="{{url('/economy')}}">অর্থনীতি </a></li>
            <li><a href="{{url('/sports')}}">খেলাধুলা</a></li>
            <li><a href="{{url('/entertainment')}}">বিনোদন</a></li>
            <li><a href="{{url('/jobs')}}">জবস</a></li>
            <li><a href="{{url('/lifestyle')}}">লাইফ স্টাইল </a></li>
            <li><a href="{{url('/education')}}">শিক্ষা</a></li>
            <li class="dropdown mega-dropdown" id="menu-dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>সব</a>
               <div class="dropdown-menu mega-dropdown-menu" style="max-height:500px;">
                  <div class="container">
                     <div class="megamenu-wrapper">
                        <div class="row">
                           <div class="col-sm-3">
                              <ul class="mega-list">
                                 <li><a href="{{url('/opinion')}}">মতামত</a></li>
                                 <li><a href="{{url('/politics')}}">রাজনীতি</a></li>
                                 <li><a href="{{url('/health')}}">স্বাস্থ্য</a></li>
                                 <li><a href="{{url('/religion')}}">ইসলাম</a></li>
                              </ul>
                           </div>
                           <div class="col-sm-3">
                              <ul class="mega-list">
                                 <li><a href="{{url('/religion')}}">ধর্ম</a></li>
                                 <li><a href="{{url('/probash')}}">প্রবাস</a></li>
                                 <!--li><a href="{{url('/medium')}}">শেয়ারবাজার</a></li-->
                                 <li><a href="{{url('/various')}}">চিত্র-বিচিত্র</a></li>
                              </ul>
                           </div>
                           <div class="col-sm-3">
                              <ul class="mega-list">
                                 <li><a href="{{url('/literature')}}">সাহিত্য</a></li>
                                 
                                 <li><a href="{{url('/feature')}}">ফিচার</a></li>
                              </ul>
                           </div>
                           <div class="col-sm-3">
                              <ul class="mega-list">
                                 <li><i class="fa fa-camera-retro"></i><a href="{{url('/archive')}}"> আর্কাইভ</a></li>
                                 <li><i class="fa fa-flask" aria-hidden="true"></i><a href="{{url('/technology')}}"> বিজ্ঞান ও প্রযুক্তি</a></li>
                                 <li><i class="fa fa-comment-o" aria-hidden="true"></i><a href="{{url('/opinion')}}"> মতামত</a></li>
                                 <li><a href="{{url('/law-courts')}}">আইন-আদালত</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="mega-footer">
                              <ul class="list-inline list-unstyled">
                                 <li><a href="{{url('/advertisement')}}">বিজ্ঞাপন</a></li>
                                 <li><a href="{{url('/ramadan')}}">মাহে রমজান</a></li>
                                 <li><a href="{{url('/hajj')}}">পবিত্র হজ্জ্ব</a></li>
                                 <li><a href="{{url('/football-world-cup')}}">বিশ্বকাপ ফুটবল</a></li>
                                 <li><a href="{{url('/cricket-world-cup')}}">বিশ্বকাপ ক্রিকেট</a></li>
                                 <li><a href="{{url('/ekushey-book-fair')}}">একুশে বইমেলা</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <!--li class="nav-item">
               <a class="nav-link" href="#search"><i class="fa fa-search" aria-hidden="true"></i></a>
            </li-->
            <div id="search">
               <button type="submit"  class="close"><i class="fa fa-times" aria-hidden="true"></i></button>
               <form>
                  <input type="search" value="" placeholder="Type Any Keyward" />
                  <button type="submit" class="btn btn-2">Search</button>
               </form>
            </div>
         </ul>
         <ul class="social-media list-unstyled list-inline">
            <li><a href="https://www.facebook.com/agamirnews24/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/dailydesheralosw/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://plus.google.com/+dailydesheralosw" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
         </ul>
      </div>
   </div>
</nav>
