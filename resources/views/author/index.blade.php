@extends('layouts.app')
@section('title', 'Home')
@section('content')
<main id="main-content">
   <section class="paddingTop20">
      <div class="container">
      <div class="row">
         <div class="col-sm-8 main-content">
            <div class="row">
               @if(!empty($article))
               @foreach($article as $art)
               <div class="col-lg-9 col-lg-push-3 col-md-push-0">
                  <div class="box-white marginBottom20 visible-xs hidden-print">
                     <ol class="breadcrumb">
                        <li><a href="{{url('/')}}"><i class="fa fa-home text-danger"></i></a></li>
                        <li><a href="0/sports">খেলাধুলা xxx</a></li>
                     </ol>
                  </div>
                  <article class="box-white">
                     <div class="single-news">
                        <div class="padding15 box-white">
                           <h1 class="no-margin">{{$art->title}}</h1>
                           <div class="dividerDetails"></div>
                           <div class="media">
                              <div class="media-body">
                                 <span class="small text-muted time-with-author">
                                 <i class="fa fa-pencil"></i>
                                 <a href="#" style="display:inline-block;" rel="nofollow">{{$art->udes}}</a>																																																																														<br>
                                 <i class="fa fa fa-clock-o text-danger"></i> প্রকাশিত: {{ date('M j, Y h:ia', strtotime($art->created_at)) }}
                                 | <i class="fa fa fa-clock-o text-danger"></i> আপডেট: {{ date('M j, Y h:ia', strtotime($art->updated_at)) }}
                                 </span>
                                 <a type="" onclick="window.print();"><i class="fa fa-print"></i></a>
                              </div>
                           </div>
                        </div>
                        <div class="paddingLeft10 paddingRight10 hidden-print">
                           <div class="custom-social-share">
                              <ul class="social-media  list-inline list-unstyled">
                                 <li><a type="button"><i class="fa fa-facebook"></i></a></li>
                                 <li><a type="button"><i class="fa fa-twitter"></i></a></li>
                                 <li><a type="button"><i class="fa fa-google-plus"></i></a></li>
                                 <!--li><button type="button" onclick="window.print();"><i class="fa fa-print"></i></button></li-->
                              </ul>
                           </div>
                        </div>
                        <div class="paddingTop10">
                           <img alt="{{$art->title}}" src="{{asset('media/news/'.$art->nphoto)}}" style="width:100%;">
                        </div>
                        <div class="content-details">{!!$art->content!!}</div>
                        <div class="sharethis-inline-share-buttons"></div>
                     </div>
                     <!--single-news-->
                     <div class="paddingLeft10 paddingRight10 hidden-print">
                        <hr>
                        <div class="padding15 hidden-print">
                           <!--div class="row">
                              <div class="col-sm-12">
                                 <h2 class="catTitle">
                                    আরও পড়ুন &nbsp;
                                    <span class="liner"></span>
                                 </h2>
                              </div>
                           </div-->

                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="single-block">
                                    <div class="img-box">
                                       <a href="">
                                         <img alt="" src="" title="ন" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="related">
                                       <h3><a href=""> </a>title</h3>
                                    </div>
                                    <div class="meta">
                                       <span class="pull-left tags">
                                       <i class="fa fa-tags"></i>
                                       <a href="">tag</a>
                                       </span>
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                  </article>
                  </div>
                  <div class="col-lg-3 col-lg-pull-9 col-md-pull-0 hidden-print">
                     <div class="box-white marginBottom20 hidden-xs">
                        <ol class="breadcrumb">
                           <li><a href="0/"><i class="fa fa-home text-danger"></i></a></li>
                           <li><a href="0/sports">{{$art->cbname}}</a></li>
                        </ol>
                     </div>
                     <div class="single-block padding15 auto">
                        <div class="content-tags">
                           <h3>বিষয়: </h3>
                           <div class="dividerDetails"></div>
                           <ul>
                              <li><a href="">{{$art->ttitle}}</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
               @endif
            </div>
            <aside class="col-sm-4 aside hidden-print">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="single-tab">
                        <!-- Nav tabs -->
                        <!--ul class="nav nav-tabs nav-justified" role="tablist">
                           <li role="presentation" class="active"><a href="0/sports/news/431689#tab1" aria-controls="tab1" role="tab" data-toggle="tab" class="cat_more">খেলাধুলা এর আরও খবর</a></li>
                        </ul-->
                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div role="tabpanel" class="tab-pane fade in active" id="tab1">
                              <ul class="media-list">
                                 @if(!empty($relnews))
                                 @foreach($relnews as $rel)
                                 <li class="media">
                                    <div class="media-left">
                                       <a href='{{ url("articles/{$rel->id}/{$rel->cslug}/{$rel->slug}") }}'><img alt="{{$rel->title}}" class="media-object" src="{{asset('media/news/'.$rel->nphoto)}}" width="100px" height="auto"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a href='{{ url("articles/{$rel->id}/{$rel->cslug}/{$rel->slug}") }}'>{{$rel->title}}</a></h4>
                                    </div>
                                 </li>
                                 @endforeach
                                 @endif
                              </ul>
                              <!--div class="allnews"><a href="0/sports"> খেলাধুলা এর সবখবর</a></div-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="jagoTab2 TwoTab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                           <li role="presentation" class="active"><a href="" aria-controls="tab21" role="tab" data-toggle="tab">সর্বশেষ</a></li>
                           <li role="presentation"><a href="" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div role="tabpanel" class="tab-pane fade in active" id="tab21">
                              <ul class="media-list">
                                 @if (count($latest) > 0)
                                 @foreach($latest as $lat)
                                 <li class="media">
                                    <div class="media-left">
                                       <a href='{{ url("articles/{$lat->id}/{$lat->cslug}/{$lat->slug}") }}'><img class="media-object" src="{{asset('media/news/'.$lat->nphoto)}}" alt="{{$lat->title}}"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a href='{{ url("articles/{$lat->id}/{$lat->cslug}/{$lat->slug}") }}'>{{$lat->title}}</a></h4>
                                    </div>
                                 </li>
                                 @endforeach
                                 @else
                                 <hr>
                                 <div class="no-items">
                                    <div class="no-items-wrapper">
                                       <h1 class="text-center cup"><i class="fa fa-coffee" aria-hidden="true"></i></h1>
                                       <h4 class="text-center">Great, You have no thing to do. Just take a cup of tea</h4>
                                    </div>
                                 </div>
                                 @endif
                              </ul>
                              <div class="allnews"><a href="0/archive" rel="nofollow">আজকের সর্বশেষ সবখবর</a></div>
                           </div>
                           <div role="tabpanel" class="tab-pane" id="tab22">
                              <ul class="media-list">
                                 @if (count($popular) > 0)
                                 @foreach($popular as $pop)
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="campus/news/424003.html"><img class="media-object" src="{{asset('media/news/'.$pop->nphoto)}}" alt="{{$pop->title}}"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a href="campus/news/424003.html">{{$pop->title}}</a></h4>
                                    </div>
                                 </li>
                                 @endforeach
                                 @else
                                 <hr>
                                 <div class="no-items">
                                    <div class="no-items-wrapper">
                                       <h1 class="text-center cup"><i class="fa fa-coffee" aria-hidden="true"></i></h1>
                                       <h4 class="text-center">Great, You have no thing to do. Just take a cup of tea</h4>
                                    </div>
                                 </div>
                                 @endif
                              </ul>
                              <div class="allnews"><a href="0/archive" rel="nofollow">আজকের সর্বশেষ সবখবর</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </aside>
         </div>
      </div>
   </section>
</main>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b1b9997e9c37c00114cb6a3&product=inline-share-buttons"></script>
@endsection
