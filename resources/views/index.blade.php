@extends('layouts.app')
@section('content')
<main id="main-content">
   <section class="leadBox paddingTop20">
      <div class="container">
         <div class="row">
            <div class="col-sm-8 main-content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="row">
                        @if(!empty($leadnews))
                        @foreach($leadnews as $ld)
                        <div class="col-sm-8">
                           <div class="single-block mainLead">
                              <div class="img-box">
                                 <a href='{{ url("$ld->cslug/articles/{$ld->id}/{$ld->slug}") }}'>
                                 <img src="{{asset('media/news/'.$ld->nphoto)}}" alt="" class="img-responsive">
                                 </a>
                              </div>
                              <div class="details">
                                 <h2><a href='{{ url("$ld->cslug/articles/{$ld->id}/{$ld->slug}") }}'>{{$ld->title}}</a></h2>
                                 <p>{{str_limit($ld->thumbnail,150)}}</p>
                              </div>
                              <div class="meta">
                                 <span class="pull-left tags">
                                 <a href="{{url('/topic/'.$ld->ttitle)}}">{{$ld->ttitle}}</a>
                                 </span>
                                 <a href="{{url('/category/'.$ld->cslug)}}" class="pull-right">{{$ld->cname}}</a>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @endif
                        @if(!empty($news))
                        @foreach($news as $ns)
                        <div class="col-sm-4">
                           <div class="feature-block">
                              <div class="img-box">
                                 <a href='{{ url("$ns->cslug/articles/{$ns->id}/{$ns->slug}") }}'>
                                 <img src="{{asset('media/news/'.$ns->nphoto)}}" alt="" class="img-responsive">
                                 </a>
                              </div>
                              <div class="feature-details">
                                <div class="feature-title">
                                 <h4><a href='{{ url("$ns->cslug/articles/{$ns->id}/{$ns->slug}") }}'>{{str_limit($ns->title,65)}}</a></h4>
                              </div>
                              </div>
                              <div class="meta">
                                 <span class="pull-left tags">
                                 <a href="{{url('/topic/'.$ns->tslug)}}">{{$ns->ttitle}}</a>
                                 </span>
                                 <a href="{{url('/category/'.$ns->cslug)}}" class="pull-right">{{$ns->cname}}</a>
                              </div>

                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
            </div>
            <aside class="col-sm-4 aside">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="box-white">
                        <div class="sidebar-tab">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs nav-justified" role="tablist">
                              <li role="presentation" class="active">
                                <a href="#tab21" aria-controls="tab21" role="tab" data-toggle="tab">সর্বশেষ</a>
                              </li>
                              <li role="presentation">
                                <a href="#tab22" aria-controls="tab22" role="tab" data-toggle="tab">জনপ্রিয়</a>
                              </li>
                           </ul>
                           <!-- Tab panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane fade in active" id="tab21">
                                 <ul class="media-list">
                                    @if(!empty($latest))
                                    @foreach($latest as $lat)
                                    <li class="media">
                                       <div class="media-left">
                                          <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>
                                          <img width="100px" class="media-object" src="{{asset('media/news/thumb/'.$lat->nphoto)}}" alt="">
                                          </a>
                                       </div>
                                       <div class="media-body">
                                          <h4 class="media-heading">
                                             <a href='{{ url("$lat->cslug/articles/{$lat->id}/{$lat->slug}") }}'>{{$lat->title}}</a>
                                          </h4>
                                       </div>
                                    </li>
                                    @endforeach
                                    @endif
                                 </ul>
                                </div>
                              <div role="tabpanel" class="tab-pane" id="tab22">
                                 <ul class="media-list">
                                    @if(!empty($popular))
                                    @foreach($popular as $pop)
                                    <li class="media">
                                       <div class="media-left">
                                          <a href='{{ url("$pop->cslug/articles/{$pop->id}/{$pop->slug}") }}'>
                                          <img class="media-object" src="{{asset('media/news/thumb/'.$pop->nphoto)}}" alt="">
                                          </a>
                                       </div>
                                       <div class="media-body">
                                          <h4 class="media-heading">
                                             <a href='{{ url("$pop->cslug/articles/{$pop->id}/{$pop->slug}") }}'>{{$pop->title}}</a>
                                          </h4>
                                       </div>
                                    </li>
                                    @endforeach
                                    @endif
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="religion">
                  <div class="single-cat-height">
                     <h2 class="cat-title">
                       <span class="cat-bg">
                         <a href="{{url('religion')}}">ধর্ম</a>
                       </span>
                     </h2>
                     <div class="single-block">
                        <div class="details">

                           @foreach($religion as $rel)
                           <div class="media">
                              <div class="media-left">
                                 <a href='{{ url("$rel->cslug/articles/{$rel->id}/{$rel->slug}") }}'>
                                 <img src="{{asset('media/news/thumb/'.$rel->nphoto)}}" alt="" class="media-object">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <h4 class="media-heading">
                                     <a href='{{ url("$rel->cslug/articles/{$rel->id}/{$rel->slug}") }}'>{{$rel->title}}</a>
                                 </h4>
                              </div>
                           </div>
                           @endforeach

                        </div>
                     </div>
                  </div>
               </div>

               <div class="advertising">
                  <div class="">
                     <div class="single-add">

                     </div>
                  </div>
               </div>



            </aside>
         </div>
      </div>
   </section>
   <!--Jobs Corner-->
   <section class="job section-padd">
      <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h2 class="cat-title">
                    <span class="pull-left cat-bg">
                     চাকুরির সংবাদ
                   </span>
                    <span class="pull-right"> <a href="{{url('jobs')}}">সবখবর <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
                  </h2>
               </div>
            </div>
            <div class="row">
              @if(!empty($jobs))
              @foreach($jobs as $job)
               <div class="col-sm-3">
                  <div class="job-block">
                    <div class="single-block">
                     <div class="img-box">
                        <a href='{{ url("$job->cslug/articles/{$job->id}/{$job->slug}") }}'>
                        <img src="{{asset('media/news/'.$job->nphoto)}}" alt="" class="img-responsive">
                        </a>
                     </div>
                     <div class="details job-title">
                        <h3><a href='{{ url("$job->cslug/articles/{$job->id}/{$job->slug}") }}'>{{$job->title}}</a></h3>
                     </div>
                     <div class="meta">
                        <span class="pull-left tags">
                        <a href="{{url('/topic/'.$job->tslug)}}">{{$job->ttitle}}</a>
                        </span>
                     </div>
                  </div>
                  </div>
               </div>
               @endforeach
               @endif
            </div>
      </div>
   </section>


   <!--section class="deshjure country-section">
      <div class="container">
      <div class="row">
         <div class="col-sm-8 main-content">
            <div class="row">
               <div class="col-sm-12">
                  <h2 class="cat-title"><a href="{{URL('country')}}">দেশজুড়ে</a></h2>
               </div>
            </div>
            <div class="row">
               @if(!empty($country))
               @foreach($country as $cou)
               <div class="col-sm-3">
                  <div class="single-block">
                     <div class="img-box"><a href='{{ url("articles/{$cou->id}/{$cou->cslug}/{$cou->slug}") }}'>
                        <img src="{{asset('media/news/'.$cou->nphoto)}}" alt="">
                        </a>
                     </div>
                     <div class="details">
                        <h4><a href='{{ url("articles/{$cou->id}/{$cou->cslug}/{$cou->slug}") }}'>{{$cou->title}}</a></h4>
                     </div>
                     <div class="meta">
                        <span class="pull-left tags">
                        <a href="{{url('/topic/'.$cou->ttitle)}}">{{$cou->ttitle}}</a>
                        </span>
                     </div>
                  </div>
               </div>
               @endforeach
               @endif
            </div>
         </div>
         <aside class="col-sm-4 aside">
            <div class="row">
               <div class="col-sm-12">
                  <h2 class="text-center">এক ক্লিকে বিভাগের খবর</h2>
                  <div class="homePageSideSvg">
                     <div class="single-block auto padding15">
                        <div class="row">
                           <div class="col-sm-12">
                              <form action="" method="get">
                                 <div class="row form-group">
                                    <div class="col-sm-12">
                                       <label for="division" class="sr-only">বিভাগ</label>
                                       <select class="form-control"  name="division_id" id="division_id" onchange="get_district(this.value);">
                                          <option value="">--বিভাগ--</option>
                                          @if(!empty($divisions))
                                          @foreach($divisions as $division)
                                          <option value="{{$division->id}}">{{$division->bn_name}}</option>
                                          @endforeach
                                          @endif
                                       </select>
                                       @if ($errors->has('division_id'))
                                       <p class="help-block">{{ $errors->first('division_id') }}</p>
                                       @endif
                                    </div>
                                    <div class="col-sm-12">
                                       <label for="district" class="sr-only">জেলা</label>
                                       <select class="form-control" name="district_id" id="district_id" onchange="get_upazila(this.value);">
                                          <option value="">--জেলা--</option>
                                       </select>
                                       @if ($errors->has('district_id'))
                                       <p class="help-block">{{ $errors->first('district_id') }}</p>
                                       @endif
                                    </div>
                                    <div class="col-sm-12">
                                       <label for="upozilla" class="sr-only">উপজেলা</label>
                                       <select class="form-control"name="upazila_id" id="upazila_id" >
                                          <option value="">--উপজেলা--</option>
                                       </select>
                                       @if ($errors->has('district_id'))
                                       <p class="help-block">{{ $errors->first('upazila_id') }}</p>
                                       @endif
                                    </div>
                                    <div class="col-sm-12">
                                       <button type="submit" class="btn btn-block">অনুসন্ধান করুন</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </aside>
         </div>
      </div>
   </section>
   <script>
      function get_subcategory(category_id)
      {
          var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
          var post_url        = '{{ url("get_subcategory")}}';
          $.ajax({
              url: post_url,
              type: 'POST',
              data: {_token: CSRF_TOKEN, category_id: category_id},
              success: function(html_data)
              {
                  if (html_data != '')
                  {
                      $('#subcategory_id').html(html_data);

                  }
              }
          });
      }
      function get_district(division_id)
      {
          var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
          var post_url        = '{{ url("get_district")}}';
          $.ajax({
              url: post_url,
              type: 'POST',
              data: {_token: CSRF_TOKEN, division_id: division_id},
              success: function(html_data)
              {
                  if (html_data != '')
                  {
                      $('#district_id').html(html_data);

                  }
              }
          });
      }
      function get_upazila(district_id)
      {
          var CSRF_TOKEN      = $('meta[name="csrf-token"]').attr('content');
          var post_url        = '{{ url("get_upazila")}}';
          $.ajax({
              url: post_url,
              type: 'POST',
              data: {_token: CSRF_TOKEN, district_id: district_id},
              success: function(html_data)
              {
                  if (html_data != '')
                  {
                      $('#upazila_id').html(html_data);

                  }
              }
          });
      }
   </script-->
   <section class="sports sport-section section-padd">
      <div class="container">
         <div>
            <div class="row">
               <div class="col-sm-12">
                  <h2 class="cat-title">
                     <span class="pull-left cat-bg">
                     <a href="{{url('sports')}}">খেলাধুলা</a>
                     </span>
                     <!--span class="sub-cat">
                       <a href="{{url('sports/cricket')}}">ক্রিকেট</a>
                       <a href="{{url('sports/football')}}">ফুটবল</a>
                     </span-->
                     <span class="pull-right"><a href="{{url('sports')}}" class="right">সবখবর <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
                  </h2>
               </div>
            </div>
            <div class="row">
               @if(!empty($sports))
               @foreach($sports as $sp)
               <div class="col-sm-3">
                  <div class="sports-block">
                    <div class="single-block">
                     <div class="img-box">
                        <a href='{{ url("$sp->cslug/articles/{$sp->id}/{$sp->slug}") }}'>
                        <img src="{{asset('media/news/'.$sp->nphoto)}}" alt="" class="img-responsive">
                        </a>
                     </div>
                     <div class="details sports-title">
                        <h3><a href='{{ url("$sp->cslug/articles/{$sp->id}/{$sp->slug}") }}'>{{$sp->title}}</a></h3>
                     </div>
                     <div class="meta">
                        <span class="pull-left tags">
                        <a href="{{url('/topic/'.$sp->tslug)}}">{{$sp->ttitle}}</a>
                        </span>
                     </div>
                  </div>
                  </div>
               </div>
               @endforeach
               @endif
            </div>
         </div>
      </div>
   </section>
   <section class="paddingTop20 padding-bottom20">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 main-content category">
               <div class="row">
                  <div class="col-sm-3">
                     <div class="single-cat-height international">
                        <h2 class="cat-title">
                          <span class="cat-bg">
                            <a href="{{url('international')}}">আন্তর্জাতিক</a>
                          </span>
                        </h2>
                        <div class="single-block">
                           <!--div class="img-box"><a href="">
                              <img src="" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($international))
                              @foreach($international as $int)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$int->cslug/articles/{$int->id}/{$int->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$int->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$int->cslug/articles/{$int->id}/{$int->slug}") }}'>{{$int->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="single-cat-height politics">
                        <h2 class="cat-title">
                        <span class="cat-bg"><a href="{{url('politics')}}">রাজনীতি</a></span></h2>
                        <div class="single-block">
                           <!--div class="img-box">
                              <a href="">
                                <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($politics))
                              @foreach($politics as $pol)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$pol->cslug/articles/{$pol->id}/{$pol->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$pol->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$pol->cslug/articles/{$pol->id}/{$pol->slug}") }}'>{{$pol->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="single-cat-height economy">
                        <h2 class="cat-title"><span class="cat-bg"><a href="{{url('economy')}}">অর্থনীতি</a></span></h2>
                        <div class="single-block">
                           <!--div class="img-box"><a href="">
                              <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($economics))
                              @foreach($economics as $eco)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$eco->cslug/articles/{$eco->id}/{$eco->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$eco->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$eco->cslug/articles/{$eco->id}/{$eco->slug}") }}'>{{$eco->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="single-cat-height law">
                        <h2 class="cat-title"><span class="cat-bg"><a href="{{url('law-courts')}}">আইন-আদালত</a></span></h2>
                        <div class="single-block">
                           <!--div class="img-box"><a href="">
                              <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($law_courts))
                              @foreach($law_courts as $courts)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$courts->cslug/articles/{$courts->id}/{$courts->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$courts->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$courts->cslug/articles/{$courts->id}/{$courts->slug}") }}'>{{$courts->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--row-->
            </div>
         </div>
      </div>
   </section>
   <section class="paddingTop20 national">
      <div class="container">
        <div class="row">
               <div class="col-sm-12">
                  <h2 class="cat-title">
          <span class="pull-left cat-bg">জাতীয়</span>
                     <span class="pull-right"><a href="{{url('sports')}}" class="pull-right">সবখবর <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                     </span>
                  </h2>
               </div>
            </div>
                    <div class="row">
                        @if(!empty($national))
                        @foreach($national as $nat)
                        <div class="col-sm-3">
                           <div class="post-box">
                              <div class="post-img-box">
                                 <a href='{{ url("$nat->cslug/articles/{$nat->id}/{$nat->slug}") }}'>
                                 <img src="{{asset('media/news/'.$nat->nphoto)}}" alt="" class="img-responsive">
                                 </a>
                              </div>
                              <div class="post-text">
                                <div class="post-box-title">
                                 <h3><a href='{{ url("$nat->cslug/articles/{$nat->id}/{$nat->slug}") }}'>{{$nat->title}}</a></h3>
                              </div>
                              <div class="post-meta">
                                 <span class="pull-left tags">
                                 <a href="{{url('/topic/'.$nat->tslug)}}">{{$nat->ttitle}}</a>
                                 </span>
                              </div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>

      </div>
   </section>
   <section class="section-padd lifestyle">
      <div class="container">
         <div class="row">
                  <div class="col-sm-4">
                     <div class="life-style">
                       <div class="single-cat-height">
                        <h2 class="cat-title"><span class="cat-bg"><a href="{{url('lifestyle')}}">লাইফস্টাইল</a></span></h2>
                        <div class="single-block">
                           <!--div class="img-box"><a href="">
                              <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($lifestyle))
                              @foreach($lifestyle as $lstyle)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$lstyle->cslug/articles/{$lstyle->id}/{$lstyle->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$lstyle->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                      <a href='{{ url("$lstyle->cslug/articles/{$lstyle->id}/{$lstyle->slug}") }}'>{{$lstyle->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="info-tech">
                       <div class="single-cat-height">
                        <h2 class="cat-title"><span class="cat-bg"><a href="{{url('technology')}}">তথ্যপ্রযুক্তি</a></span></h2>
                        <div class="single-block">
                           <!--div class="img-box">
                              <a href=""><img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($technology))
                              @foreach($technology as $tech)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$tech->cslug/articles/{$tech->id}/{$tech->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$tech->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$tech->cslug/articles/{$tech->id}/{$tech->slug}") }}'>{{$tech->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="feature">
                       <div class="single-cat-height">
                        <h2 class="cat-title">
                          <span class="cat-bg">
                            <a href="{{url('feature')}}">ফিচার</a></span>
                          </h2>
                        <div class="single-block">
                           <!--div class="img-box">
                              <a href="">
                              <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4-->
                           <div class="details">
                              @if(!empty($features))
                              @foreach($features as $feature)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$feature->cslug/articles/{$feature->id}/{$feature->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$feature->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$feature->cslug/articles/{$feature->id}/{$feature->slug}") }}'>{{$feature->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
      </div>
   </section>
   <section class="entertainment section-padd">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <h2 class="cat-title">
                  <span class="pull-left cat-bg">
                  <a href="{{url('entertainment')}}">বিনোদন </a>
                  </span>
                  <!--span class="sub-cat">
                  <a href="{{url('hollywood')}}">হলিউড</a>
                  <a href="{{url('bollywood')}}">বলিউড</a>
                  </span-->

                  <span class="pull-right"><a href="{{url('entertainment')}}" class="right">সবখবর <i class="fa fa-angle-double-right"></i></a></span>
               </h2>
            </div>
         </div>
         <div class="binodon">
            <div class="row">
               <div class="col-sm-12">
                  <div class="binodon-slider">
                     @if(!empty($entertainment))
                     @foreach($entertainment as $ente)
                     <div class="single-item">
                        <div class="img-box">
                           <a href='{{ url("$ente->cslug/articles/{$ente->id}/{$ente->slug}") }}'>
                           <img src="{{asset('media/news/'.$ente->nphoto)}}" alt="" class="img-responsive">
                           </a>
                        </div>
                        <div class="details">
                           <h3><a href='{{ url("$ente->cslug/articles/{$ente->id}/{$ente->slug}") }}'>{{$ente->title}}</a></h3>
                        </div>
                        <div class="post-meta">
                           <span class="pull-left tags">
                           <a href="{{url('/topic/'.$ente->tslug)}}">{{$ente->ttitle}}</a>
                           </span>
                           </div>
                     </div>
                     @endforeach
                     @endif
                  </div>
               </div>
            </div>
         </div>
         <!--binodon-->
      </div>
   </section>
    <section class="section-padd">
      <div class="container">
         <div class="row">
                       <div class="col-md-4">
                         <div class="health">
                           <h2 class="cat-title">
                          <span class="cat-bg"><a href="{{url('health')}}">স্বাস্থ্য</a></span>
                        </h2>
                         <div class="single-cat-height">
                            <div class="single-block">
                               <div class="single-block">
                           <div class="details">
                              @if(!empty($health))
                              @foreach($health as $hea)

                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$hea->cslug/articles/{$hea->id}/{$hea->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$hea->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$hea->cslug/articles/{$hea->id}/{$hea->slug}") }}'>{{$hea->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                            </div>
                         </div>
                         </div>
                       </div>
                  <div class="col-sm-4">
                      <div class="education">
                        <h2 class="cat-title">
                           <span class="cat-bg"><a href="{{url('education')}}">শিক্ষা</a>
                           </span>
                        </h2>
                     <div class="single-cat-height">
                        <div class="single-block">
                           <div class="details">
                              @if(!empty($education))
                              @foreach($education as $edu)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("$edu->cslug/articles/{$edu->id}/{$edu->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$edu->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("$edu->cslug/articles/{$edu->id}/{$edu->slug}") }}'>{{$edu->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                      </div>
                  </div>
                  <div class="col-sm-4">
               <div class="various">
                 <div class="single-cat-height">
                  <h2 class="cat-title">
                    <span class="cat-bg">
                      <a href="{{url('various')}}">চিত্র বিচিত্র</a>
                    </span>
                  </h2>
                  <div class="single-block">
                     <div class="details">
                        @if(!empty($various))
                        @foreach($various as $variou)
                        <div class="media">
                          <div class="media-left">
                                    <a href='{{ url("articles/{$variou->id}/{$variou->cslug}/{$variou->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$variou->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                           <div class="media-body">
                              <h4 class="media-heading">
                                 <a href='{{ url("$variou->cslug/articles/{$variou->id}/{$variou->slug}") }}'>{{$variou->title}}</a>
                              </h4>
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
               </div>
            </div>
                  <!--div class="col-sm-3">
                     <div class="single-cat-height">
                        <h2 class="cat-title"><a href="{{url('probash')}}">প্রবাস</a><span class=" "></span></h2>
                        <div class="single-block">
                           <div class="img-box">
                              <a href="">
                              <img src="{{ asset('') }}" alt="" class="img-responsive">
                              </a>
                              </div>
                              <h4><a href=""></a></h4>
                           <div class="details">
                              @if(!empty($probash))
                              @foreach($probash as $prob)
                              <div class="media">
                                 <div class="media-left">
                                    <a href='{{ url("articles/{$prob->id}/{$prob->cslug}/{$prob->slug}") }}'>
                                    <img src="{{asset('media/news/thumb/'.$prob->nphoto)}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       <a href='{{ url("articles/{$prob->id}/{$prob->cslug}/{$prob->slug}") }}'>{{$prob->title}}</a>
                                    </h4>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div-->
               </div>
      </div>
   </section>
   <!--section style="background-color:#000;" class="gallery-section">
      <div class="container photo">
         <div class="marginTopBottom20">
            <div class="row">
               <div class="col-sm-8 main-content">
                  <h2 class="cat-title"><a href="{{url('photo')}}">ফটো গ্যালারি</a></h2>
               </div>
               <aside class="col-sm-4 aside">
                  <h2 class="cat-title">
                     সর্বশেষ &nbsp;
                     <span class=" "></span>
                  </h2>
               </aside>
            </div>
            <div class="row">
               <div class="col-sm-8 main-content">
                  <div id="" class="slider-section">
                     <div class="gallery hompage-slider">
                        @if(!empty($galleries))
                        @foreach($galleries as $gal)
                        <div class="item single-slider">
                           <a href='{{ url("$gal->cslug/articles/{$gal->id}/{$gal->slug}") }}'>
                           <img alt="{{$gal->title}}" src="{{asset('media/gallery/'.$gal->nphoto)}}">
                           </a>
                           <div class="gallery-caption">
                              <div class="caption">
                                 <i class="fa fa-camera" aria-hidden="true"></i>
                                 <a style="color:#fff" href='{{ url("$gal->cslug/articles/{$gal->id}/{$gal->slug}") }}'>{{$gal->title}}</a>
                              </div>
                           </div >
                        </div >
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
               <aside class="col-sm-4 aside latest-news">
                  <div class="row single-media">
                     <div class="col-sm-12">
                        <div class="media-list">
                           @if(!empty($allTopic))
                           @foreach($allTopic as $all)
                           <div class="media">
                              <div class="media-left">
                                 <a href='{{ url("$ld->cslug/articles/{$ld->id}/{$ld->slug}") }}'>
                                 <img class="media-object" src="{{asset('media/news/thumb/'.$all->nphoto)}}" alt ="">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href='{{ url("$ld->cslug/articles/{$ld->id}/{$ld->slug}") }}' class="media-heading">{{$all->title}}</a>
                              </div>
                           </div>
                           @endforeach
                           @endif
                        </div>
                     </div>
                  </div>
               </aside>
            </div>
         </div>
      </div>
   </section-->


   <section class="section-padd">
      <div class="container">
         <div class="row">
            <div class="col-sm-4">
               <div class="literature">
                 <div class="single-cat-height">
                  <h2 class="cat-title">
                    <span class="cat-bg"><a href="{{url('literature')}}">সাহিত্য</a></span>
                  </h2>
                  <div class="single-block">
                     <div class="details">
                        @if(!empty($literature))
                        @foreach($literature as $lit)
                        <div class="media">
                           <div class="media-body">
                              <h4 class="media-heading">
                                 <a href='{{ url("$lit->cslug/articles/{$lit->id}/{$lit->slug}") }}'>{{$lit->title}}</a>
                              </h4>
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
               </div>
            </div>

            <div class="col-sm-4">
               <div class="opinion">
                 <div class="single-cat-height">
                  <h2 class="cat-title">
                    <span class="cat-bg">
                      <a href="{{url('opinion')}}">মতামত</a>
                    </span>
                  </h2>
                  <div class="single-block">
                     <div class="details">
                        @if(!empty($opinion))
                        @foreach($opinion as $opin)
                        <div class="media">
                           <div class="media-body">
                              <h4 class="media-heading">
                                 <a href='{{ url("$ld->cslug/articles/{$ld->id}/{$ld->slug}") }}'>{{$opin->title}}</a>
                              </h4>
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection
