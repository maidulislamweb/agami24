<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class News extends Model
{
  use Sluggable;
  public function sluggable()
     {
         return [
             'slug' => [
                 'source' => 'slug'
             ]
         ];
     }
    //
}
