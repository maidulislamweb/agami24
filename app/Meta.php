<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
  protected $fillable = ['meta_description','meta_title','meta_key'];
}
