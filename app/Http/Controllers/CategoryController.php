<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\News;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use Purifier;
use File;
class CategoryController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['categories'] = DB::table('categories')
            ->join('users', 'categories.created_by', '=', 'users.id')
            ->select('categories.*','users.name as user_name')->orderBy('categories.id', 'ASC')
            ->get();
      return view('admin.category.index',$data);
    }

    public function categoryStatus()
    {
        $id = Input::get('id');
        $category = Category::findOrFail($id);
        $category->status = !$category->status;
        $category->save();
        return response()->json($category);
    }


    public function get_subcategory(Request $request)
    {
        $input      = $request->all();
        $subcategories      =  Subcategory::where('category_id', $input['category_id'])->get();
        $str        = '<option value="">----Select Subcategory----</option>';
        if($subcategories)
        {
            foreach($subcategories as $subcategory)
            {
                $str .= "<option value='".$subcategory['id']."'>".$subcategory['name']."</option>";
            }
        }
        echo $str;
    }
    public function category_publish($categoryId){
     $publish = DB::table('categories')->where('id', $categoryId)->update(['status'=>'1']);
     Session::flash('success', 'Category Successfully Published');
     return Redirect::back();
   }
   public function category_unpulish($categoryId){
     $unpulish = DB::table('categories')->where('id', $categoryId)->update(['status'=>'0']);
     Session::flash('info_message', 'Category Successfully Published');
     return Redirect::back();
   }
   public function subcategory_publish($subcategoryId){
    $publish = DB::table('subcategories')->where('id', $subcategoryId)->update(['status'=>'1']);
    Session::flash('success', 'Subcategory Successfully Published');
    return Redirect::back();
  }
  public function subcategory_unpulish($subcategoryId){
    $unpulish = DB::table('subcategories')->where('id', $subcategoryId)->update(['status'=>'0']);
    Session::flash('info_message', 'Subcategory Unpublished');
    return Redirect::back();
  }
   public function category_edit($categoryId){
      //dd('HI');
      $category = Category::find($categoryId);
      Session::flash('success', 'Category Successfully Edited');
      return view('admin.category.edit')->withCategory($category);
    }
  public function category_delete($categoryId){
        DB::table('categories')->where('id', $categoryId)->delete();
        Session::flash('info_message', 'Category Deleted');
   return Redirect::back();
 }
  public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
          Session::flash('info_message', 'Category  Save Failed');
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
        $category = new Category();
        $category->name = $request->name;
         $str = strtolower($request->slug);
         $category->slug = preg_replace('/\s+/', '-', $str);
         $category->title = $request->title;
         $category->keywards = $request->keywards;
         $category->description = $request->description;
         $category->top_menu = $request->top_menu;
         $category->order_id = $request->order_id;
         $category->created_by = Auth::user()->id;
         $category->status = '1';
         $category->created_at = date('Y-m-d H:i:s');
         if($request->hasFile('banner')){
               $image=$request->file('banner');
               $name=time().'.'.$image->getClientOriginalExtension();
               Image::make($image)->resize(500,null,function($constraints){$constraints->aspectRatio();})
               ->crop(300,300)->save(public_path().'/media/banner/'.$name);
               $category->banner=$name;
           }
           //dd($category);
         if($category->save())
         {
             return redirect::back()->with('success_message','User updated successfully');
         }else{
             return redirect::back()->with('info_message','User could not be updated successfully');
         }
       }
    }
     public function update(Request $request)
       {
         $category=Category::find($request->id);
         $category->name = $request->name;
         $category->title = $request->title;
         $category->slug = $request->slug;
         $category->description = $request->description;
         $category->keywards = $request->keywards;
         $rootPath = public_path('/media/banner');
         if($request->hasFile('banner')){
                if(!empty($category->banner))
                {
                    if(file_exists($rootPath.'/' .$category->banner)){
                        unlink($rootPath.'/' .$category->banner);
                    }
                }
                $image  = $request->file('banner');
                $name   = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(500, null, function ($constraints) {
                    $constraints->aspectRatio();
                })->crop(300, 300)->save(public_path() . '/media/banner/' . $name);
                $category->banner=$name;
            }
         $category->status = '1';
         $category->updated_at = date('Y-m-d H:i:s');
         if($category->save())
         {
             return redirect::back()->with('success_message','User updated successfully');
         }else{
             return redirect::back()->with('info_message','User could not be updated successfully');
         }
       }

}
