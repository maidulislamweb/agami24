<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use DB;

class CommonController extends Controller
{

 /**
     * return bangla date
     */
    public static function GetBangla($text){

        $search_array= array("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ":", ",");

        $replace_array= array("শনিবার", "রোববার", "সোমবার", "মঙ্গলবার", "বুধবার", "বৃহস্পতিবার", "শুক্রবার", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", "জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর", ":", ",");

        $text = str_replace($search_array, $replace_array,$text);

        return $text;
    }


    

    ## --------------end------------ ##
}
