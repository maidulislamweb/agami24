<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use App\District;
class DistrictController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['divisions'] =DB::table('divisions')->where('status', 1)->get();
      $data['districts'] = DB::table('districts')
            ->join('divisions', 'districts.division_id', '=', 'divisions.id')
            ->select('districts.*','divisions.name as divi_name')->orderBy('divisions.id', 'ASC')
            ->get();
            return view('admin.area.district',$data);
    }
public function get_district(Request $request)
{
    $input      = $request->all();
    $districts      =  District::where('division_id', $input['division_id'])->get();
   //$subcategories      =  DB::table('subcategories')->where('category_id', $input['category_id'])->get();//->orderBy('area_name', 'asc')
    $str        = '<option value="">----জেলা----</option>';
    if($districts)
    {
        foreach($districts as $district)
        {
            $str .= "<option value='".$district['id']."'>".$district['bn_name']."</option>";
        }
    }
    echo $str;
}
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'bn_name' => 'required',
          'division_id' => 'required',
        ]);
        if ($validator->fails()) {
          Session::flash('info_message', 'Division Save Erroe');
          return Redirect::back()->withErrors($validator)->withInput();
        } else {
                $district = new District();
                $district['name'] =$request->name;
                $district['bn_name'] =$request->bn_name;
                $district['division_id'] =$request->division_id;
                $district['lat'] =$request->lat;
                $district['lon'] =$request->lon;
                $district['order_id'] =$request->order_id;
                $district['status'] = 1;
                $district['created_at'] = date('Y-m-d H:i:s');
                $district->save();

               Session::flash('success_message', 'Division Successfully Saved!');
               return redirect::back();
             }
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
      $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
      $data['districts'] = DB::table('districts')->where('id', $id)->where('status', 1)->take(1)->get();
      return view('admin.area.district_edit',$data);
    }
     public function update(Request $request, $id)
     {
        // dd($request->all());
         $validator = Validator::make($request->all(), [
           'name' => 'required',
           'division_id' => 'required',
           'bn_name' => 'required',
         ]);
         if ($validator->fails()) {
           Session::flash('info_message', 'District Update Failed');
           return Redirect::back()->withErrors($validator)->withInput();
         } else {
           $district = District::find($id);
           $district['name'] =$request->name;
           $district['bn_name'] =$request->bn_name;
           $district['division_id'] =$request->division_id;
           $district['lat'] =$request->lat;
           $district['lon'] =$request->lon;
           $district['order_id'] =$request->order_id;
           $district['status'] = 1;
           $district['updated_at'] = date('Y-m-d H:i:s');
           $district->save();
            Session::flash('success_message', 'District Successfully Updated!');
            return redirect()->route('district.index', $district->id);
       }
     }
     public function destroy($id)
    {
        $districts   = District::findOrFail($id);
        $districts->delete();
        return response()->json($districts);
    }
}
