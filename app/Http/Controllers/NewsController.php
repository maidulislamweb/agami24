<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Response;
use App\Category;
use App\Subcategory;
use App\Photo;
use App\News;
use App\Meta;
use App\Area;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use Purifier;
use File;
class NewsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function changeStatus()
    {
        $id = Input::get('id');
        $news = News::findOrFail($id);
        $news->status = !$news->status;
        $news->save();
        return response()->json($news);
    }
    public function featureStatus()
      {
          $id = Input::get('id');
          $news = News::findOrFail($id);
          $news->is_feature = !$news->is_feature;
          $news->save();
          return response()->json($news);
      }
      public function leadnewsStatus()
        {
            $id = Input::get('id');
            $news = News::findOrFail($id);
            $news->is_leadnews = !$news->is_leadnews;
            $news->save();
            return response()->json($news);
        }


  public function index()
  {
    $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
    ->orderBy('news.id', 'desc')
    ->paginate(20);
    return view ('admin.news.index',$data);
  }
  public function newsfilter()
  {
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    $data['users'] = DB::table('users')->where('status', 1)->get();
    $data['districts'] = DB::table('districts')->where('status', 1)->get();
    $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
    ->where('news.status', 1)->where('news.created_by', Auth::user()->id)->orderBy('news.id', 'desc')
    ->get();
    return view ('admin.news.newsfilter',$data);
  }
  public function newsfilterView(Request $request)
  {
    $user = Input::get('user_id');
      //dd($user);
    $category = Input::get('category_id');
    $subcategory = Input::get('sub_category_id');
    $district = Input::get('district_id');
    if(!empty($user)){
      $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
      ->join('users', 'news.created_by', '=', 'users.id')
      ->join('photos', 'news.id', '=', 'photos.news_id')
      ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
      ->where('news.status', 1)->orderBy('news.id', 'desc')
      ->get();
    }
    return view ('admin.news.newsfilter',$data);
  }

   public function myPost()
  {
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    $data['users'] = DB::table('users')->where('status', 1)->get();
    $data['districts'] = DB::table('districts')->where('status', 1)->get();
    $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
    ->where('news.status', 1)->where('news.created_by', Auth::user()->id)->orderBy('news.id', 'desc')
    ->get();

    return view ('admin.news.myPost',$data);
  }
  public function days_news()
  {
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    $data['users'] = DB::table('users')->where('status', 1)->get();
    $data['districts'] = DB::table('districts')->where('status', 1)->get();
    $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
    ->where('news.status', 1)->where('news.created_by', Auth::user()->id)->orderBy('news.id', 'desc')
    ->simplePaginate(20);;

    return view ('admin.news.days_news',$data);
  }


  public function draft_post(){
    $data['news'] = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.bn_name as uname','photos.photo')
    ->where('news.status', 0)->orderBy('news.id', 'desc')
    ->get();
    return view('admin.news.draft_post',$data);
  }
  public function addNews()
  {
    $data['users'] = DB::table('users')->where('status', 1)->get();
    $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
    $data['tags'] = DB::table('tags')->where('status', 1)->get();
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    return view ('admin.news.create',$data);
  }
  public function news_publish($newsId){

   $publish = DB::table('news')->where('id', $newsId)->update(['status'=>'1']);
   Session::flash('success_message', 'News Successfully Updated!');
   return Redirect::back();
 }
 public function news_unpulish($newsId){
   $unpulish = DB::table('news')->where('id', $newsId)->update(['status'=>'0']);
   Session::flash('info_message', 'News Unpublished');
   return Redirect::back();
 }
/*
    public function postInsert(Request $request)
      {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'thumbnail' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          if ($validator->fails()) {
            Session::flash('info_message', 'Your post fail');
            return Redirect::back()->withErrors($validator)->withInput();
          } else {
        $content=$request->input('content');
        $news=new News;
        $dom = new \DomDocument();
        $dom->loadHtml( mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        //$dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
       // foreach <img> in the submited message
        foreach($images as $img){
            $src = $img->getAttribute('src');
            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/media/news/$filename.$mimetype";
                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                  // resize if required
                  ->encode($mimetype, 100)  // encode file to the specified mimetype
                  ->save(public_path($filepath));
                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!-
        $news->content = clean($dom->saveHTML());


        //For Feature Image
      /*  $image = $request->file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/media/news/');
        $image->move($destinationPath, $name);
        $news->photo = $name;


        //
        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);
        $this->postImage->add($input);


        $image = $request->file('photo');
      $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
      $name=$input['imagename'];
      $destinationPath = public_path('/media/news/thumb');
      $img = Image::make($image->getRealPath());
      $img->resize(100, 100, function ($constraint) {
      $constraint->aspectRatio();
  })->save($destinationPath.'/'.$input['imagename']);
      $destinationPath = public_path('/media/news/');
      $image->move($destinationPath, $name);
      //$this->postImage->add($input);
        $news->photo = $name;
        $news->category_id = $request->category_id;
        $news->sub_category_id = $request->sub_category_id;
        $news->division_id = $request->division_id;
        $news->district_id = $request->district_id;
        $news->upazila_id = $request->upazila_id;
        $news->title = $request->title;
        $str = $request->slug;
        $news->slug = preg_replace('/\s+/', '-', $str);
        //$str = strtolower($request->slug);
        $news->thumbnail = $request->thumbnail;
        $news->photo_caption = $request->photo_caption;
        $news->tags = $request->tags;
        $news->is_feature = $request->is_feature;
        $news->is_leadnews = $request->is_leadnews;
        $news->meta_description = $request->meta_description;
        $news->meta_title = $request->meta_title;
        $news->meta_key = $request->meta_key;
        $news->created_by = Auth::user()->id;
        $news->status = '1';
        $news->publish = $request->publish;
        $news->created_at = date('Y-m-d H:i:s');
        //dd($news);
        if($news->save())
        {
            return redirect::back()->with('success_message','Your post has been submitted and is pending approval by Admin');
        }else{
            return redirect::back()->with('info_message','User could not be updated successfully');
        }
      }
    }*/
  /*public function newsDelete($newsId){
          //dd($newsId);
          DB::table('news')->where('id', $newsId)->delete();
     return Redirect::back();
   }
   */

   public function postInsert(Request $request)
   {
      // dd($request->all());

     $validator = Validator::make($request->all(), [
       'content' => 'required',

       'title' => 'required',
       'thumbnail' => 'required',
       'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
     ]);
     if ($validator->fails()) {
       Session::flash('info_message', 'Your post fail');
       return Redirect::back()->withErrors($validator)->withInput();
     } else {
       $content=$request->input('content');
       $news=new News;
       $dom = new \DomDocument();
       $dom->loadHtml( mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
       //$dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
       $images = $dom->getElementsByTagName('img');
      // foreach <img> in the submited message
       foreach($images as $img){
         $src = $img->getAttribute('src');
           // if the img source is 'data-url'
         if(preg_match('/data:image/', $src)){
               // get the mimetype
           preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
           $mimetype = $groups['mime'];
               // Generating a random filename
           $filename = uniqid();
           $filepath = "/media/news/$filename.$mimetype";
               // @see http://image.intervention.io/api/
           $image = Image::make($src)
                 // resize if required
           /* ->resize(300, 200) */
                 ->encode($mimetype, 100)  // encode file to the specified mimetype
                 ->save(public_path($filepath));
                 $new_src = asset($filepath);
                 $img->removeAttribute('src');
                 $img->setAttribute('src', $new_src);
           } // <!--endif
       } // <!-
       $news->content = clean($dom->saveHTML());
       $news->category_id = $request->category_id;
       $news->sub_category_id = $request->sub_category_id;
       $news->title = $request->title;
       $str = $request->title;
       $news->slug = preg_replace('/\s+/', '-', $str);
       $news->thumbnail = $request->thumbnail;
       $news->reporter_id = $request->reporter_id;


       // $news->tags = $request->tags;

       $news->is_feature = $request->is_feature;
       $news->is_leadnews = $request->is_leadnews;
       $news->created_by = Auth::user()->id;
       $news->status = '1';
       $news->created_at = date('Y-m-d H:i:s');

       $news->tags = implode(", ", $request->tags);

      //Feature Image
       $image = $request->file('photo');
       $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
       $name=$input['imagename'];
       $destinationPath = public_path('/media/news/thumb');
       $img = Image::make($image->getRealPath());
       $img->resize(100, 100, function ($constraint) {
         $constraint->aspectRatio();
       })->save($destinationPath.'/'.$input['imagename']);
       $destinationPath = public_path('/media/news/');
       $image->move($destinationPath, $name);
       if($news->save()){
        if(!is_null($name)) {
          $news_image = new Photo;
          $news_image->photo = $name;
          $news_image->news_id = $news->id;
          $news_image->caption = $request->caption;
          $news_image->created_at = date('Y-m-d H:i:s');
          $news_image->save();
          $meta = new Meta;
          $meta->news_id = $news->id;
          $meta->title = $request->title;
          $meta->keywards = $request->meta_key;
          $meta->description = $request->meta_description;
          $meta->created_at = date('Y-m-d H:i:s');
          $meta->save();
          $area = new Area;
          $area->news_id = $news->id;
          $meta->division_id = $request->division_id;
          $area->district_id = $request->district_id;
          $area->upazila_id = $request->upazila_id;
          $area->created_at = date('Y-m-d H:i:s');
          $area->save();
        }
        return redirect()->back()->with('success_message','Item added successfully.');
      }else
      return redirect()->back()->with('info_message','Can not add item now. Plese tyr again!!.');
    }
  }
  public function newsDelete($id){
    $news = News::find($id);
    if($news){
      // $news = News::find($id)->delete();
      $photo = DB::table('photos')->where('news_id', $id)->first();
      if($photo){
        // dd('ase pho');
        if($photo->photo){
          $file_path = 'media/news/'.$photo->photo;
          $file_path_thumb = 'media/news/thumb/'.$photo->photo;

          if(file_exists($file_path)){

            @unlink($file_path);
          }
          if(file_exists($file_path_thumb)){

            @unlink($file_path_thumb);
          }
        }
        $photo = Photo::where('news_id',$id)->delete();
      }

      $meta = DB::table('metas')->where('news_id', $id)->first();
      if($meta){
       $meta = Meta::where('news_id',$id)->delete();
     }

     $area = DB::table('areas')->where('news_id', $id)->first();
     if($area){
       $area = Area::where('news_id',$id)->delete();
     }
     $news = DB::table('news')->where('id', $id)->first();
     if($news){
       $news = News::where('id',$id)->delete();
     }
     return redirect()->back()->with('success_message', 'News deleted successfully');
   }
   else{
    return redirect()->back();
  }
}


public function newsView($newsId){

  // dd($newsId);
 $data['newsDetails'] = DB::table('news')
 ->join('categories', 'news.category_id', '=', 'categories.id')
 ->join('users', 'news.created_by', '=', 'users.id')
 ->join('photos', 'news.id', '=', 'photos.news_id')
 ->select('news.*', 'categories.name as category_name','users.name as user_name','categories.name','users.photo as uphoto','users.designation','photos.photo as n_photo')
 ->where('news.status', 1)->where('news.id', $newsId)->take(1)
 ->get();
 return view('admin.news.news-details',$data);
}


public function newsEdit($newsId)
{

  $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
  $data['tags'] = DB::table('tags')->where('status', 1)->get();
  $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
  $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
  $data['newsDetails'] = DB::table('news')
  ->join('categories', 'news.category_id', '=', 'categories.id')
  ->join('users', 'news.created_by', '=', 'users.id')
  ->join('photos', 'news.id', '=', 'photos.news_id')
  ->join('metas', 'news.id', '=', 'metas.news_id')
  ->join('areas', 'news.id', '=', 'areas.news_id')
  ->join('tags', 'news.tags', '=', 'tags.id')
  ->select('news.*', 'categories.name as cname','categories.id as cid','users.name as uname','users.photo','photos.photo as nphoto','photos.caption','areas.division_id as did','metas.title as mtitle','metas.keywards as mkey','metas.description as mdes')
  ->where('news.status', 1)
  ->where('news.id', $newsId)
  ->orderBy('news.id', 'desc')
  ->take(1)
  ->get();

  return view('admin.news.edit',$data);
}


public function postUpdate(Request $request){

  $validator = Validator::make($request->all(), [
    'content' => 'required', 'title' => 'required', 'slug' => 'required', 'thumbnail' => 'required',]);
  if ($validator->fails()) {
    Session::flash('info_message', 'Your post fail');
    return Redirect::back()->withErrors($validator)->withInput();
  } else {
    $news=News::find($request->id);
    //END For Text Editor Value Insert
    $content=$request->input('content');
    //$news=new News;
    $dom = new \DomDocument();
    $dom->loadHtml( mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    //$dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $images = $dom->getElementsByTagName('img');
   // foreach <img> in the submited message
    foreach($images as $img){
      $src = $img->getAttribute('src');
        // if the img source is 'data-url'
      if(preg_match('/data:image/', $src)){
            // get the mimetype
        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
        $mimetype = $groups['mime'];
            // Generating a random filename
        $filename = uniqid();
        $filepath = "/media/news/$filename.$mimetype";
            // @see http://image.intervention.io/api/
        $image = Image::make($src)
              // resize if required
        /* ->resize(300, 200) */
              ->encode($mimetype, 100)  // encode file to the specified mimetype
              ->save(public_path($filepath));
              $new_src = asset($filepath);
              $img->removeAttribute('src');
              $img->setAttribute('src', $new_src);
        } // <!--endif
    } // <!-
    $news->content = clean($dom->saveHTML());
    $news->category_id = $request->category_id;
    $news->sub_category_id = $request->sub_category_id;
    $news->title = $request->title;
       $str = $request->title;
       $news->slug = preg_replace('/\s+/', '-', $str);
       $news->thumbnail = $request->thumbnail;
       $news->reporter_id = $request->reporter_id;


       // $news->tags = $request->tags;

       $news->is_feature = $request->is_feature;
       $news->is_leadnews = $request->is_leadnews;
       $news->created_by = Auth::user()->id;
       $news->status = '1';
       $news->created_at = date('Y-m-d H:i:s');

      //Feature Image
       $image = $request->file('photo');
       $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
       $name=$input['imagename'];
       $destinationPath = public_path('/media/news/thumb');
       $img = Image::make($image->getRealPath());
       $img->resize(100, 100, function ($constraint) {
         $constraint->aspectRatio();
       })->save($destinationPath.'/'.$input['imagename']);
       $destinationPath = public_path('/media/news/');
       $image->move($destinationPath, $name);
        $news->save();
                 Session::flash('success_message', 'Update Successfully Saved');
                 return redirect::back();




  }


}






}
