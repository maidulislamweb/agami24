<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Redirect;;
use Session;
use File;
use Hash;
use Image;
use Auth;
use App\Company;
class CompanyController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $company=DB::table('companies')->where('status', 1)->take(1)->get();
      return view ('admin.setting.index')->withCompany($company);
    }

    public function companyInfo_save(Request $request)
    {
      $company=Company::find($request->id);
      //dd($company);
      $detail=$request->input('address');
      $dom = new \DomDocument();
      $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $images = $dom->getElementsByTagName('img');
      foreach($images as $k => $img){
          $data = $img->getAttribute('src');
          list($type, $data) = explode(';', $data);
          list(, $data)      = explode(',', $data);
          $data = base64_decode($data);
          $image_name= "/media/logo/" . time().$k.'.jpg';
          $path = public_path() . $image_name;
          file_put_contents($path, $data);
          $img->removeAttribute('src');
          $img->setAttribute('src', $image_name);
      }
      $detail = $dom->saveHTML();
      $company->address = $dom->saveHTML();
      //logo
      $rootPath = public_path('/media/logo');
        if($request->hasFile('logo')){
            if(!empty($company->logo))
            {
                if(file_exists($rootPath.'/' .$company->logo)){
                    unlink($rootPath.'/' .$company->logo);
                }
            }
            $image  = $request->file('logo');
            $name   = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(80, null, function ($constraints) {
                $constraints->aspectRatio();
            })->save($rootPath.'/' . $name);
            $company->logo=$name;
        }

      //Favicon
      if($request->hasFile('favicon')){
          if(!empty($company->favicon))
          {
              if(file_exists($rootPath.'/' .$company->favicon)){
                  unlink($rootPath.'/' .$company->favicon);
              }
          }
          $image  = $request->file('favicon');
          $name   = time() . '.' . $image->getClientOriginalExtension();
          Image::make($image)->resize(80, null, function ($constraints) {
              $constraints->aspectRatio();
          })->save($rootPath.'/' . $name);
          $company->favicon=$name;
      }
      $company->name = $request->name;
      $company->title = $request->title;
      $company->thumbnail = $request->thumbnail;
      $company->founder = $request->founder;
      $company->thumbnail = $request->editor;
      $company->contact = $request->contact;
      $company->support = $request->support;
      $company->website = $request->website;
      $company->establish = $request->establish;
      $company->email = $request->email;
      $company->address = $request->address;
      $company->facebook = $request->facebook;
      $company->twitter = $request->twitter;
      $company->youtube = $request->youtube;
      $company->linkedin = $request->linkedin;
      $company->googleplus = $request->googleplus;
      $company->pinterest = $request->pinterest;
      $company->instagram = $request->instagram;
      $company->flickr = $request->flickr;
      $company->created_by = Auth::user()->id;
      $company->status = '1';
      $company->updated_at = date('Y-m-d H:i:s');
      //dd($company);
      $company->save();
      Session::flash('success_message', 'Your post has been submitted and is pending approval by Admin');
      return redirect::back();
    }


    public function edit($id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
