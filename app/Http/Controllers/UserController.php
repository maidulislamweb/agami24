<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use Redirect;
use Validator;
use Session;
use Hash;
use File;
class UserController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }


    public function userStatus()
    {
        $id = Input::get('id');
        $user = User::findOrFail($id);
        $user->status = !$user->status;
        $user->save();

        return response()->json($user);
    }
    public function index()
    {
      $users = User::get();
      $data['users'] = $users;
        return view ('admin.users.index',$data);
    }
    public function profile()
    {
      $data['users'] =DB::table('users')->where('id', Auth::user()->id)->get();
      return view ('admin.users.profile',$data);
    }
    public function profile_activity()
    {
      $data['users'] =DB::table('users')->where('id', Auth::user()->id)->get();
      return view ('admin.users.profile-activity',$data);
    }
    public function profile_edit()
    {
      $data['users'] =DB::table('users')->where('id', Auth::user()->id)->get();
      return view ('admin.users.profile-edit',$data);
    }
    public function singleUser($id)
       {
         $user=User::find($id);
         //dd($user);
         return view('admin.users.single')->withUser($user);
       }
    public function register(Request $request)
    {
       //dd($request->all());
      $validator = Validator::make($request->all(), [
          'name' => 'required',
          'username' => 'required',
          'password' => 'required',
          //'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) {
          Session::flash('info_message', 'New User Registration Failed');
          return Redirect::back()->withErrors($validator)->withInput();
        } else {

      //For Feature Image
      $user = new User;
      $user->name = $request->name;
      $user->bn_name = $request->bn_name;
      $user->display_name = $request->dname;
      $user->username = $request->username;
      $user->designation = $request->designation;
      $user->email = $request->email;
      $user->phone = $request->phone;
      $user->designation = $request->designation;
      $user->address = $request->address;
      //$user->photo = $request->photo;
      $user->role = $request->role;
      if($request->hasFile('photo')){
            $image=$request->file('photo');
            $name=time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(500,null,function($constraints){$constraints->aspectRatio();})
            ->crop(300,300)->save(public_path().'/media/author/'.$name);
            $user->photo=$name;
        }
      //$user->photo = $name;
      $user->password = Hash::make($request->password);
      $user->status = '1';
      $user->created_at = date('Y-m-d H:i:s');
      $user->save();
      //dd($user);
      Session::flash('success_message', 'User Registration Successful');
      return redirect::back();
    }
  }
  public function userAdd()
     {
         return view('admin.users.create');
     }
  public function store(Request $request)
  {
      //
  }

  public function edit($userId){
    $user = User::find($userId);
    return view('admin.users.edit')->withUser($user);
  }
  public function deleteUser($id)
    {
      if (!isset($id))
          return redirect()->back()->with('info_message', 'There was a problem');
      $user = User::find($id);
      if ($user->delete()) {
        if(!empty($user->photo))
        {
          unlink(public_path() . '/media/author/' .$user->photo);
        }
          return redirect()->back()->with('success_message', 'User deleted successfully');
      } else {
          redirect()->back()->with('info_message', 'There was a problem');
      }
    }
  public function update_user(Request $request)
  {
  //  dd('HI');
      $user=User::find($request->id);
      $user->name = $request->name;
      $user->bn_name = $request->bn_name;
      $user->display_name = $request->dname;
      $user->username = $request->username;
      $user->designation = $request->designation;
      $user->email = $request->email;
      $user->phone = $request->phone;
      $user->designation = $request->designation;
      $user->address = $request->address;
      $user->role = $request->role;
      $rootPath = public_path('/media/author');
      if($request->hasFile('photo')){
             if(!empty($user->photo))
             {
                 if(file_exists($rootPath.'/' .$user->photo)){
                     unlink($rootPath.'/' .$user->photo);
                 }
             }
             $image  = $request->file('photo');
             $name   = time() . '.' . $image->getClientOriginalExtension();
             Image::make($image)->resize(500, null, function ($constraints) {
                 $constraints->aspectRatio();
             })->crop(300, 300)->save(public_path() . '/media/author/' . $name);
             $user->photo=$name;
         }
         if(!empty($request->password))
           {
             $user->password= Hash::make($request->password);
           }
      $user->status = '1';
      $user->updated_at = date('Y-m-d H:i:s');
      if($user->save())
      {
          return redirect::back()->with('success_message','User updated successfully');
      }else{
          return redirect::back()->with('info_message','User could not be updated successfully');
      }
  }
  public function update(Request $request)
  {
      //dd($id);
      $user=User::find($request->id);
      $user->name = $request->name;
      $user->bn_name = $request->bn_name;
      $user->display_name = $request->display_name;
      $user->username = $request->username;
      $user->designation = $request->designation;
      $user->email = $request->email;
      $user->phone = $request->phone;
      $user->designation = $request->designation;
      $user->address = $request->address;
      if(!empty($request->password))
           {
             $user->password= Hash::make($request->password);
           }
      $user->updated_at = date('Y-m-d H:i:s');
      if($request->hasFile('photo')){
          if(unlink(public_path() . '/media/author/' .$user->photo)) {
              $image = $request->file('photo');
              $name = time() . '.' . $image->getClientOriginalExtension();
              Image::make($image)->resize(500, null, function ($constraints) {
                  $constraints->aspectRatio();
              })->crop(300, 300)->save(public_path() . '/media/author/' . $name);
              $user->photo=$name;
          }
      }
      if($user->save())
      {
          return redirect::back()->with('success_message','User updated successfully');
      }else{
          return redirect::back()->with('info_message','User could not be updated successfully');
      }
  }


}
