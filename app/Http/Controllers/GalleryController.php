<?php
namespace App\Http\Controllers;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Gallery;
use App\News;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use Purifier;
class GalleryController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['galleries'] = DB::table('galleries')->join('categories', 'galleries.category_id', '=', 'categories.id')
            ->join('users', 'galleries.created_by', '=', 'users.id')
            ->select('galleries.*', 'categories.name as cname','users.name as uname')
            ->where('galleries.status', 1)->orderBy('galleries.id', 'desc')
            ->get();
    return view('admin.gallery.index',$data);
    }
    public function gallery_add()
    {
    //  dd('Hi');
    $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
    $data['tags'] = DB::table('tags')->where('status', 1)->get();
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    return view('admin.gallery.create',$data);
    }
    public function galleryStore(Request $request)
      {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'body' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'thumbnail' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          if ($validator->fails()) {
            Session::flash('info_message', 'Your post fail');
            return Redirect::back()->withErrors($validator)->withInput();
          } else {
        $content=$request->input('body');
        $gallery=new Gallery;
        $dom = new \DomDocument();
        $dom->loadHtml( mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        //$dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
       // foreach <img> in the submited message
        foreach($images as $img){
            $src = $img->getAttribute('src');
            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/media/PhotoGallery/$filename.$mimetype";
                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                  // resize if required
                  /* ->resize(300, 200) */
                  ->encode($mimetype, 100)  // encode file to the specified mimetype
                  ->save(public_path($filepath));
                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!-
        $gallery->content = clean($dom->saveHTML());
        //For Feature Image
        $image = $request->file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/media/PhotoGallery/');
        $image->move($destinationPath, $name);
        $gallery->photo = $name;
        $gallery->category_id = $request->category_id;
        $gallery->sub_category_id = $request->sub_category_id;
        $gallery->title = $request->title;
        $gallery->slug = $request->slug;
        $gallery->thumbnail = $request->thumbnail;
        $gallery->tags = $request->tags;
        $news->meta_title = $request->meta_title;
        $news->meta_key = $request->meta_key;
        $news->meta_description = $request->meta_description;
        $gallery->created_by = Auth::user()->id;
        $gallery->status = '1';
        $gallery->publish = $request->publish;
        $gallery->created_at = date('Y-m-d H:i:s');
        $gallery->save();
        //dd($news);
        Session::flash('success_message', 'Your post has been submitted and is pending approval by Admin');
        return view('admin.gallery.create');
      }
    }


      public function videos()
    {
      return view('admin.gallery.videos');
    }

    public function store(Request $request)
    {
        //
    }
    public function galleryEdit($id)
    {
      //dd('HI');
    $data['tags'] = DB::table('tags')->where('status', 1)->get();
    $data['subcategories'] = DB::table('subcategories')->where('status', 1)->get();
    $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
    $data['gallery'] = DB::table('news')
                    ->join('categories', 'news.category_id', '=', 'categories.id')
                    ->join('users', 'news.created_by', '=', 'users.id')
                    ->select('news.*', 'categories.name as category_name','categories.id as cid','users.name as user_name','categories.bn_name','users.photo','news.photo as n_photo')
                    ->where('news.status', 1)->where('news.id', $newsId)->orderBy('news.id', 'desc')->take(1)//->where('status', 1)
                    ->get();
     return view('admin.gallery.edit');
    }
    public function update(Request $request)
  {
      $gallery=Gallery::find($request->id);
      $gallery->category_id = $request->category_id;
      $gallery->sub_category_id = $request->sub_category_id;
      $gallery->title = $request->title;
      $gallery->slug = $request->slug;
      $gallery->thumbnail = $request->thumbnail;
      $gallery->tags = $request->tags;
      $news->meta_title = $request->meta_title;
      $news->meta_key = $request->meta_key;
      $news->meta_description = $request->meta_description;
      $gallery->created_by = Auth::user()->id;
      $content=$request->input('body');
      $content=$request->input('content');
      $news=new Gallery;
      $dom = new \DomDocument();
      $dom->loadHtml( mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      //$dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $images = $dom->getElementsByTagName('img');
     // foreach <img> in the submited message
      foreach($images as $img){
          $src = $img->getAttribute('src');
          // if the img source is 'data-url'
          if(preg_match('/data:image/', $src)){
              // get the mimetype
              preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
              $mimetype = $groups['mime'];
              // Generating a random filename
              $filename = uniqid();
              $filepath = "/media/gallery/$filename.$mimetype";
              // @see http://image.intervention.io/api/
              $image = Image::make($src)
                // resize if required
                /* ->resize(300, 200) */
                ->encode($mimetype, 100)  // encode file to the specified mimetype
                ->save(public_path($filepath));
              $new_src = asset($filepath);
              $img->removeAttribute('src');
              $img->setAttribute('src', $new_src);
          } // <!--endif
      } // <!-
      $news->content = clean($dom->saveHTML());
      //feature Inage Change
      $rootPath = public_path('/media/gallery');
      if($request->hasFile('photo')){
             if(!empty($user->photo))
             {
                 if(file_exists($rootPath.'/' .$user->photo)){
                     unlink($rootPath.'/' .$user->photo);
                 }
             }
             $image  = $request->file('photo');
             $name   = time() . '.' . $image->getClientOriginalExtension();
             Image::make($image)->resize(500, null, function ($constraints) {
                 $constraints->aspectRatio();
             })->crop(300, 300)->save(public_path() . '/media/gallery/' . $name);
             $user->photo=$name;
         }
      $gallery->status = '1';
      $gallery->publish = $request->publish;
      $gallery->created_at = date('Y-m-d H:i:s');
      if($user->save())
      {
          return redirect::back()->with('success_message','User updated successfully');
      }else{
          return redirect::back()->with('info_message','User could not be updated successfully');
      }
  }


  public function galleryDelete($id)
      {
        //dd($id);
          if (!isset($id))
              return redirect()->back()->with('info_message', 'There was a problem');
          $news = Gallery::find($id);
          if ($news->delete()) {
              unlink(public_path() . '/media/gallery/' .$news->photo);
              return redirect()->back()->with('success_message', 'User deleted successfully');
          } else {
              redirect()->back()->with('info_message', 'There was a problem');
          }
      }
    public function destroy($id)
    {
        //
    }
}
