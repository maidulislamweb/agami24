<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\News;
use App\Home;
use App\Subcategory;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use Meta;
class HomeController extends Controller
{
 

//Single Article Details
  public function article($category, $id, $slug)
  {
    //Meta::set('name','AGAMI24 |');
    $meta= DB::table('metas')->where('news_id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keywards));
    $article = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
        // ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('users', 'news.reporter_id', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*','categories.slug as cslug','categories.name as cbname','photos.photo as nphoto','photos.caption as pcaption','users.designation as udes')
    ->where('news.id', $id)
    ->first();
    $array = explode(',', $article->tags);
    $tags = DB::table('tags')->whereIn('id', $array)->get();
    Meta::set('image', asset('../media/news/'.$article->nphoto));
    $relnews = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*','categories.name as cname','categories.slug as cslug','photos.photo as nphoto','tags.title as ttitle','tags.id as tag_id')
    ->where('news.status', 1)->where('categories.slug', $category)->orderBy('news.id', 'desc')->take(6)
    ->get();
    $latest= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','categories.name','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('articles.index', compact('article', 'relnews', 'latest', 'tags'));
  }
  public function topic($tslug)
  {
    $meta= DB::table('tags')->where('title', $tslug)->get();
    Meta::set('title',$meta[0]->title);
   // Meta::set('description',($meta[0]->description));
    //Meta::set('key',($meta[0]->keywards));
    $data['tag']=DB::table('tags')->where('title',$tslug)->take(1)->get();
    $data['tag_wise'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','users.name as uname','categories.slug as cslug','users.photo','photos.photo as nphoto','tags.title as ttitle','tags.id as tag_id')
    ->where('news.status', 1)->where('tags.title',$tslug)->orderBy('news.id', 'desc')->take(20)
    ->get();
    return view('topic.index',$data);
  }

    public function category($cname)
  {
     //$meta= Category::select('title','description','keywards')->where('id', $id)->get();
   // Meta::set('title',$meta[0]->title);
   // Meta::set('description',($meta[0]->description));
   // Meta::set('key',($meta[0]->keyward));
    $data['category_wise'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo')
    ->where('news.status', 1)->where('categories.slug',$cslug)->orderBy('news.id', 'desc')
    ->simplePaginate(20);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.slug', $cslug)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('index',$data);
  }
  public function archive()
  {
    $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
    $data['category']=DB::table('categories')->where('status',1)->get();
    $data['archive'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','categories.name as cbname','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')
    ->paginate(20);
    return view('archive',$data);
  }

//Front Page
  public function index()
  {
    $meta=DB::table('homes')->get();
    //Meta::set('name','AGAMI24');
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    Meta::set('author',($meta[0]->author));
    Meta::set('developer',($meta[0]->developer));
    Meta::set('logo', asset('media/news/'.$meta[0]->logo));
    $data['leadnews'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*','categories.name as cname','photos.photo as nphoto','categories.slug as cslug','tags.title as ttitle','tags.slug as tslug','tags.id as tagid')
    ->where('news.status', 1)->where('news.is_leadnews', 1)->orderBy('news.id', 'desc')->take(1)
    ->get();
    $data['news'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','photos.photo as nphoto','categories.slug as cslug','tags.title as ttitle','tags.id as tid','tags.slug as tslug')
    ->where('news.status', 1)->where('news.is_feature', 1)->orderBy('news.id', 'desc')->take(8)
    ->get();
    //$data['allTopic'] = DB::table('news')
    //->join('categories', 'news.category_id', '=', 'categories.id')
    //->join('users', 'news.created_by', '=', 'users.id')
    //->join('photos', 'news.id', '=', 'photos.news_id')
    //->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto')
    //->where('news.status', 1)->orderBy('news.id', 'desc')->take(6)
   // ->get();
    $data['popular'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->inRandomOrder()->limit(5)
    ->get();
    $data['latest'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['international'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',2)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['economics'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',3)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['opinion'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('users', 'news.created_by', '=', 'users.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','users.name as uname','categories.slug as cslug','users.photo','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',4)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['sports'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*','categories.id as cid','photos.photo as nphoto','categories.slug as cslug','tags.title as ttitle','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',5)->orderBy('news.id', 'desc')->take(4)
    ->get();
    $data['entertainment'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto','tags.title as ttitle','tags.id as tag_id','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',6)->orderBy('news.id', 'desc')->take(8)
    ->get();
    $data['features'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',7)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['lifestyle'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',8)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['national'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*','categories.id as cid','photos.photo as nphoto','categories.slug as cslug','tags.title as ttitle','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',10)->orderBy('news.id', 'desc')->take(8)
    ->get();
    $data['politics'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',11)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['technology'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',12)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['law_courts'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',13)->orderBy('news.id', 'desc')->take(5)
    ->get();
    //$data['healthlead'] =DB::table('news')
    //->join('categories', 'news.category_id', '=', 'categories.id')
    //->join('photos', 'news.id', '=', 'photos.news_id')
    //->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto')
    //->where('news.status', 1)->where('categories.id',14)->where('categories.id',14)->orderBy('news.id', 'desc')->take(1)
    //->get();
    $data['health'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',14)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['education'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',15)->orderBy('news.id', 'desc')->take(5)
    ->get();

   // $data['edulead'] =DB::table('news')
    //->join('categories', 'news.category_id', '=', 'categories.id')
   // ->join('photos', 'news.id', '=', 'photos.news_id')
    //->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    //->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',15)->orderBy('news.id', 'desc')->take(1)
    //->get();
    //$data['probash'] =DB::table('news')
    //->join('categories', 'news.category_id', '=', 'categories.id')
    //->join('photos', 'news.id', '=', 'photos.news_id')
   // ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    //->where('news.status', 1)->where('categories.id',16)->orderBy('news.id', 'desc')->take(5)
    //->get();
    $data['religion'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',17)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['literature'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname', 'categories.id as cid','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',18)->orderBy('news.id', 'desc')->take(5)
    ->get();
    $data['country'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.name','categories.slug as cslug','photos.photo as nphoto','tags.title as ttitle','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',19)->orderBy('news.id', 'desc')->limit(8)
    ->get();
    //$data['special_reports'] = DB::table('news')
    //->join('categories', 'news.category_id', '=', 'categories.id')
    //->join('tags', 'news.tags', '=', 'tags.id')
    //->join('photos', 'news.id', '=', 'photos.news_id')
    //->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto','tags.title as ttitle')
    //->where('news.status', 1)->where('categories.id',20)->orderBy('news.id', 'desc')->limit(4)
    //->get();
    $data['various'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',21)->orderBy('news.id', 'desc')->limit(5)
    ->get();
    $data['jobs'] = DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->join('tags', 'news.tags', '=', 'tags.id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto','tags.title as ttitle','tags.slug as tslug')
    ->where('news.status', 1)->where('categories.id',22)->orderBy('news.id', 'desc')->limit(8)
    ->get();
    //$data['galleries'] = DB::table('galleries')
    //->join('categories', 'galleries.category_id', '=', 'categories.id')
    //->select('galleries.*', 'categories.name as cname','categories.slug as cslug','galleries.photo as nphoto')
    //->where('galleries.status', 1)->orderBy('galleries.id', 'desc')->limit(4)
    //->get();
    //$data['divisions'] = DB::table('divisions')->where('status', 1)->get();
    return view('index',$data);
  }
  public function international()
  {
    //Meta::set('name','AGAMI24');
    $id=2;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['international'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('international',$data);
  }
  public function economy()
  {
    $id=3;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['economy'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('economy',$data);
  }
  public function opinion()
  {
    $id=4;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['opinion'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('opinion',$data);
  }
  public function sports()
  {
    $id=5;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['sports'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('sports',$data);
  }
  public function entertainment()
  {
    $id=6;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['entertainment'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('entertainment',$data);
  }
  public function feature()
  {
    $id=7;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['feature'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('feature',$data);
  }
  public function lifestyle()
  {
    Meta::set('name','AGAMI24');
    $id=8;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['lifestyle'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    return view('lifestyle',$data);
  }
  public function bangladesh()
  {
    Meta::set('name','AGAMI24');
    $id=10;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->where('status',1)->get();
    $data['bangladesh'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->simplePaginate(20);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('bangladesh',$data);
  }
  public function politics()
  {
    $id=11;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['politics'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('politics',$data);
  }
  public function technology()
  {
    $id=12;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['technology'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('technology',$data);
  }
  public function law_courts()
  {
    $id=13;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['law'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('law-courts',$data);
  }
  public function health()
  {
    $id=14;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['health'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('health',$data);
  }
  public function education()
  {
    $id=15;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['education'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('education',$data);
  }
  public function probash()
  {
    $id=16;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['probash'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('probash',$data);
  }
  public function religion()
  {
    $id=17;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
//$data['subcategory']=Subcategory::first()->where('category_id',$id);
//dd($subcategory);
    $data['religion'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->simplePaginate(20);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('religion',$data);
  }
  public function country()
  {
    $id=19;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['divisions'] = DB::table('divisions')->where('status', 1)->get();
    $data['category']=DB::table('categories')->where('status',1)->get();
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['country'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('country',$data);
  }
  public function literature()
  {
    Meta::set('name','AGAMI24');
    $id=18;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['literature'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('literature',$data);
  }
  public function various()
  {
    $id=21;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['various'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('various',$data);
  }
  public function jobs()
  {
    $id=22;
    $meta= Category::select('title','description','keywards')->where('id', $id)->get();
    Meta::set('title',$meta[0]->title);
    Meta::set('description',($meta[0]->description));
    Meta::set('key',($meta[0]->keyward));
    $data['category']= Category::find($id);
    $data['subcategory']=DB::table('subcategories')->where('category_id',$id)->get();
    $data['jobs'] =DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug', 'categories.id as cid','photos.photo as nphoto')
    ->where('news.status', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')
    ->paginate(21);
    $data['latest']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->orderBy('news.id', 'desc')->take(10)
    ->get();
    $data['featurenews']= DB::table('news')
    ->join('categories', 'news.category_id', '=', 'categories.id')
    ->join('photos', 'news.id', '=', 'photos.news_id')
    ->select('news.*', 'categories.name as cname','categories.slug as cslug','photos.photo as nphoto')
    ->where('news.status', 1)->where('news.is_feature', 1)->where('categories.id',$id)->orderBy('news.id', 'desc')->take(1)
    ->get();
    return view('jobs',$data);
  }
  public function gallery()
  {
     $data['galleries'] = DB::table('galleries')
      ->join('categories', 'galleries.category_id', '=', 'categories.id')
      ->select('galleries.*', 'categories.name as cname','categories.slug as cslug','galleries.photo as nphoto')
      ->where('galleries.status', 1)->orderBy('galleries.id', 'desc')->limit(4)
      ->get();
      return view('photo',$data);
    }
    public function districts_news()
    {
      $data['divisions']=DB::table('divisions')->where('status',1)->get();
      return view('districts-news',$data);
    }

    public function travel()
    {
      return view('travel');
    }

  }
