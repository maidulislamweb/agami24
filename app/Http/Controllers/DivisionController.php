<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use App\Division;
class DivisionController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['divisions'] =DB::table('divisions')->where('status', 1)->orderBy('divisions.order_id', 'ASC')->get();
      return view('admin.area.division',$data);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
      //dd($request->all());
      $validator = Validator::make($request->all(), [
        'name' => 'required',
        'bn_name' => 'required',
        'order_id' => 'required',
      ]);
      if ($validator->fails()) {
        Session::flash('info_message', 'Division Save Erroe');
        return Redirect::back()->withErrors($validator)->withInput();
      } else {
              $division = new Division();
              $division->name = $request->name;
              $division->bn_name = $request->bn_name;
              $division->order_id = $request->order_id;
              $division->status = '1';
              $division->created_at = date('Y-m-d');
              $division->save();
             Session::flash('success_message', 'Division Successfully Saved!');
             return redirect::back();
           }
    }
    public function division_publish($id){
     $publish = DB::table('divisions')->where('id', $id)->update(['status'=>'1']);
     Session::flash('success', 'Successfully saved your new tag!');
     return Redirect::back();
   }
   public function division_unpulish($id){
     //dd('HI');
     $unpulish = DB::table('divisions')->where('id', $id)->update(['status'=>'0']);
     Session::flash('success', 'Successfully saved your new tag!');
     return Redirect::back();
   }
    public function edit($id)
    {
      $division= Division::find($id);
      return view('admin.area.division_edit')->withDivision($division);
    }
    public function update(Request $request, $id)
    {
      // dd($request->all());
       $validator = Validator::make($request->all(), [
         'name' => 'required',
         'order_id' => 'required',
         'bn_name' => 'required',
       ]);
       if ($validator->fails()) {
         Session::flash('info_message', 'Division Update Failed');
         return Redirect::back()->withErrors($validator)->withInput();
       } else {
         $division = Division::find($id);
         $division['name'] =$request->name;
         $division['bn_name'] =$request->bn_name;
         $division['order_id'] =$request->order_id;
         $division['status'] = 1;
         $division['updated_at'] = date('Y-m-d H:i:s');
         $division->save();
          Session::flash('success_message', 'Division Successfully Updated!');
          return redirect()->route('division.index', $division->id);
     }
    }
    public function destroy($id)
   {
       $divisions   = Division::findOrFail($id);
       $divisions->delete();
       return response()->json($divisions);
   }
}
