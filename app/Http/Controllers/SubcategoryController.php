<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
class SubcategoryController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['subCategory'] = DB::table('subcategories')->join('categories', 'subcategories.category_id', '=', 'categories.id')
            ->join('users', 'subcategories.created_by', '=', 'users.id')
            ->select('subcategories.*', 'categories.name as category_name','users.name as user_name')
            ->orderBy('categories.id', 'ASC')
            ->get();
         $data['categoryinfo'] = DB::table('categories')->where('status', 1)->get();
          return view('admin.subcategory.index',$data);
    }

    public function store(Request $request)
    {
            //dd($request->all());
          $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
            'slug' => 'required',
          ]);
          if ($validator->fails()) {
            Session::flash('info_message', 'Subcategory  Save Failed');
            return Redirect::back()->withErrors($validator)->withInput();
          } else {
                  $subcategory = new Subcategory();
                  $subcategory['name'] =$request->name;
                  $subcategory['category_id'] =$request->category_id;
                  $str = strtolower($request->slug);
                  $subcategory['slug'] = preg_replace('/\s+/', '-', $str);$str = strtolower($request->slug);
                  $subcategory['slug'] = preg_replace('/\s+/', '-', $str);
                  $subcategory['description'] =$request->description;
                  $subcategory['order_id'] =$request->order_id;
                  $subcategory['created_by'] = Auth::user()->id;
                  $subcategory['status'] = 1;
                  $subcategory['created_at'] = date('Y-m-d H:i:s');
                  $subcategory->save();
                 Session::flash('success_message', 'Subcategory Successfully Saved');
                 return redirect::back();
               }
    }
    public function edit($subcategoryId)
    {
      $categoryinfo = DB::table('categories')->where('status', 1)->get();
      $subcategory = DB::table('subcategories')->where('id', $subcategoryId)->where('status', 1)->take(1)->get();
      //$subcategory = Subcategory::find($subcategoryId);

      return view('admin.subcategory.edit', compact('categoryinfo', 'subcategory'));
      //return view('admin.subcategory.edit',$data);//->withSubcategory($subcategory,$data);
    }
    public function update(Request $request, $id)
    {
        //dd('HI');
        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'category_id' => 'required',
          'slug' => 'required',
          'description' => 'required',
          'order_id' => 'required',
        ]);
        if ($validator->fails()) {
          Session::flash('info_message', 'Subcategory Update Failed');
          return Redirect::back()->withErrors($validator)->withInput();
        } else {
          $subcategory = Subcategory::find($id);
          $subcategory['name'] =$request->name;
          $subcategory['category_id'] =$request->category_id;
          $str = strtolower($request->slug);
          $subcategory['slug'] = preg_replace('/\s+/', '-', $str);
          $subcategory['description'] =$request->description;
          $subcategory['order_id'] =$request->order_id;
          $subcategory['updated_by'] = Auth::user()->id;
          $subcategory['status'] = 1;
          $subcategory['updated_at'] = date('Y-m-d H:i:s');
          $subcategory->save();
           Session::flash('success_message', 'Subcategory Successfully Updated!');
           return redirect()->route('subcategory.index', $subcategory->id);
      }
    }


    public function subcatStatus()
    {
        $id = Input::get('id');
        $sub = Subcategory::findOrFail($id);
        $sub->status = !$sub->status;
        $sub->save();
        return response()->json($sub);
    }





    public function get_subcategory(Request $request)
    {
        $input      = $request->all();
        $subcategories      =  Subcategory::where('category_id', $input['category_id'])->get();
        $str        = '<option value="">--Select--</option>';
        if($subcategories)
        {
            foreach($subcategories as $subcategory)
            {
                $str .= "<option value='".$subcategory['id']."'>".$subcategory['name']."</option>";
            }
        }
        echo $str;
    }

    public function get_area(Request $request)
      {
          $input          = $request->all();
          $areas       = Area::where('district_id', $input['district_id'])->orderBy('name', 'asc')->get();
          $str = '<option value="">----Select Upazila----</option>';
          if($upazilas)
          {
              foreach($upazilas as $sInfo)
              {
                  $str .= "<option value='".$sInfo['id']."'>".$sInfo['name']."</option>";
              }
          }
      }

      public function sub_destroy(Request $request)
     {
         $id = $request->input('id');
         $subcategory = Subcategory::find($id);
         $subcategory->delete();
         return response()->json($subcategory);
     }
}
