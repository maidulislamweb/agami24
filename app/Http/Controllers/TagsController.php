<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use App\Tag;
class TagsController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $data['tags'] =DB::table('tags')->get();
        return view ('admin.tags.index',$data);
    }
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
          Session::flash('info_message', 'Tag Save Unsuccessfull');
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
        $tags = new Tag();
         $tags->title = $request->title;
         $str = strtolower($request->slug);
         $tags->slug = preg_replace('/\s+/', '-', $str);
         $tags->created_by = Auth::user()->id;
         $tags->status = '1';
         $tags->created_at = date('Y-m-d');
         $tags->save();
         Session::flash('success_message', 'Tag successfully Saved..!');
         return redirect::back();
       }
    }
public function tagStatus()
    {
        $id = Input::get('id');
        $tag = Tag::findOrFail($id);
        $tag->status = !$tag->status;
        $tag->save();
        return response()->json($tag);
    }
     public function edit($id)
    {
        $tag = Tag::find($id);
        return view('admin.tags.edit')->withTag($tag);
    }
    public function update(Request $request, $id)
    {

      $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
          Session::flash('info_message', 'Unsuccessfull');
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
         $tags = Tag::find($id);
         $tags->title = $request->title;
         $str = strtolower($request->slug);
         $tags->slug = preg_replace('/\s+/', '-', $str);
         //$tags->description = $request->description;
         $tags->updated_by = Auth::user()->id;
         $tags->updated_at = date('Y-m-d');
         $tags->save();
         Session::flash('success_message', 'Tag Successfully updated...!');
         return redirect()->route('tags.index', $tags->id);
       }
    //  dd('HI');
    }
     public function destroy($id)
    {
        $tags   = Tag::findOrFail($id);
        $tags->delete();
        return response()->json($tags);
    }

}
