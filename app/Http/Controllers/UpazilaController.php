<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use App\District;
use App\Upazila;
class UpazilaController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
       $data['districts'] = DB::table('districts')->where('status', 1)->get();
      $data['upazilas'] = DB::table('upazilas')
            ->join('districts', 'upazilas.district_id', '=', 'districts.id')
            ->select('upazilas.*','districts.name as dist_name')//->orderBy('districts.id')
            ->get();
            return view('admin.area.upazila',$data);

    }
    public function get_upazila(Request $request)
    {
        $input      = $request->all();
        $upazilas      =  Upazila::where('district_id', $input['district_id'])->get();
       //$subcategories      =  DB::table('subcategories')->where('category_id', $input['category_id'])->get();//->orderBy('area_name', 'asc')
        $str        = '<option value="">--উপজেলা--</option>';
        if($upazilas)
        {
            foreach($upazilas as $upazila)
            {
                $str .= "<option value='".$upazila['id']."'>".$upazila['bn_name']."</option>";
            }
        }
        echo $str;
    }
    public function store(Request $request)
    {
    //dd($request->all());
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'bn_name' => 'required',
      'district_id' => 'required',
    ]);
    if ($validator->fails()) {
      Session::flash('info_message', 'Upazila Save Erroe');
      return Redirect::back()->withErrors($validator)->withInput();
    } else {
          $upazila = new Upazila();
          $upazila->name = $request->name;
           $upazila->bn_name = $request->bn_name;
           $upazila->district_id = $request->district_id;
           $upazila->order_id = $request->order_id;
           $upazila->status = '1';
           $upazila->created_at = date('Y-m-d');
           $upazila->save();
           Session::flash('success_message', 'Upazila Successfully Saved!');
           return redirect::back();
         }
    }
     public function edit($id)
     {
       //$data['divisions'] = DB::table('divisions')->where('status', 1)->get();
       $data['districts'] = DB::table('districts')->where('status', 1)->get();
       $data['upazilas'] = DB::table('upazilas')->where('id', $id)->where('status', 1)->take(1)->get();
       return view('admin.area.upazila_edit',$data);//->withSubcategory($subcategory,$data);
     }
     public function update(Request $request, $id)
     {
        // dd($request->all());
         $validator = Validator::make($request->all(), [
           'name' => 'required',
           'district_id' => 'required',
           'bn_name' => 'required',
         ]);
         if ($validator->fails()) {
           Session::flash('info_message', 'Upazila Update Failed');
           return Redirect::back()->withErrors($validator)->withInput();
         } else {
           $upazila = Upazila::find($id);
           $upazila['name'] =$request->name;
           $upazila['bn_name'] =$request->bn_name;
           $upazila['district_id'] =$request->district_id;
           $upazila['status'] = 1;
           $upazila['updated_at'] = date('Y-m-d H:i:s');
           $upazila->save();
            Session::flash('success_message', 'Upazila Successfully Updated!');
            return redirect()->route('upazila.index', $upazila->id);
       }
     }
     public function destroy($id)
    {
        $upazilas   = Upazila::findOrFail($id);
        $upazilas->delete();
        return response()->json($upazilas);
    }
}
