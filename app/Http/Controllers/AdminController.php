<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Home;
use DB;
use Redirect;
use Auth;
use Validator;
use Session;
use Image;
use File;
use Analytics;
use Spatie\Analytics\Period;
class AdminController extends Controller
{
  public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $news = DB::table('news')->join('categories', 'news.category_id', '=', 'categories.id')
            ->join('users', 'news.created_by', '=', 'users.id')
            ->select('news.*', 'categories.name as category_name','users.name as user_name')->take(5)
            ->get();
      $analytic=Analytics::fetchVisitorsAndPageViews(Period::days(1));
      $categories =DB::table('categories')->where('status', 1)->take(19)->get();

      return view('admin.index', compact('news', 'analytic', 'categories'));

    }
    public function home_info()
    {
      $data['meta']=DB::table('homes')->take(1)->get();
    //  dd($meta);
      return view ('admin.setting.home_info',$data);
    }
    public function updateMeta(Request $request)
    {
      $meta=Home::find($request->id);
    //  dd($meta);

      $meta->title = $request->title;
      $meta->keyward = $request->keyward;
      $meta->description = $request->description;
      $meta->author = $request->author;
      $meta->developer = $request->developer;
      $meta->updated_at = date('Y-m-d H:i:s');
      //logo
      $rootPath = public_path('/media/logo');
        if($request->hasFile('logo')){
            if(!empty($meta->logo))
            {
                if(file_exists($rootPath.'/' .$meta->logo)){
                    unlink($rootPath.'/' .$meta->logo);
                }
            }
            $image  = $request->file('logo');
            $name   = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(80, null, function ($constraints) {
                $constraints->aspectRatio();
            })->save($rootPath.'/' . $name);
            $meta->logo=$name;
        }
      //dd($meta);
      if($meta->save())
      {
          return redirect::back()->with('success_message','User updated successfully');
      }else{
          return redirect::back()->with('info_message','User could not be updated successfully');
      }
    }




    public function comment()
    {
    return view ('admin.pages.comment');
    }
    public function social()
    {
    return view ('admin.pages.social');
    }
    public function subscriber()
    {
      $data['subscriber'] =DB::table('users')->where('status', 1)->where('role', 4)->take(5)->get();
    return view ('admin.subscriber.index',$data);
    }
}
