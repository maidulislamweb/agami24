<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index');
Route::get('/category/{cslug}', 'HomeController@category_wise');
Route::get('/bangladesh', 'HomeController@bangladesh')->name('bangladesh');
Route::get('/international', 'HomeController@international')->name('international');
Route::get('/entertainment', 'HomeController@entertainment')->name('entertainment');
Route::get('/country', 'HomeController@country')->name('country');
Route::get('/politics', 'HomeController@politics')->name('politics');
Route::get('/economy', 'HomeController@economy')->name('economy');
Route::get('/sports', 'HomeController@sports')->name('Sports');
Route::get('/feature', 'HomeController@feature')->name('Feature');
Route::get('/cricket', 'HomeController@cricket')->name('Cricket');
Route::get('/football', 'HomeController@cricket')->name('Footbal');
Route::get('/hollywood', 'HomeController@hollywood')->name('hollywood');
Route::get('/bollywood', 'HomeController@bollywood')->name('bollywood');
Route::get('/lifestyle', 'HomeController@lifestyle')->name('lifestyle');
Route::get('/travel', 'HomeController@travel')->name('travel');
Route::get('/districts-news', 'HomeController@districts_news')->name('districts-news');
Route::get('/jokes', 'HomeController@jokes')->name('jokes');
Route::get('/opinion', 'HomeController@opinion')->name('opinion');
Route::get('/photo-gallery', 'HomeController@gallery')->name('gallery');
Route::get('/education', 'HomeController@education')->name('education');
Route::get('/health', 'HomeController@health')->name('health');
Route::get('/law-courts', 'HomeController@law_courts')->name('law-courts');
Route::get('/religion', 'HomeController@religion')->name('religion');
Route::get('/probash', 'HomeController@probash')->name('probash');
Route::get('/various', 'HomeController@various')->name('various');
Route::get('/technology', 'HomeController@technology')->name('technology');
Route::get('/literature', 'HomeController@literature')->name('literature');
Route::get('/jobs', 'HomeController@jobs')->name('jobs');
Route::get('/ekushey-book-fair', 'HomeController@ekushey_book_fair')->name('ekushey-book-fair');
Route::get('/archive', 'HomeController@archive')->name('archive');
//TAGS
Route::get('/topic/{ldTtitle}', 'HomeController@topic');
//ARTICLES
Route::get('/{category}/articles/{id}/{slug}', 'HomeController@article');
//DASHBOARD AREA
Auth::routes();
Route::get('/admin', 'AdminController@index')->name('Admin');
Route::get('/home_info', 'AdminController@home_info')->name('home_info');
//Category Route
Route::get('/category', 'CategoryController@index')->name('home');
Route::post('/categoryInsert', 'CategoryController@store')->name('Category Insert');
Route::delete('/category/delete', 'CategoryController@destroy');
Route::get('/category-edit/{categoryId}', 'CategoryController@category_edit')->name('');
Route::post('category_update', 'CategoryController@update')->name('');
Route::get('/category-delete/{categoryId}', 'CategoryController@category_delete')->name('');
Route::post('categoryStatus', array('as' => 'categoryStatus', 'uses' => 'CategoryController@categoryStatus'));
//SUBCATEGORY
Route::post('/subCategoryInsert','CategoryController@subCatStore')->name('Category Insert');
Route::post('get_subcategory','CategoryController@get_subcategory');
Route::get('/subcategory/{id}', 'CategoryController@ajaxsubCategory');
Route::resource('subcategory', 'SubcategoryController', ['except' => ['create']]);
Route::delete('/sub_category/delete', 'CategoryController@sub_destroy');
Route::post('subcatStatus', array('as' => 'subcatStatus', 'uses' => 'SubcategoryController@subcatStatus'));
//NEWS
Route::get('/allnews','NewsController@index')->name('news');
Route::get('/addNews', 'NewsController@addNews')->name('user');
Route::post('postInsert','NewsController@postInsert');
Route::get('/draft_post', 'NewsController@draft_post')->name('');
Route::get('/news-delete/{newsId}', 'NewsController@newsDelete')->name('news delete');
Route::get('/news-view/{newsId}', 'NewsController@newsView')->name('ad view');
Route::get('/news-edit/{newsId}', 'NewsController@newsEdit')->name('ad edit');
Route::get('newsfilter', 'NewsController@newsfilter')->name('newsfilter');
Route::post('newsfilterView', 'NewsController@newsfilterView')->name('newsfilter');
Route::post('postUpdate', 'NewsController@postUpdate')->name('News Update');
Route::get('/mypost','NewsController@myPost')->name('news');
Route::get('/days_news','NewsController@days_news')->name('news');

//ANALYTICS
Route::get('/google-analytics','AnalyticController@index')->name('google-analytics');
//advertisement
Route::get('/advertisement','AddsController@index')->name('advertisement');
Route::get('/videos', 'GalleryController@videos')->name('Videos');
//USER
Route::post('register_user','UserController@register')->name('update');
Route::get('/user', 'UserController@index')->name('user');
Route::get('/userAdd', 'UserController@userAdd')->name('Profile');
Route::post('profile-update','UserController@update')->name('update');
Route::post('update_user','UserController@update_user')->name('update');
Route::post('userStatus', array('as' => 'userStatus', 'uses' => 'UserController@userStatus'));
Route::get('/user-edit/{userId}', 'UserController@edit')->name('');
Route::get('/user_delete/{userId}', 'UserController@deleteUser')->name('deleteUser');
Route::get('/singleUser/{userId}', 'UserController@singleUser')->name('singleUser');
Route::get('/profile','UserController@profile')->name('User Profile');
Route::get('/profile-edit','UserController@profile_edit')->name('Profile');
Route::get('/profile-activity','UserController@profile_activity')->name('Profile');
Route::get('/subscriber', 'AdminController@subscriber')->name('subscriber');
//AREA
Route::post('get_district','DistrictController@get_district')->name('');
Route::post('get_upazila','UpazilaController@get_upazila')->name('');
Route::resource('division', 'DivisionController', ['except' => ['create']]);
Route::resource('district', 'DistrictController', ['except' => ['create']]);
Route::resource('upazila', 'UpazilaController', ['except' => ['create']]);
Route::resource('tags', 'TagsController', ['except' => ['create']]);
Route::post('tagStatus', array('as' => 'tagStatus', 'uses' => 'TagsController@tagStatus'));
//META
Route::post('updateMeta', 'AdminController@updateMeta')->name('Company Update');
//GALLERY
Route::get('gallery', 'GalleryController@index');
Route::get('gallery_add', 'GalleryController@gallery_add');
Route::post('galleryStore','GalleryController@galleryStore');
Route::post('galleryEdit/{galId}/','GalleryController@galleryEdit');
Route::get('galleryDelete/{galId}', 'GalleryController@galleryDelete')->name('');
//COMPANY info
Route::get('/company_info', 'CompanyController@index')->name('Company Info');
Route::post('companyInfo_save', 'CompanyController@companyInfo_save')->name('Company Update');
//COMMENT
Route::get('/comments', 'AdminController@comment')->name('comment');
Route::post('changeStatus', array('as' => 'changeStatus', 'uses' => 'NewsController@changeStatus'));
Route::post('featureStatus', array('as' => 'featureStatus', 'uses' => 'NewsController@featureStatus'));
Route::post('leadnewsStatus', array('as' => 'leadnewsStatus', 'uses' => 'NewsController@leadnewsStatus'));
